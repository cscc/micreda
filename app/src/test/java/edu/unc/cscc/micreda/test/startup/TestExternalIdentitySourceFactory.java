package edu.unc.cscc.micreda.test.startup;

import java.util.Collection;
import java.util.Collections;

import org.slf4j.Logger;

import edu.unc.cscc.micreda.model.ExternalIdentitySource;
import edu.unc.cscc.micreda.model.ExternalIdentitySourceFactory;

public class TestExternalIdentitySourceFactory
implements ExternalIdentitySourceFactory
{
	
	public static final String		SOURCE_ID = "loaded-via-factory";

	@Override
	public Collection<ExternalIdentitySource> 
	obtainSources(Logger logger)
	{
		return Collections.singleton(new ExternalIdentitySource()
		{
			
			@Override
			public String identifier()
			{
				return SOURCE_ID;
			}
			
			@Override
			public String description()
			{
				return SOURCE_ID;
			}
			
			@Override
			public boolean authenticate(String username, String credentials)
			{
				return false;
			}
		});
	}

}
