package edu.unc.cscc.micreda.test.startup;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import edu.unc.cscc.micreda.model.Discoverable;
import edu.unc.cscc.micreda.model.ExternalIdentitySourceFactory;
import edu.unc.cscc.micreda.model.IdentitySource;
import edu.unc.cscc.micreda.service.IdentityResolver;
import edu.unc.cscc.micreda.service.InternalIdentitySourceService;
import edu.unc.cscc.micreda.test.SpringSupportedTest;

public class LoadingTest
extends SpringSupportedTest
{
	@Autowired
	private InternalIdentitySourceService		iss;
	
	@Autowired
	private IdentityResolver					resolver;
	
	
	@Test(expected = IllegalArgumentException.class)
	public void
	preventInternalSourceCreationOnConflict()
	{
		this.iss.save(DummyExternalIdentitySource.IDENTIFIER, "test");
	}
	
	@Test
	public void
	sourceLoadedByFactory()
	{
		IdentitySource source = 
				this.resolver.resolveSource(TestExternalIdentitySourceFactory.SOURCE_ID);
		
		Assert.assertNotNull(source);
	}
	
	/**
	 * Tests whether a source that is on the classpath (in this case in 
	 * our test hierarchy) annotated with {@link Discoverable} but NOT present
	 * in the Java-style SPI service declaration is detected and loaded. 
	 */
	@Test
	public void
	discoverableSource()
	{
		IdentitySource source = 
				this.resolver.resolveSource(DiscoverableExternalIdentitySource.IDENTIFIER);
		
		Assert.assertNotNull(source);
	}
	
	/**
	 * Tests whether an
	 *  {@link ExternalIdentitySourceFactory external identity source factory} 
	 * that is on the classpath (in this case in our test hierarchy) annotated 
	 * with {@link Discoverable} but NOT present
	 * in the Java-style SPI service declaration is detected and whether the
	 * sources that it provides are made available.
	 */
	@Test
	public void
	discoverableSourceFactory()
	{
		IdentitySource source = 
				this.resolver.resolveSource(DiscoverableExternalIdentitySourceFactory.SOURCE_ID);
		
		Assert.assertNotNull(source);
	}
}
