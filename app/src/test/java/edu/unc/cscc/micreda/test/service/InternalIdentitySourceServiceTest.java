package edu.unc.cscc.micreda.test.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import edu.unc.cscc.micreda.model.MUser;
import edu.unc.cscc.micreda.model.UserIdentity;
import edu.unc.cscc.micreda.service.InternalIdentitySource;
import edu.unc.cscc.micreda.service.InternalIdentitySourceService;
import edu.unc.cscc.micreda.service.UserService;
import edu.unc.cscc.micreda.test.SpringSupportedTest;

public class InternalIdentitySourceServiceTest
extends SpringSupportedTest
{

	@Autowired
	private InternalIdentitySourceService	service;
	
	@Autowired
	private UserService					userService;
	
	private MUser
	createUser()
	{
		Set<String> roles = new HashSet<>();
		
		roles.add("test_role");
		roles.add("something else");
		
		MUser user = new MUser(null, false, roles);
		
		return this.userService.save(user);
	}
	
	@Test
	public void
	createAndLoad()
	{
		final String identifier = 
				"identity-source-" + UUID.randomUUID().toString();
		
		InternalIdentitySource source = this.service.save(identifier, "test");
		
		Assert.assertEquals(identifier, source.identifier());
		
		InternalIdentitySource loaded = this.service.load(source.id());
		
		Assert.assertEquals(source, loaded);
		
	}
	
	@Test
	public void
	updateExisting()
	{
		final String identifier = 
				"identity-source-" + UUID.randomUUID().toString();
		
		InternalIdentitySource totallyUnrelated = 
				this.service.save("totally-unrelated-" + UUID.randomUUID().toString(),
									"totally-unrelated-desc-" + UUID.randomUUID().toString());
		
		InternalIdentitySource source = this.service.save(identifier, "test");
				
		InternalIdentitySource loaded = this.service.load(source.id());
		
		Assert.assertEquals(source, loaded);
		
		final String newDesc = "other-description";
				
		InternalIdentitySource modified = this.service.save(identifier, newDesc);
		
		Assert.assertEquals(newDesc, modified.description());
		Assert.assertEquals(loaded.identifier(), modified.identifier());
		
		loaded = this.service.find(identifier);
		
		Assert.assertEquals(modified, loaded);
		
		/* make sure that we didn't change anything else by mistake */
		InternalIdentitySource loadedUnrelated = 
				this.service.find(totallyUnrelated.identifier());
		
		Assert.assertEquals(totallyUnrelated, loadedUnrelated);
	}
	
	@Test
	public void
	findByIdentifier()
	{
		final String identifier = 
				"identity-source-" + UUID.randomUUID().toString();
		
		InternalIdentitySource source = this.service.save(identifier, "test");
		
		Assert.assertEquals(identifier, source.identifier());
		
		InternalIdentitySource found = this.service.find(identifier);
		
		Assert.assertEquals(source, found);
	}
	
	@Test
	public void
	exists()
	{
		final String identifier = 
				"identity-source-" + UUID.randomUUID().toString();
		
		InternalIdentitySource source = this.service.save(identifier, "test");
		
		Assert.assertEquals(identifier, source.identifier());
		
		Assert.assertTrue(this.service.exists(identifier));
		
		Assert.assertFalse(this.service.exists("doesn't exist"));
	}
	
	@Test
	public void
	creationAndDiscovery()
	{
		Collection<String> identifiers = 
				IntStream.range(0, (new Random()).nextInt(90) + 10)
							.mapToObj(i -> "identifier-" + i + "-" + UUID.randomUUID())
							.collect(Collectors.toList());
		
		identifiers.forEach(id -> this.service.save(id, "test'"));
		
		
		Map<String, InternalIdentitySource> sourceMap = 
				this.service.enumerateSources();
		
		Assert.assertTrue(identifiers.stream().allMatch(sourceMap :: containsKey));
		
	}
	
	@Test
	public void
	createSourceAndUser()
	{
		final String identifier = 
				"identity-source-" + UUID.randomUUID().toString();
		
		InternalIdentitySource source = this.service.save(identifier, "test");
		
		final String username = "internal-user-test-" + UUID.randomUUID();
		final String password = "foobar123";
		
		final MUser user = this.createUser();
		
		UserIdentity ident = new UserIdentity(user, username, identifier);
		
		ident = source.save(ident, password);
		
		/* now try to find the identity that we just created */
		
		UserIdentity found = source.find(username);
		
		Assert.assertEquals(ident, found);
		
	}
	
	@Test
	public void
	findIdentitiesByUser()
	{
		Random random = new Random();
		
		MUser user = this.userService.save(this.createUser());
		
		final String identifier = 
				"identity-source-" + UUID.randomUUID().toString();
		
		InternalIdentitySource source = this.service.save(identifier, "test");
		
		Collection<UserIdentity> created = new ArrayList<>();
		
		/* add some identities for them */
		
		for (int i = 0; i < random.nextInt(20) + 5; i++)
		{
			final String username = "internal-user-test-" + UUID.randomUUID();
			
			created.add(new UserIdentity(user, username, identifier));
						
		}
		
		final String password = "foobar123";

		
		created = created.stream()
						.map(id -> source.save(id, password))
						.collect(Collectors.toSet());
		
		/* now try to find them by user */
		Collection<UserIdentity> found = source.find(user);
		
		Assert.assertEquals(created.size(), found.size());
		Assert.assertTrue(created.containsAll(found));
		Assert.assertTrue(found.containsAll(created));
		
	}
	
}
