package edu.unc.cscc.micreda.test.startup;

import edu.unc.cscc.micreda.model.ExternalIdentitySource;

/**
 * Dummy external identity source.  Does nothing other than exist.  Intended
 * to provide a test case with a constant name for use when testing 
 * external source loading.
 * 
 * @author Rob Tomsick (rtomsick@unc.edu)
 *
 */
public class DummyExternalIdentitySource
implements ExternalIdentitySource
{
	
	public static final String		IDENTIFIER = "dummy-ext-ident-source";

	@Override
	public String identifier()
	{
		return IDENTIFIER;
	}

	@Override
	public String description()
	{
		return "";
	}

	@Override
	public boolean 
	authenticate(String username, String credentials)
	{
		return false;
	}
	
	

}
