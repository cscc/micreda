package edu.unc.cscc.micreda.test.service;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import edu.unc.cscc.micreda.model.MUser;
import edu.unc.cscc.micreda.model.UserIdentity;
import edu.unc.cscc.micreda.service.InternalIdentitySource;
import edu.unc.cscc.micreda.service.InternalIdentitySourceService;
import edu.unc.cscc.micreda.service.UserService;
import edu.unc.cscc.micreda.test.SpringSupportedTest;

/**
 * Test the operation of dynamically-created identity sources.
 * 
 * @author Rob Tomsick (rtomsick@unc.edu)
 *
 */
public class InternalIdentitySourceTest
extends SpringSupportedTest
{

	@Autowired
	private InternalIdentitySourceService		sourceService;
	
	@Autowired
	private UserService					userService;
	
	private MUser
	createUser()
	{
		Set<String> roles = new HashSet<>();
		
		roles.add("test_role");
		roles.add("something else");
		
		MUser user = new MUser(null, false, roles);
		
		return this.userService.save(user);
	}
	
	public InternalIdentitySource
	createSource()
	{
		return this.sourceService.save("src-" + UUID.randomUUID(), "test");
	}
	
	@Test
	public void
	createAndAuthenticate()
	{
		InternalIdentitySource source = this.createSource();
		
		final String username = "test";
		final String password = "testpassword";
		
		UserIdentity ident = 
				new UserIdentity(this.createUser(), 
									username,
									source.identifier());
		
		ident = source.save(ident, password);
		
		Assert.assertTrue(source.authenticate(username, password));
	}
	
	@Test
	public void
	delete()
	{
		/* first, create, save, and authenticate */
		InternalIdentitySource source = this.createSource();
		
		final String username = "test";
		final String password = "testpassword";
		
		UserIdentity ident = 
				new UserIdentity(this.createUser(), 
									username,
									source.identifier());
		
		ident = source.save(ident, password);
		
		Assert.assertTrue(source.authenticate(username, password));
		
		/* now nuke them and check that we can't find or auth them */
		source.delete(ident);
		
		Assert.assertFalse(source.authenticate(username, password));
		
		Assert.assertTrue(source.find(ident.user()).isEmpty());
		Assert.assertNull(source.find(username));
	}
	
	@Test
	public void
	authenticationFailure()
	{
		InternalIdentitySource source = this.createSource();
		
		final String username = "test";
		final String password = "testpassword";
		
		UserIdentity ident = 
				new UserIdentity(this.createUser(), 
									username,
									source.identifier());
		
		ident = source.save(ident, password);
		
		Assert.assertFalse(source.authenticate(username, "wrong password"));
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void
	rejectEmptyUsername()
	{
		InternalIdentitySource source = this.createSource();
		
		final String username = "";
		final String password = "testpassword";
		
		UserIdentity ident = 
				new UserIdentity(this.createUser(), 
									username,
									source.identifier());
		
		ident = source.save(ident, password);
	}
}
