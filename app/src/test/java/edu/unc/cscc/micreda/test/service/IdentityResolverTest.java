package edu.unc.cscc.micreda.test.service;

import java.util.Collections;
import java.util.UUID;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import edu.unc.cscc.micreda.model.IdentitySourceAttributes;
import edu.unc.cscc.micreda.model.MUser;
import edu.unc.cscc.micreda.model.UserIdentity;
import edu.unc.cscc.micreda.service.IdentityResolver;
import edu.unc.cscc.micreda.service.InternalIdentitySource;
import edu.unc.cscc.micreda.service.InternalIdentitySourceService;
import edu.unc.cscc.micreda.service.UserService;
import edu.unc.cscc.micreda.test.SpringSupportedTest;
import edu.unc.cscc.micreda.test.startup.DummyExternalIdentitySource;

/**
 * Test for the {@link IdentityResolver user identity resolver} shim.
 * 
 * @author Rob Tomsick (rtomsick@unc.edu)
 *
 */
public class IdentityResolverTest
extends SpringSupportedTest
{
	
	@Autowired
	private IdentityResolver				resolver;
	
	@Autowired
	private InternalIdentitySourceService	iss;
	
	@Autowired
	private UserService					userService;
	
	private MUser
	createUser()
	{	
		return this.userService.save(new MUser(null, false, 
									Collections.emptySet()));
	}

	
	@Test
	public void
	identityResolution_success()
	{
		/* create an internal source and resolve against that */
		
		final String identifier = "test-iden-" + UUID.randomUUID();
		
		InternalIdentitySource source = this.iss.save(identifier, "Test");
		
		final String username = "test-username-" + UUID.randomUUID();
		final String password = UUID.randomUUID().toString();
		
		final MUser user = this.createUser();
		
		final UserIdentity identity = 
				source.save(new UserIdentity(user, username, identifier), 
								password);
		
		UserIdentity resolved = this.resolver.resolve(username, identifier);
		
		Assert.assertNotNull(resolved);
		
		Assert.assertEquals(identity.user(), resolved.user());
		Assert.assertEquals(identity.username(), resolved.username());
		Assert.assertEquals(identity.identitySourceIdentifier(), 
								resolved.identitySourceIdentifier());
	}
	
	@Test
	public void
	identityResolution_failure()
	{
		Assert.assertNull(this.resolver.resolve("bogus", "does not exist"));
	}
	
	@Test
	public void
	sourceResoution_success()
	{
		
		/* create an internal source and resolve that */
		
		final String identifier = "test-iden-" + UUID.randomUUID();
		
		InternalIdentitySource source = this.iss.save(identifier, "test");
		
		Assert.assertNotNull(source);
		
		IdentitySourceAttributes resolved = this.resolver.resolveSource(identifier);
		
		Assert.assertEquals(source, resolved);
		
		/* now try to resolve our dummy external ID source */
		IdentitySourceAttributes is = this.resolver.resolveSource(DummyExternalIdentitySource.IDENTIFIER);
		
		Assert.assertNotNull(is);
		
		Assert.assertEquals(DummyExternalIdentitySource.IDENTIFIER, is.identifier());
		
	}
	
	@Test
	public void
	sourceResolution_failure()
	{
		Assert.assertNull(this.resolver.resolveSource("does not exist"));
	}
}
