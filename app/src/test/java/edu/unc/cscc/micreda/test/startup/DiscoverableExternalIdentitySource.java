package edu.unc.cscc.micreda.test.startup;

import edu.unc.cscc.micreda.model.Discoverable;

@Discoverable
public class DiscoverableExternalIdentitySource
extends DummyExternalIdentitySource
{
	
	public static final String			IDENTIFIER = "dummy-discoverable-source";

	@Override
	public String identifier()
	{
		return IDENTIFIER;
	}
	
}
