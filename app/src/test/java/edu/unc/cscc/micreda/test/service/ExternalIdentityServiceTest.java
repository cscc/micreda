package edu.unc.cscc.micreda.test.service;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import edu.unc.cscc.micreda.model.MUser;
import edu.unc.cscc.micreda.model.UserIdentity;
import edu.unc.cscc.micreda.service.ExternalIdentityService;
import edu.unc.cscc.micreda.service.UserService;
import edu.unc.cscc.micreda.test.SpringSupportedTest;

public class ExternalIdentityServiceTest
extends SpringSupportedTest
{

	@Autowired
	private ExternalIdentityService		service;
	
	@Autowired
	private UserService					userService;
	
	private MUser
	createUser()
	{
		Set<String> roles = new HashSet<>();
		
		roles.add("test_role");
		roles.add("something else");
		
		MUser user = new MUser(null, false, roles);
		
		return this.userService.save(user);
	}
	
	@Test
	public final void 
	createAndFind()
	{
		MUser user = this.createUser();
		
		String username = "username-" + UUID.randomUUID();
		
		String isid = "external-identifier-source-" + UUID.randomUUID();
		
		UserIdentity identity = 
				new UserIdentity(user, username, isid);
		
		
		identity = this.service.create(identity);
		
		UserIdentity found = this.service.find(username, isid);
		
		Assert.assertEquals(identity, found);
	}
	
	@Test
	public final void
	delete()
	{
		MUser user = this.createUser();
		
		String username = "username-" + UUID.randomUUID();
		
		String isid = "external-identifier-source-" + UUID.randomUUID();
		
		UserIdentity identity = 
				new UserIdentity(user, username, isid);
		
		
		identity = this.service.create(identity);
		
		UserIdentity found = this.service.find(username, isid);
		
		Assert.assertEquals(identity, found);
		
		this.service.delete(identity);
		
		Assert.assertNull(this.service.find(username, isid));
	}

}
