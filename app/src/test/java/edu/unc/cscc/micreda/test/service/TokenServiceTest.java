package edu.unc.cscc.micreda.test.service;

import java.time.Instant;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Stream;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;

import edu.unc.cscc.micreda.config.JWTProperties;
import edu.unc.cscc.micreda.model.MUser;
import edu.unc.cscc.micreda.service.TokenService;
import edu.unc.cscc.micreda.test.SpringSupportedTest;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.impl.TextCodec;

public class TokenServiceTest
extends SpringSupportedTest
{

	
	
	
	@Autowired
	private JWTProperties					jwtConfig;
	@Autowired
	private TokenService					tokenService;
	
	
	private static final MUser
	createRandomUser()
	{
		final String username = 
				"username-" + randomUUID().toString();
		
		return createRandomUser(username);
	}
	
	private static final MUser
	createRandomUser(final String username)
	{
		final Set<String> roles = new HashSet<>();
		
		for (int i = 0; i < 5; i++)
		{
			roles.add("random role " + randomUUID().toString());
		}
		
		return new MUser(UUID.randomUUID(), false, roles);
	}
	
	private final Claims
	generateClaims(String ... excludedClaims)
	{
		MUser user = createRandomUser();
		
		final String token = this.tokenService.createToken(user, false);
		
		this.tokenService.verifyToken(token);
		
		/* check that we don't accept unsigned tokens */
		
		Claims claims = Jwts.parser()
							.setSigningKey(this.jwtConfig.signingKey())
							.parseClaimsJws(token)
							.getBody();
		
		Stream.of(excludedClaims)
				.forEach(claims :: remove);
		
		return claims;
	}
	
	private final String
	createToken(Claims claims)
	{
		return Jwts.builder()
					.setClaims(claims)
					.signWith(this.jwtConfig.signatureAlgorithm(),
								this.jwtConfig.signingKey())
					.compact();
	}
	
	
	@Test
	public void
	tokenRoundtrip()
	{
		MUser user = createRandomUser();
		
		final String token = this.tokenService.createToken(user, false);
		
		this.tokenService.verifyToken(token);
		
		UUID id = this.tokenService.parseUserID(token);
		
		Assert.assertEquals(user.id(), id);
		
	}
	
	@Test(expected = AccessDeniedException.class)
	public void
	verifyRejectsEmptyToken()
	{
		final String token = "";
		
		this.tokenService.verifyToken(token);
	}
	
	@Test
	public void
	parseRejectsEmptyToken()
	{
		final String token = "";
		
		UUID id = this.tokenService.parseUserID(token);
		
		Assert.assertNull(id);
	}
	
	@Test
	public void
	tokenSignatureCheck()
	{
		final String username = "test-username";
		
		MUser user = createRandomUser(username);
		
		final String token = this.tokenService.createToken(user, false);
		
		this.tokenService.verifyToken(token);
		
		/* check that we don't accept unsigned tokens */
		
		Claims claims = Jwts.parser()
							.setSigningKey(this.jwtConfig.signingKey())
							.parseClaimsJws(token)
							.getBody();

				
		String badToken = 
				Jwts.builder()
					.setClaims(claims)
					.compact();
		
		boolean fail = true;
		try
		{
			this.tokenService.verifyToken(badToken);
		}
		catch (AccessDeniedException e)
		{
			fail = false;
		}
		
		if (fail)
		{
			Assert.fail("Verification succeeded for token without signature");
		}
		
		
		
		/* check that we don't accept signed tokens that have been tampered with */
		
		String decoded = new String(TextCodec.BASE64URL.decode(token));
		
		badToken = decoded.replace(username, "evil-username");
		
		badToken = TextCodec.BASE64URL.encode(badToken);
		
		fail = true;
		try
		{
			this.tokenService.verifyToken(badToken);
		}
		catch (AccessDeniedException e)
		{
			fail = false;
		}
		
		if (fail)
		{
			Assert.fail("Verification succeeded for token with invalid signature");
		}
	}

	@Test(expected = AccessDeniedException.class)
	public void
	rejectExpiredToken()
	{
		MUser user = createRandomUser();
		
		String token = this.tokenService.createToken(user, false);
		
		try
		{
			this.tokenService.verifyToken(token);
		}
		catch (AccessDeniedException e)
		{
			/* if we failed verification here, the test is meaningless */
			Assert.fail();
		}
		
		/* check that we don't accept expired tokens */
		
		Claims claims = Jwts.parser()
							.setSigningKey(this.jwtConfig.signingKey())
							.parseClaimsJws(token)
							.getBody();
		
		token = Jwts.builder()
					.setClaims(claims)
					.setIssuedAt(Date.from(Instant.parse("2016-07-17T00:00:00.00Z")))
		            .setExpiration(Date.from(Instant.parse("2017-01-01T00:00:00.00Z")))
		            .signWith(this.jwtConfig.signatureAlgorithm(), 
		            			this.jwtConfig.signingKey())
		            .compact();
		
		this.tokenService.verifyToken(token);
			
	}
	
	@Test
	public void
	gracePeriodAcceptance()
	{
		MUser user = createRandomUser();
		
		String token = this.tokenService.createToken(user, false);
		
		try
		{
			this.tokenService.verifyToken(token);
		}
		catch (AccessDeniedException e)
		{
			/* if we failed verification here, the test is meaningless */
			Assert.fail();
		}
		
		
		Claims claims = Jwts.parser()
							.setSigningKey(this.jwtConfig.signingKey())
							.parseClaimsJws(token)
							.getBody();
		
		Instant now = Instant.now();
				
		token = Jwts.builder()
					.setClaims(claims)
					.setIssuedAt(Date.from(Instant.parse("2016-07-17T00:00:00.00Z")))
		            .setExpiration(Date.from(now))
		            .signWith(this.jwtConfig.signatureAlgorithm(), 
		            			this.jwtConfig.signingKey())
		            .compact();
		
		this.tokenService.verifyToken(token, 8600);
			
	}
	
	@Test(expected = AccessDeniedException.class)
	public void
	rejectMalformedToken_missingExpirationDate()
	{
		Claims claims = this.generateClaims();
		
		claims.setExpiration(null);
		
		final String token = this.createToken(claims);
		
		this.tokenService.verifyToken(token);
	}
	
	@Test(expected = AccessDeniedException.class)
	public void
	rejectMalformedToken_missingID()
	{
		Claims claims = 
				this.generateClaims(this.jwtConfig.claimPrefix() 
										+ this.jwtConfig.idClaimName());
				
		final String token = this.createToken(claims);
		
		this.tokenService.verifyToken(token);
		
	}
	
	@Test(expected = AccessDeniedException.class)
	public void
	rejectMalformedToken_missingRoles()
	{
		Claims claims = 
				this.generateClaims(this.jwtConfig.claimPrefix() 
										+ this.jwtConfig.rolesClaimName());
				
		final String token = this.createToken(claims);
		
		this.tokenService.verifyToken(token);
	}
	
	
}
