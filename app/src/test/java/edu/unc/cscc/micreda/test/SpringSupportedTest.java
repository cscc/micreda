package edu.unc.cscc.micreda.test;

import java.util.UUID;

import org.junit.ClassRule;
import org.junit.Rule;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.rules.SpringClassRule;
import org.springframework.test.context.junit4.rules.SpringMethodRule;

import com.fasterxml.uuid.Generators;
import com.fasterxml.uuid.NoArgGenerator;

import edu.unc.cscc.micreda.MicredaApplication;

@ContextConfiguration(classes = {MicredaApplication.class})
@SpringBootTest
@TestPropertySource(locations={"classpath:test.properties"})
public abstract class SpringSupportedTest
{
	/* support traditional JUnit annotations, i.e. what 
	 * SpringJUnit4ClassRunner supports
	 */
	@ClassRule
    public static final SpringClassRule 	SPRING_CLASS_RULE = new SpringClassRule();
    @Rule
    public final SpringMethodRule			springMethodRule = new SpringMethodRule();
    
    
    private static final NoArgGenerator		UUID_GENERATOR = 
			Generators.randomBasedGenerator();
    
    
    public static final UUID
    randomUUID()
    {
    	return UUID_GENERATOR.generate();
    }
}
