package edu.unc.cscc.micreda.test.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import edu.unc.cscc.micreda.model.MUser;
import edu.unc.cscc.micreda.service.UserService;
import edu.unc.cscc.micreda.test.SpringSupportedTest;

public class UserServiceTest
extends SpringSupportedTest
{
	
	private final Random	random = new Random();
	
	@Autowired
	private UserService		userService;
	
	
	private static final MUser
	createUser()
	{
		Set<String> roles = new HashSet<>();
		
		roles.add("random_role");
		roles.add("other_role");
		roles.add("third_role");
		
		IntStream.range(0, new Random().nextInt(10) + 5)
				.mapToObj(i -> "random-role-" + i + "," + UUID.randomUUID().toString())
				.forEach(roles :: add);
		
		return new MUser(null, false, roles);
	}
	
	@Test
	public void
	disableAndEnableUser()
	{
		MUser user = this.userService.save(createUser());

		Assert.assertFalse(user.disabled());
		
		user = user.disabled(true);
		
		MUser saved = this.userService.save(user);
		
		MUser loaded = this.userService.load(user.id());
		
		Assert.assertEquals(user, loaded);
		Assert.assertEquals(user, saved);
	}
	
	@Test
	public void
	exists()
	{
		MUser user = this.userService.save(createUser());
		
		Assert.assertTrue(this.userService.exists(user.id()));
	}
	
	@Test
	public void
	loadByID()
	{
		MUser user = this.userService.save(createUser());
		
		MUser found = this.userService.load(user.id());
		
		Assert.assertEquals(user, found);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void
	rejectEmptyRoles()
	{
		Set<String> roles = new HashSet<>();
		
		roles.add("good_role");
		
		roles.add("");
		
		MUser user = new MUser(null, false, roles);
		
		this.userService.save(user);
	}
	
	@Test
	public void
	search()
	{
		/* create some roles that we'll search for */
		Set<String> roles = new HashSet<>();
		
		for (int i = 0; i < this.random.nextInt(5) + 5; i++)
		{
			roles.add("random_role_" + i + "__" + UUID.randomUUID());
		}
		
		/* now create a bunch of users with all of those roles ... */
		
		Collection<MUser> allRoles = new ArrayList<>();
		
		for (int i = 0; i < this.random.nextInt(30) + 10; i++)
		{
			allRoles.add(new MUser(null, false, roles));
		}
		
		/* ... and some that only have a subset */
		
		Collection<MUser> someRoles = new ArrayList<>();
		for (int i = 0; i < this.random.nextInt(30) + 10; i++)
		{
			Set<String> subset = new HashSet<>();
			
			Iterator<String> ri = roles.iterator();
			
			/* give them either the first or the second */
			if (this.random.nextBoolean())
			{
				subset.add(ri.next());
				ri.next();
			}
			else
			{
				ri.next();
				subset.add(ri.next());
			}
			
			/* now assign them some portion of the remaining roles */
			while (ri.hasNext())
			{
				if (this.random.nextBoolean())
				{
					subset.add(ri.next());
				}
			}
			
			someRoles.add(new MUser(null, false, subset));
		}
		
		/* now save everybody */
		
		allRoles = allRoles.stream()
							.map(this.userService :: save)
							.collect(Collectors.toList());
		
		someRoles = someRoles.stream()
							.map(this.userService :: save)
							.collect(Collectors.toList());
		
		/* now find the users with all roles */
		
		Collection<MUser> found = 
				this.userService.search(roles, false, 10000);
		
		Assert.assertEquals(allRoles.size(), found.size());
		Assert.assertTrue(found.containsAll(allRoles));
		
		/* find anyone with any of our roles */
		
		found = this.userService.search(roles, true, 10000);
		
		Assert.assertEquals(allRoles.size() + someRoles.size(), found.size());
		Assert.assertTrue(found.containsAll(someRoles));
		Assert.assertTrue(found.containsAll(allRoles));
		
		/* finally, check that adding a role that nobody has empties the results... */
		
		Set<String> bogus = new HashSet<>(roles);
		bogus.add("nobody-has-this-role");
		
		found = this.userService.search(bogus, false, 1000);
		
		Assert.assertTrue(found.isEmpty());
		
		/* ... but doesn't empty the results for anyMatch */
		found = this.userService.search(bogus, true, 1000);
		
		Assert.assertEquals(allRoles.size() + someRoles.size(), found.size());
		Assert.assertTrue(found.containsAll(someRoles));
		Assert.assertTrue(found.containsAll(allRoles));
		
	}
	
	@Test
	public void
	resolveID()
	{		
		/* start with some basic sanity checks */
		
		Assert.assertNull(this.userService.resolveAbbreviatedID(""));
		Assert.assertNull(this.userService.resolveAbbreviatedID(" "));
		
		
		/* check that resolution flat-out rejects anything that's not hex-ish */
		Assert.assertNull(this.userService.resolveAbbreviatedID("!@#$"));
		Assert.assertNull(this.userService.resolveAbbreviatedID("a930z"));
		Assert.assertNull(this.userService.resolveAbbreviatedID("A 9F"));
		
		
		MUser user = this.userService.save(createUser());
		
		String abbreviated = user.id().toString().substring(0, 8);
		
		UUID resolved = this.userService.resolveAbbreviatedID(abbreviated);
		
		Assert.assertEquals(user.id(), resolved);
		
		/* now test that resolution fails with an ambiguous production.  To do
		 * that we'll need to create a bunch of users (such that we're likely
		 * to have some collisions.  This is because our service doesn't allow
		 * ID assignment by callers (probably a good thing...)
		 * 
		 * Note that this is potentially infinite if we're really unlucky...
		 */
		
		List<MUser> users = new ArrayList<>();
		
		while (users.stream()
					.filter(u -> u.id().toString().startsWith("4"))
					.count() < 2)
		{
			users.add(this.userService.save(createUser()));
		}
		
		resolved = this.userService.resolveAbbreviatedID("4");
		
		Assert.assertNull(resolved);
		
		
	}

}
