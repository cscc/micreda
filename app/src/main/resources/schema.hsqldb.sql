SET DATABASE TRANSACTION CONTROL MVCC;
DROP TABLE IF EXISTS "external_user_identities";
DROP TABLE IF EXISTS "internal_user_identities";
DROP TABLE IF EXISTS "internal_identity_sources";
DROP TABLE IF EXISTS "user_roles";
DROP TABLE IF EXISTS "users";


CREATE TABLE "users" (
	"id"				UUID PRIMARY KEY,
	"disabled"			BOOLEAN DEFAULT false NOT NULL
);


CREATE TABLE "user_roles" (
	"id__users"				UUID NOT NULL,
	"role_name"				LONGVARCHAR NOT NULL,

	UNIQUE ("id__users", "role_name"),
	FOREIGN KEY ("id__users") REFERENCES "users" ("id")
);

CREATE INDEX IF NOT EXISTS user_roles_fkey_idx ON "user_roles" ("id__users");
CREATE INDEX IF NOT EXISTS role_name_idx ON "user_roles" ("role_name");


-- identities

CREATE TABLE "internal_identity_sources" (
	"id"					UUID PRIMARY KEY NOT NULL,
	"identifier"			LONGVARCHAR NOT NULL,
	"description"			LONGVARCHAR NOT NULL,
	
	UNIQUE ("identifier")
);

CREATE TABLE "internal_user_identities" (
	"id"								UUID NOT NULL,
	"id__internal_identity_sources"		UUID NOT NULL,
	"username"							LONGVARCHAR NOT NULL,
	"password_hash"						LONGVARCHAR NOT NULL,
	"hash_name"							VARCHAR(128) DEFAULT 'bcrypt' NOT NULL 
		CHECK ("hash_name" IN ('bcrypt')),
	
	FOREIGN KEY ("id") REFERENCES "users" ("id"),
	FOREIGN KEY ("id__internal_identity_sources") 
		REFERENCES "internal_identity_sources" ("id"),
		
	UNIQUE ("username", "id__internal_identity_sources"),
	PRIMARY KEY ("id", "username")
);
CREATE INDEX IF NOT EXISTS internal_user_identities_fkey_idx
	ON "internal_user_identities" ("id");
CREATE INDEX IF NOT EXISTS internal_user_identities_fkey_source_idx
	ON "internal_user_identities" ("id__internal_identity_sources");


CREATE TABLE "external_user_identities" (
	"id"						UUID NOT NULL,
	"username"					LONGVARCHAR NOT NULL,
	"identity_source"			LONGVARCHAR NOT NULL,
	
	FOREIGN KEY ("id") REFERENCES "users" ("id"),
	
	UNIQUE ("username", "identity_source")
);

CREATE INDEX IF NOT EXISTS external_user_identities_fkey_idx
	ON "external_user_identities" ("id");
