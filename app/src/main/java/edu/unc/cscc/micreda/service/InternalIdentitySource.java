/*-
 * ========================LICENSE_START=================================
 * micreda
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.micreda.service;

import java.util.Collection;
import java.util.UUID;

import org.springframework.dao.DataIntegrityViolationException;

import com.fasterxml.jackson.annotation.JsonGetter;

import edu.unc.cscc.micreda.APIConstants;
import edu.unc.cscc.micreda.model.IdentitySource;
import edu.unc.cscc.micreda.model.MUser;
import edu.unc.cscc.micreda.model.UserIdentity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Identity source for {@link InternalIdentity internal users}, stores its
 * credentials and identity information within micreda's configured storage.
 * 
 * @author Rob Tomsick (rtomsick@unc.edu)
 *
 */
@ApiModel("internal-identity-source")
public interface InternalIdentitySource
extends IdentitySource
{	
	/**
	 * Get the ID of the identity source.
	 * 
	 * @return storage ID of the identity source
	 */
	@JsonGetter("id")
	@ApiModelProperty(value = "ID of the identity source", required = true,
						example = APIConstants.EXAMPLE_UUID)
	public UUID id();
	
	/**
	 * Save the given internal user identity with the given credentials.  The 
	 * identity will be created if it does not exist.
	 * 
	 * @param identity identity of user for which to save credentials
	 * @param credentials credentials of the given identity, 
	 * 	<b>NOT</b> encoded (aka plaintext)
	 * @return copy of the identity reflecting underlying storage
	 * @throws IllegalArgumentException thrown if the identity is associated
	 * with an unknown user or if the identity is managed by a different 
	 * identity source
	 * @throws DataIntegrityViolationException thrown if the identity is 
	 * associated with a different user than 
	 * {@link InternalIdentity#user() the specified user}
	 */
	public UserIdentity save(UserIdentity identity, String credentials)
		throws IllegalArgumentException, DataIntegrityViolationException;
	
	/**
	 * Delete the given internal user identity (and any stored credentials) if
	 * it exists in the source.
	 * 
	 * @param identity identity, not <code>null</code>
	 */
	public void delete(UserIdentity identity);
	
	/**
	 * Find the internal user identity with the given username in the identity
	 * source.
	 * 
	 * @param username username, not <code>null</code>, not empty
	 * @return identity with the given username, or <code>null</code> if no 
	 * such identity eixsts
	 */
	public UserIdentity find(String username);
	
	/**
	 * Determine whether a user with the given username exists in the 
	 * identity source.
	 * 
	 * @param username username to check, not <code>null</code>, not empty
	 * @return <code>true</code> if a user with the given username exists,
	 * <code>false</code> otherwise
	 */
	public boolean exists(String username);
	
	/**
	 * Find all identities known to the source associated with the given user.
	 * 
	 * @param user user for which to find identities, not <code>null</code>
	 * @return associated identities, not <code>null</code>
	 */
	public Collection<UserIdentity> find(MUser user);
	
	@Override
	default boolean isExternal()
	{
		return false;
	}
	
}
