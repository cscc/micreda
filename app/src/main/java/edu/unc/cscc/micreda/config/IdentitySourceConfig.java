/*-
 * ========================LICENSE_START=================================
 * micreda
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.micreda.config;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.jooq.DSLContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import edu.unc.cscc.micreda.model.ExternalIdentitySource;
import edu.unc.cscc.micreda.service.InternalIdentitySource;
import edu.unc.cscc.micreda.service.InternalIdentitySourceService;
import edu.unc.cscc.micreda.service.UserService;
import edu.unc.cscc.micreda.service.impl.InternalIdentitySourceServiceImpl;
import edu.unc.cscc.micreda.service.internal.ActiveExternalIdentitySourceHolder;

@Configuration
public class IdentitySourceConfig
{
	
	/* ====
	 * Pay attention here!
	 * 
	 * This configuration is rather odd because we're working around a logical
	 * cycle between the internal identity source service and the external 
	 * sources.
	 * 
	 * The problem is this:
	 * 
	 * * on startup, we must refuse to load external sources with an identifier
	 *   that is used by an internal source (i.e. one which is stored in our
	 *   DB)
	 *   
	 * * once initialized, the internal source service must refuse to create
	 *   sources which would conflict with known (loaded) external identity
	 *   sources
	 *   
	 * Naturally, this means that the ExternalIdentitySource loader/resolver
	 * shim and the InternalIdentitySourceService depend on one another (or 
	 * rather that the creation of the former depends on the latter and the
	 * constructor of the latter requires a reference to the former).
	 * 
	 * The workaround for this is to make a mutable implementation of the
	 * external source shim, use that to set up the internal source service
	 * (and as the candidate bean where the shim interface is specified), and
	 * then after init of the internal source service, to actually load 
	 * the external sources (checking against our newly-created internal source
	 * service) and set those as the backing map for the external source shim.
	 * 
	 */
	
	private final Logger								logger;
	private final MutableExternalIdentitySourceShim		shim;
	
	public IdentitySourceConfig()
	{
		this.logger = LoggerFactory.getLogger("identity-source-configuration");
		this.shim = new MutableExternalIdentitySourceShim(Collections.emptyMap());
	}
	
	@Bean
	public ActiveExternalIdentitySourceHolder
	externalShim()
	{
		return this.shim;
	}
	
	@Autowired
	public void
	configureExternalSources(EISLoader loader, 
								InternalIdentitySourceService internalService)
	throws IOException
	{
		
		final Map<String, ExternalIdentitySource> sources = loader.load();
		
		final Map<String, InternalIdentitySource> internalSources = 
				internalService.enumerateSources();
		
		/* check for name clashes, preferring internal sources */
		final Set<String> conflicts = new HashSet<>();
		
		internalSources.keySet()
						.stream()
						.filter(sources :: containsKey)
						.forEach(conflicts :: add);
		
		for (final String c : conflicts)
		{
			this.logger.error(String.format(
							"Refusing to use external source with identifier "
									+ "'%s'; conflicts with known "
									+ "internal source identifier",
									c));
			sources.remove(c);
		}
		
		
		this.shim.externalIdentitySources(sources);
	}
	
	@Bean
	@Autowired
	public InternalIdentitySourceService
	internalSourceCreator(@Value("${micreda.sources.internal.freeze}")
							boolean freezeInternalSources,
						DSLContext ctx, UserService userService)
	{
		return new InternalIdentitySourceServiceImpl(ctx, userService, 
													! freezeInternalSources,
													this.shim);
	}
	
	/* since our internal identity source service will depend on an 
	 * ExternalIdentitySourceShim to check for conflict, and the shim's creation
	 * requires checking the internal sources for conflicts, we need to do
	 * this to avoid an unsolvable cycle
	 */
	private static final class MutableExternalIdentitySourceShim
	implements ActiveExternalIdentitySourceHolder
	{

		private Map<String, ExternalIdentitySource> sources;
		
		public MutableExternalIdentitySourceShim(Map<String, ExternalIdentitySource> sources)
		{
			this.sources = new HashMap<>(sources);
		}
		
		@Override
		public Map<String, ExternalIdentitySource> 
		externalIdentitySources()
		{
			return Collections.unmodifiableMap(sources);
		}
		
		private void
		externalIdentitySources(Map<String, ExternalIdentitySource> sources)
		{
			this.sources = new HashMap<>(sources);
		}
		
	}
}
