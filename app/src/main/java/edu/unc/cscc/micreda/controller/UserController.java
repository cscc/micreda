/*-
 * ========================LICENSE_START=================================
 * micreda
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.micreda.controller;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.Collections;
import java.util.UUID;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import edu.unc.cscc.micreda.APIConstants;
import edu.unc.cscc.micreda.model.MUser;
import edu.unc.cscc.micreda.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

@Api(tags = "Users")
@RestController
@RequestMapping("/users")
@Validated
public class UserController
{
	@Autowired
	private UserService						userService;
		
	
	@RequestMapping(value = "/count",
					produces = MediaType.TEXT_PLAIN_VALUE,
					method = RequestMethod.GET)
	@ApiOperation(value = "get a count of the number of users known to the instance",
					notes = "Count the number of users known to the instance.  "
							+ "Includes all users, enabled and otherwise.")
	@ApiResponses({
		@ApiResponse(code = 200, message = "count was successful")
	})
	public long
	countUsers()
	{
		return this.userService.count();
	}
	
	
	@RequestMapping(value = "/",
					method = RequestMethod.POST,
					consumes = MediaType.APPLICATION_JSON_VALUE,
					produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "create a new user",
					notes = "Create a new user.  The resulting user will not "
							+ "have any identities associated with it.  Note that "
							+ "the user in the request body MUST NOT specify an ID.",
					code = 201)
	@ApiResponses({
		@ApiResponse(code = 201,
						message = "user created successfully; response "
								+ "reflects persisted user",
						response = MUser.class,
						responseHeaders = {
								@ResponseHeader(name = "Location", 
											description = "URI of newly-created user")
						}
		),
		@ApiResponse(code = 400, message = "malformed user" /* no jokes please... */)
		
	})
	public ResponseEntity<?>
	createUser(@RequestBody
				@ApiParam(value = "user to create, must NOT specify ID")
				final MUser user) 
	throws URISyntaxException
	{
		if (user.id() != null)
		{
			return ResponseEntity.badRequest()
								.body("cannot specify ID of new user");
		}
		
		MUser saved = this.userService.save(user);
		
		return ResponseEntity.created(new URI(null, null, saved.id().toString(), null))
								.body(saved);
	}
	
	@RequestMapping(value = "/{id}",
				method = RequestMethod.POST,
				consumes = MediaType.APPLICATION_JSON_VALUE,
				produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "update an existing user user",
		notes = "Update an existing user.  The user's identities will not be "
				+ "modified as part of this process.  It is not required to "
				+ "supply an ID for the given user, but if one is present "
				+ "on the user object it must match the ID supplied in the path.")
	@ApiResponses({
	@ApiResponse(code = 200,
			message = "user savedsuccessfully; response "
					+ "reflects persisted user",
			response = MUser.class),
	@ApiResponse(code = 400, message = "user was malformed (ID changed?)")
	
	})
	public ResponseEntity<?>
	saveUser(@PathVariable("id") 
			@NotNull
			@ApiParam(value = "ID of the user to update", 
						example = APIConstants.EXAMPLE_UUID)
				final UUID id, 
			@RequestBody 
			@Valid 
			@ApiParam(value = "user containing updated properties")
			final MUser user)
	{
		if (user.id() != null && ! id.equals(user.id()))
		{
			return ResponseEntity.badRequest()
								.body("cannot change ID of user");
		}
		
		/* assign user ID before save, since they either left it out (OK)
		 * or specified the one from the path (in which case we overwrite 
		 * with same)
		 */
		MUser saved = this.userService.save(user.id(id));
		
		return ResponseEntity.ok(saved);
	}
	
	@RequestMapping(value = "/{id}",
					method = RequestMethod.GET,
					produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "retrieve a user",
					notes = "Get the user with the given ID.")
	@ApiResponses({
		@ApiResponse(code = 200, message = "user retrieved successfully",
						response = MUser.class),
		@ApiResponse(code = 404, message = "user not found")
	})
	public ResponseEntity<?>
	getUser(@PathVariable("id")
			@NotNull 
			@ApiParam(value = "ID of user to retrieve", 
						example = APIConstants.EXAMPLE_UUID)
				final UUID id)
	{
		MUser user = this.userService.load(id);
		
		if (user != null)
		{
			return new ResponseEntity<>(user, HttpStatus.OK);
		}
		
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
	
	@RequestMapping(value = "/by-short-id/{abbr-id}",
					method = RequestMethod.GET,
					produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "load user by short ID",
					notes = "Load a user by supplying an abbreviated ID.  "
							+ "Similar in concept to git's short commit IDs, "
							+ "a user may be loaded by provision of a unique "
							+ "starting subsequence of their ID.  This "
							+ "method of retrieval will ONLY work if the given "
							+ "sequence is unique.  No provision is nor will be "
							+ "made for loading more than one user via provision "
							+ "of a non-unique sequence.",
					code = 302)
	@ApiResponses({
		@ApiResponse(code = 404, 
						message = "no such user was found and/or ID abbreviation "
								+ "was non-unique"),
		@ApiResponse(code = 302, 
					message = "user identified; Location header provides "
							+ "canonical URI for user")
	})
	public ResponseEntity<UUID>
	loadByAbbreviatedID(@PathVariable("abbr-id")
						@Size(min = 1)
						@ApiParam(value = "abbreviated ID", example = "3dd6f")
							final String abbr)
	{
		UUID id = this.userService.resolveAbbreviatedID(abbr);
		
		if (id == null)
		{
			return ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.status(HttpStatus.FOUND)
								.header("Location", "../" + id.toString())
								.body(id);
	}
	
	
	/**
	 * List up to a given number of users known to the instance, 
	 * optionally restricting by roles. May or may not include identity 
	 * information.
	 * 
	 * @param roleFilters role filters (may be empty)
	 * @return
	 */
	@RequestMapping(value = "/search",
					method = RequestMethod.GET,
					produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "find users within an instance",
					notes = "List up to a given umber of users known to the "
							+ "instance, optionally restricting by roles.")
	public Collection<MUser>
	list(@RequestParam(value = "role", required = false)
			@Size(max = 128)
			@ApiParam(value = "roles which must be held by any returned users",
						required = false)
			final Collection<String> roleFilters, 
			
			@RequestParam(value = "match-all", 
							required = false, 
							defaultValue = "true")
			@ApiParam(value = "Whether returned users must hold all specified, or "
							+ "at least one.  This option is ignored if roles "
							+ "are not specified.", required = false)
			final boolean matchAll,
			
			@RequestParam(value = "limit",
							required = false, 
							defaultValue = "100")
			@Min(1) @Max(1000)
			@ApiParam(value = "maximum number of users to return", 
						required = false, defaultValue = "100")
				final int limit)
	{	
		return this.userService.search(roleFilters == null
											? Collections.emptyList() 
											: roleFilters,
										! matchAll, limit);
	}
	
	
	
	
}


