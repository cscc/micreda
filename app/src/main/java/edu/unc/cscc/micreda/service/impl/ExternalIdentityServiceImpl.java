/*-
 * ========================LICENSE_START=================================
 * micreda
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.micreda.service.impl;

import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.name;

import java.util.Collection;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.Table;
import org.jooq.impl.DSL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import edu.unc.cscc.micreda.model.IdentitySource;
import edu.unc.cscc.micreda.model.MUser;
import edu.unc.cscc.micreda.model.UserIdentity;
import edu.unc.cscc.micreda.service.ExternalIdentityService;
import edu.unc.cscc.micreda.service.UserService;

@Service
public class ExternalIdentityServiceImpl 
implements ExternalIdentityService
{

	private static final Table<?>		EXT_USER_TABLE = 
			DSL.table(DSL.name("external_user_identities"));
	
	private final DSLContext			ctx;
	private final UserService			userService;
	
	
	@Autowired
	public ExternalIdentityServiceImpl(DSLContext ctx, UserService userService)
	{
		this.ctx = ctx;
		this.userService = userService;
	}

	@Override
	@Transactional
	public UserIdentity 
	create(UserIdentity identity)
	{
		if (identity.user() == null
			|| identity.user().id() == null)
		{
			throw new IllegalArgumentException(
					"identity must be associated with existing user");
		}
		
		if (! IdentitySource.validIdentifier(identity.identitySourceIdentifier()))
		{
			throw new IllegalArgumentException("invalid identity source identifier");
		}
		
		final UserIdentity existing = 
				this.find(identity.username(), identity.identitySourceIdentifier());
		
		
		if (null != existing
			&& ! identity.user().id().equals(existing.user().id()))
		{
			throw new DataIntegrityViolationException(
					String.format("username/identity source already in use "
									+ "by user (id %s)", 
									existing.user().id().toString()));
		}
		
		int affected = 
				this.ctx
					.insertInto(EXT_USER_TABLE)
						.set(field(name("username")), identity.username())
						.set(field(name("identity_source")), 
								identity.identitySourceIdentifier())
						.set(field(name("id")), identity.user().id())
					.execute();
	
		Assert.state(affected == 1, "affected <> 1 rows");
		
		return identity;	
	}
	
	@Override
	@Transactional
	public void 
	delete(UserIdentity identity)
	{
		this.ctx.deleteFrom(EXT_USER_TABLE)
				.where(DSL.field(name("id")).eq(identity.user().id()))
				.and(field(name("username")).eq(identity.username()))
				.and(field(name("identity_source")).eq(identity.identitySourceIdentifier()))
				.execute();
	}
	
	@Override
	public UserIdentity 
	find(String username, String identitySourceID)
	{
		return this.ctx
				.selectFrom(EXT_USER_TABLE)
				.where(field(name("username"), String.class)
						.eq(username))
				.and(field(name("identity_source"), String.class)
						.eq(identitySourceID))
				.fetchOne(u -> {
					final UUID id = u.get(field(name("id"), UUID.class));
					
					return new UserIdentity(this.userService.load(id), 
													username,
													identitySourceID);
				});
	}

	@Override
	public Collection<UserIdentity>
	search(String query)
	{
		return this.search(query, null);
	}

	@Override
	public Collection<UserIdentity>
	search(String query, String identitySourceID)
	{
		final Condition queryCond = 
						(! StringUtils.isEmpty(query))
							? field(name("username"), String.class)
									.contains(query)
							: DSL.trueCondition();
									
		final Condition idsCond = 
						(! StringUtils.isEmpty(identitySourceID))
							? field(name("identity_source"), String.class)
									.eq(identitySourceID)
							: DSL.trueCondition();
		return this.ctx
				.selectFrom(EXT_USER_TABLE)
				.where(queryCond)
				.and(idsCond)
				.fetch(u -> {
					final UUID id = u.get(field(name("id"), UUID.class));
					
					return new UserIdentity(this.userService.load(id), 
													u.get(field(name("username"), String.class)),
													u.get(field(name("identity_source"), String.class)));
				});
	}

	@Override
	public Collection<UserIdentity> 
	findByUser(MUser user)
	{
		return this.ctx
				.selectFrom(EXT_USER_TABLE)
				.where(field(name("id"), UUID.class).eq(user.id()))
				.fetch(u -> 					
					new UserIdentity(user,
							u.get(field(name("username"), String.class)),
							u.get(field(name("identity_source"), String.class)))
				);
	}

	@Override
	public Collection<String>
	searchIdentifiers(String query, int limit)
	{
		Assert.state(limit > 0, "limit must be > 0");
		
		Condition qc = StringUtils.isEmpty(query)
						? DSL.trueCondition()
						: field(name("identity_source"), String.class)
							.startsWith(query);
		
		return this.ctx
					.select(field(name("identity_source"), String.class))
					.from(EXT_USER_TABLE)
					.where(qc)
					.limit(limit)
					.fetch(field(name("identity_source"), String.class));
	}
	
	
}
