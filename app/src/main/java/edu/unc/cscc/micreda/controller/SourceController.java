/*-
 * ========================LICENSE_START=================================
 * micreda
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.micreda.controller;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;

import edu.unc.cscc.micreda.model.ExternalIdentitySource;
import edu.unc.cscc.micreda.model.IdentitySource;
import edu.unc.cscc.micreda.service.ExternalIdentityService;
import edu.unc.cscc.micreda.service.IdentityResolver;
import edu.unc.cscc.micreda.service.InternalIdentitySource;
import edu.unc.cscc.micreda.service.InternalIdentitySourceService;
import edu.unc.cscc.micreda.service.internal.ActiveExternalIdentitySourceHolder;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

@Api(tags = "Identity Sources")
@RestController
@RequestMapping(value = "/identity-sources")
public class SourceController
{

	@Autowired
	private IdentityResolver					resolver;
		
	@Autowired
	private InternalIdentitySourceService		iiss;
	
	@Autowired
	private ActiveExternalIdentitySourceHolder	eshim;
	
	@Autowired
	private ExternalIdentityService				eis;
	

	@RequestMapping(value = "/",
					produces = MediaType.APPLICATION_JSON_VALUE,
					method = RequestMethod.GET)
	@ApiOperation(value = "enumerate internal and/or external identity sources",
					notes = "Enumerate the internal and/or external identity sources "
							+ "currently configured within the instance.  Only "
							+ "those external identity sources currently active "
							+ "within the instance will be considered.",
					response = IdentitySource.class,
					responseContainer = "Set")
	public Collection<IdentitySource>
	identitySources(@RequestParam(name = "internal",
									required = false, defaultValue = "true")
					@ApiParam(name = "internal", 
								value = "whether to enumerate internal identity sources",
								required = false, defaultValue = "true")
						final boolean internal,
					@RequestParam(name = "external",
									required = false, defaultValue = "true")
					@ApiParam(name = "external",
								value = "whether to enumerate external identity sources",
								required = false, defaultValue = "true")
					final boolean external)
	{
		
		final Collection<IdentitySource> sources = new ArrayList<>();
		
		if (internal)
		{
			sources.addAll(this.iiss.enumerateSources().values());
		}
		
		if (external)
		{
			sources.addAll(this.eshim.externalIdentitySources().values());
		}
		
		return sources;
	}
	

	@RequestMapping(value = "/internal/",
					method = RequestMethod.POST,
					consumes = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "create or update an internal identity source",
					notes = "Create or update an internal identity source.  "
							+ "Attempts to create/update an internal identity "
							+ "source with the same identifier as a known external "
							+ "identity source will fail.  The identifier of the "
							+ "given source will be used as the key by which to "
							+ "differentiate update from creation.")
	@ApiResponses({
		@ApiResponse(code = 200, 
						message = "source saved successfully; 'Location' "
								+ "header contains canonical URL for new/updated "
								+ "source; response body contains current state",
						response = InternalIdentitySource.class,
						responseHeaders = {
								@ResponseHeader(name = "Location", 
											description = "canonical URL for new/updated source")}),
		@ApiResponse(code = 409, 
					message = "identifier in use by an active external source")
	})
	public ResponseEntity<InternalIdentitySource>
	saveInternalSource(@RequestBody 
						@Valid @NotNull
						@ApiParam(value = "new/updated internal identity source")
						final InternalDataOnlyIdentitySource source)
	throws MalformedURLException, URISyntaxException
	{
		if (this.eshim.externalIdentitySources().containsKey(source.identifier()))
		{
			return ResponseEntity.status(HttpStatus.CONFLICT).build();
		}
		
		/* no external source w/ that ID so we're working on an internal one
		 */
		
		final InternalIdentitySource found = this.iiss.find(source.identifier());
		
		final InternalIdentitySource saved = 
				this.iiss.save(source.identifier(), source.description());
		
		if (found == null)
		{
			return ResponseEntity.created(new URI(null, null, "internal/" + saved.identifier(), null))
									.body(saved);
		}
		
		return ResponseEntity.ok()
								.header("Location", 
										"./internal/" + saved.id())
								.body(saved);
	}
	
	@RequestMapping(value = "/internal/{id}",
					method = RequestMethod.GET,
					produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "retrieve an internal source by ID",
					notes = "Get the internal identity source with the given ID.",
					response = InternalIdentitySource.class)
	@ApiResponses({
		@ApiResponse(code = 200, message = "identity source successfully retrieved"),
		@ApiResponse(code = 404, message = "no such source found")
	})
	public ResponseEntity<InternalIdentitySource>
	getInternalSource(@PathVariable("id")
						@NotNull
						@ApiParam(value = "ID of internal identity source")
						final UUID id)
	{
		final InternalIdentitySource source = this.iiss.load(id);
		
		if (source == null)
		{
			return ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.ok(source);
	}
	
	
	@RequestMapping(value = "/external/by-identifier",
				method = RequestMethod.GET,
				produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "get an external identity source by identifier",
					notes = "Get the external identity source with the specified "
						+ "identifier.")
	@ApiResponses({
		@ApiResponse(code = 200, message = "identity source successfully retrieved"),
		@ApiResponse(code = 404, message = "no such source found")
	})
	public ResponseEntity<ExternalIdentitySource>
	getExternalSource(@RequestParam(value = "identifier", required = true)
						@NotNull @Size(min = 1)
						@ApiParam(value = "identifier of source to retrieve", 
									required = true, example = "my-ext-src_2")
						final String identifier)
	{
		ExternalIdentitySource source = 
				this.eshim.externalIdentitySources().get(identifier);
		
		if (source == null)
		{
			return ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.ok(source);
	}
	
	@RequestMapping(value = "/internal/by-identifier",
					method = RequestMethod.GET,
					produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "get an internal identity source by identifier",
					notes = "Get the internal identity source with the specified "
						+ "identifier.")
	@ApiResponses({
		@ApiResponse(code = 200, message = "identity source successfully retrieved"),
		@ApiResponse(code = 404, message = "no such source found")
	})
	public ResponseEntity<InternalIdentitySource>
	getInternalSource(@RequestParam(value = "identifier", required = true)
						@NotNull @Size(min = 1)
						@ApiParam(value = "identifier of source to retrieve", 
									required = true, example = "my-int-src_1")
						final String identifier)
	{
		final InternalIdentitySource source = this.iiss.find(identifier);
		
		if (source == null)
		{
			return ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.ok(source);
	}
	
	@RequestMapping(value = "/all/by-identifier",
				method = RequestMethod.GET,
				produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "get an identity source by identifier",
					notes = "Get the internal OR external identity source "
							+ "with the specified identifier.")
	@ApiResponses({
		@ApiResponse(code = 200, message = "identity source successfully retrieved"),
		@ApiResponse(code = 404, message = "no such source found")
	})
	public ResponseEntity<IdentitySource>
	getSource(@RequestParam(value = "identifier", required = true)
				@NotNull @Size(min = 1)
				@ApiParam(value = "identifier of source to retrieve", 
							required = true, example = "my-ident-src_1")
				final String identifier)
	{
		IdentitySource source = 
				this.resolver.resolveSource(identifier);
		
		if (source == null)
		{
			return ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.ok(source);
	}
	
	@RequestMapping(value = "/all/",
			method = RequestMethod.GET,
			params = {"query"},
			produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "search for identity sources (internal and/or external)",
					notes = "Search for identitiy sources (internal and/or external) "
							+ "optionally including those sources that are known "
							+ "but inactive.")
	@ApiResponses({
		@ApiResponse(code = 200, message = "search succeded",
						response = IdentitySource.class,
						responseContainer = "List")
	})
	public Collection<IdentitySource>
	search(@RequestParam(value = "query",
						required = false)
			@Size(min = 1)
			@ApiParam(value = "query term; omit to enumerate all known sources", 
						required = false, example = "ldap")
				final String query,
				
			@RequestParam(value = "include-inactive-external",
							required = false,
							defaultValue = "true")
			@ApiParam(value = "include inactive external sources", 
						defaultValue = "true")
				final boolean ici,
				
			@RequestParam(value = "include-external",
							required = false,
							defaultValue = "true")
			@ApiParam(value = "include external sources", defaultValue = "true")
				final boolean includeExternal,
				
			@RequestParam(value = "include-internal",
							required = false,
							defaultValue = "true")
			@ApiParam(value = "include internal sources", defaultValue = "true")
				final boolean includeInternal,
				
			@RequestParam(value = "limit",
							required = false,
							defaultValue = "100")
			@ApiParam(value = "maximum number of results", defaultValue = "100")
			@Min(1) @Max(1000)
				final int limit)
	{	
		
		/* some quick arg sanity checking */
		final boolean includeInactive = (! includeExternal && ici) ? false : ici;
		
		
		Collection<IdentitySource> results = new ArrayList<>();
		
		/* check for internal sources */
		if (includeInternal)
		{
			results.addAll(this.iiss.search(query, limit));
		}
		
		/* what about external? */
		if (includeExternal)
		{
			final Set<String> identifiers = 
					new HashSet<>(this.eis.searchIdentifiers(query, limit - results.size()));
			
			this.eshim.externalIdentitySources()
						.entrySet()
						.stream()
						.filter(e -> e.getKey().contains(query))
						.map(e -> e.getValue().identifier())
						.forEach(identifiers :: add);
			
			/* at this point, identifiers has the identifiers of all EISs, both
			 * active and inactive, so now we filter based on whether we care
			 * about inactive
			 */
			Map<String, ExternalIdentitySource> activeSources = 
					this.eshim
						.externalIdentitySources();
			
			identifiers.stream()
						.filter(identifier -> 
									includeInactive 
									|| activeSources.containsKey(identifier))
						.map(identifier -> {
							
							ExternalIdentitySource source = 
										activeSources
										.get(identifier);
							
							if (source == null)
							{
								source = new InactiveExternalIdentitySource(identifier);
							}
							
							return source;
						})
						.forEach(results :: add);
		}
		
		return results;
	}
	
	/**
	 * Intended to allow deserialziation of {@link IdentitySource}s (of which
	 * we don't care about the details.)
	 * 
	 * @author Rob Tomsick (rtomsick@unc.edu)
	 *
	 */
	private static final class InternalDataOnlyIdentitySource
	implements IdentitySource
	{
		
		private final String		identifier;
		private final String		description;
		
		
		@JsonCreator
		public InternalDataOnlyIdentitySource(@JsonProperty("identifier") String identifier, 
										@JsonProperty(required = false,
														defaultValue = "",
														value = "description") 
										String description)
		{
			this.identifier = identifier;
			this.description = (description == null ? "" : description);
		}

		@Override
		public String identifier()
		{
			return this.identifier;
		}
		
		@Override
		public String description()
		{
			return this.description;
		}
		
		@Override
		public boolean authenticate(String username, String credentials)
		{
			throw new IllegalStateException("Not implemented");
		}
		
		/* sham setters because swagger devs don't understand inference 
		 * (swagger-core bug 2169 and 2379)
		 */
		@JsonSetter("identifier")
		private void HACK_identifier(String identifier){}
		@JsonSetter("description")
		private void HACK_description(String description){}
		
	}
	
	
	private static final class InactiveExternalIdentitySource
	implements ExternalIdentitySource
	{

		private final String identifier;
		
		public InactiveExternalIdentitySource(String identifier)
		{
			this.identifier = identifier;
		}

		@Override
		public String 
		identifier()
		{
			return this.identifier;
		}

		@Override
		public String
		description()
		{
			return "";
		}

		@Override
		public boolean
		authenticate(String username, String credentials)
		{
			return false;
		}

		@Override
		public boolean
		isActive()
		{
			return false;
		}
		
		@JsonSetter("identifier")
		private void HACK_identifier(String identifier){}
		@JsonSetter("description")
		private void HACK_description(String description){}
		
	}
}
