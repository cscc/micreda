/*-
 * ========================LICENSE_START=================================
 * micreda
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.micreda.config;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.ServiceLoader;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import edu.unc.cscc.micreda.model.Discoverable;
import edu.unc.cscc.micreda.model.ExternalIdentitySource;
import edu.unc.cscc.micreda.model.ExternalIdentitySourceFactory;
import edu.unc.cscc.micreda.model.ExternalIdentitySourceFactory.ExternalIdentitySourceConfigurationException;
import io.github.lukehutch.fastclasspathscanner.FastClasspathScanner;
import io.github.lukehutch.fastclasspathscanner.scanner.ScanResult;

@Service
class EISLoader
{

	private static final Logger		LOG = 
			LoggerFactory.getLogger("external-identity-sources");
	
	private static final String		SERVICE_DEF_PATH = "META-INF/services/";
	
	
	private final List<String>		additionalPaths;
	
	@Autowired
	public EISLoader(@Value("${micreda.sources.external.path}") String pathStr)
	{
		this.additionalPaths = new ArrayList<>();
		
		if (pathStr == null || pathStr.trim().isEmpty())
		{
			return;
		}
		
		String[] paths = pathStr.split("\\Q;\\E");
		
		for (final String path : paths)
		{
			this.additionalPaths.add(path);
		}
	}
	
	private List<File>
	searchPaths()
	{
		return this.additionalPaths
					.stream()
					.map(p -> new File(p))
					.collect(Collectors.toList());
	}
	
	private URLClassLoader
	createLoader()
	{
		final List<URL> urls = new ArrayList<>();
		
		for (final File f : this.searchPaths())
		{
			if (! f.canRead())
			{
				LOG.error(String.format(
						"Unable to read search path '%s', skipping", 
						f.getAbsolutePath()));
				continue;
			}
			
			try
			{
				urls.add(f.toURI().toURL());
			}
			catch (MalformedURLException e)
			{
				LOG.error(String.format("Skipping malformed path '%s'",
							f.toURI().toString()));
			}
		}
		
		return new URLClassLoader(urls.toArray(new URL[0]), 
									this.getClass().getClassLoader()); 
	}
	
	/**
	 * Discover and load all external identity sources accessible to the 
	 * instance (via direct instantiation or via {@link ExternalIdentitySourceFactory factories})
	 * 
	 * @return sources
	 * @throws IOException
	 */
	final Map<String, ExternalIdentitySource>
	load()
	throws IOException
	{
		/* first, find all our factory classes */
		
		final ClassLoader loader = this.createLoader();
				
		final Collection<Class<? extends ExternalIdentitySourceFactory>> factoryClasses = 
				discoverImplementations(ExternalIdentitySourceFactory.class, loader);
		
		
		/* now instantiate the factories */
		
		final List<ExternalIdentitySourceFactory> factories = new ArrayList<>();
		
		factoryClasses
			.stream()
			.map(cls -> {
				try
				{
					return cls.newInstance();
				}
				catch (InstantiationException | IllegalAccessException e)
				{
					throw new RuntimeException(e);
				}
			})
			.forEach(factories :: add);
		
		
		
		/* finally, add our internal factory (for EIS implementations which
		 * aren't managed by an external factory)
		 */
		factories.add(this.internalFactory(loader));
		
		/* now process the sources from the various factories */
		
		
		final Map<String, ExternalIdentitySource> sources = new HashMap<>();
		
		
		/* now load sources from each factory, check them, and add them to our
		 * map
		 */
		for (final ExternalIdentitySourceFactory factory : factories)
		{
			Collection<ExternalIdentitySource> factorySources;
			try
			{
				final Logger logger = 
						LoggerFactory.getLogger(
								"external-source-factory_"
									+ factory.getClass().getCanonicalName());
				factorySources = factory.obtainSources(logger);
			}
			catch (RuntimeException
					| ExternalIdentitySourceConfigurationException e)
			{
				LOG.error(String.format(
						"Failed to obtain sources from factory '%s', skipping."
						+ "  Exception: ", 
							factory.getClass().getCanonicalName()), e);
				continue;
			}
			
			for (final ExternalIdentitySource source : factorySources)
			{
				if (source.identifier() == null
					|| source.identifier().trim().isEmpty())
				{
					LOG.error("Refusing to load external source with empty ID");
					continue;
				}
				
				/* Do a quick on-load check against dumb sources.  Doesn't prevent 
				 * them from lying about being external later on, but at least 
				 * it will filter out some simple forms of dumb.
				 */
				if (! source.isExternal())
				{
					LOG.error(
							"Refusing to load external source (ID '" 
							+ source.identifier() + "') that lies about "
							+ "whether it's external.");
					continue;
				}
				
				
				
				sources.put(source.identifier(), source);
			}
		}
		
		return sources;
	}
	
	/**
	 * Produce a factory that will produce  
	 * {@link ExternalIdentitySource external identity sources} that have been
	 * loaded via ServiceLoader.
	 * 
	 * @return factory
	 * @throws IOException 
	 */
	private final ExternalIdentitySourceFactory
	internalFactory(ClassLoader loader)
	throws IOException
	{
		
		Collection<Class<? extends ExternalIdentitySource>> implementationClasses = 
				discoverImplementations(ExternalIdentitySource.class, loader);
				
		/* now instantiate all our classes (service decs + discoverable) */
		
		final List<ExternalIdentitySource> sources = 
					implementationClasses
					.stream()
					.map(cls -> {
						try
						{
							return cls.newInstance();
						}
						catch (InstantiationException | IllegalAccessException e)
						{
							throw new RuntimeException(e);
						}
					})
					.collect(Collectors.toList());
		
		
		return new ExternalIdentitySourceFactory()
		{
			
			@Override
			public Collection<ExternalIdentitySource>
			obtainSources(Logger logger)
			{
				return sources;
			}
		};
	}
	
	private static final List<String>
	parseImplementationNames(InputStream resource)
	throws IOException
	{
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(resource)))
		{
			return reader.lines().collect(Collectors.toList());
		}
	}
	
	/**
	 * Discover implementations of the specified class that are either 
	 * declared in accordance with {@link ServiceLoader the Java SPI conventions}
	 * or are annotated with {@link Discoverable}.
	 * 
	 * @param cls interface for which to find implementations
	 * @param loader classloader to use for loading classes, or <code>null</code>
	 *  to use the loader used to load the given class
	 * @return all discovered classes implementing the specified interface
	 *  
	 * @throws IOException
	 */
	public static final <T> Collection<Class<? extends T>> 
	discoverImplementations(final Class<T> cls, final ClassLoader l)
	throws IOException
	{
		
		final ClassLoader loader = 
				(l == null) ? cls.getClassLoader() : l;
		
		final Enumeration<URL> urls = 
				loader.getResources(SERVICE_DEF_PATH + cls.getCanonicalName());
		
		final Set<String> classNames = new HashSet<>();
		
		while (urls.hasMoreElements())
		{
			classNames.addAll(parseImplementationNames(urls.nextElement().openStream()));
		}
		
		FastClasspathScanner scanner = 
				new FastClasspathScanner("-org.springframework");
		
		/* add loader if we're using non-default one */
		if (l != null)
		{
			scanner.addClassLoader(loader);
		}
		
		ScanResult result = scanner.scan();
		
		/* get any discoverable implementations */
		
		Set<String> discoverableNames = 
				new HashSet<>(result.getNamesOfClassesWithAnnotation(Discoverable.class));
		
		result.getNamesOfClassesImplementing(cls)
				.stream()
				.filter(discoverableNames :: contains)
				.forEach(classNames :: add);
				
		/* now instantiate all our classes (service decs + discoverable) */
		
		return classNames
				.stream()
				.map(t -> {
					try
					{
						return (Class<? extends T>) loader.loadClass(t).asSubclass(cls);
					}
					catch (ClassNotFoundException e)
					{
						throw new RuntimeException(e);
					}
				})
				.collect(Collectors.toList());	
	}
}
