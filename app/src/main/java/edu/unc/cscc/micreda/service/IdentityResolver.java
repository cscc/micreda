/*-
 * ========================LICENSE_START=================================
 * micreda
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.micreda.service;

import java.util.Collection;

import edu.unc.cscc.micreda.model.IdentitySource;
import edu.unc.cscc.micreda.model.MUser;
import edu.unc.cscc.micreda.model.UserIdentity;

/**
 * Resolves a user identity (internal or external) from a 
 * username/identity source ID tuple.
 * 
 * <p>
 * Note: This is effectively a shim to prevent callers from having to figure out
 * the difference between internal/external identities.  Why not just have 
 * the identity source itself do this?  Because the identity source shouldn't
 * have any notion of MUsers, as it may or may not even know that this 
 * application exists.  In order to keep implementation of the identity sources
 * as lightweight (and thus as flexible as possible) no assumption of 
 * bi-directional associations is made.  This shim allows for such decoupling.
 * </p>
 * 
 * @author Rob Tomsick (rtomsick@unc.edu)
 */
public interface IdentityResolver
{
	/**
	 * Resolve a user identity from the given information.  Will consider
	 * all identity sources currently available to the application.
	 * 
	 * @param username username, not <code>null</code>
	 * @param identitySourceID ID of identity source, not <code>null</code>
	 * @return user identity matching the given information, or 
	 * <code>null</code> if such a user or identity source does not exist
	 */
	public UserIdentity resolve(String username, String identitySourceID);
	
	/**
	 * Resolve the identity source with the given identifier.  The resulting
	 * source may be external or internal.
	 * 
	 * @param identifier identifier of the source to resolve, not <code>null</code>
	 * @return identity source with the given identifier or <code>null</code>
	 * if no such source is known
	 */
	public IdentitySource resolveSource(String identifier);
	
	/**
	 * Enumerate all identities, internal and external, that are known for the
	 * given user.
	 * 
	 * @param user user
	 * @return identities, not <code>null</code>
	 */
	public Collection<UserIdentity> enumerateIdentities(MUser user);
	
}
