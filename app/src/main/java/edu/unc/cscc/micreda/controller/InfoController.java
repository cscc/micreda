/*-
 * ========================LICENSE_START=================================
 * micreda
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.micreda.controller;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import edu.unc.cscc.micreda.APIConstants;
import edu.unc.cscc.micreda.config.WebSecurityConfig.AdminRoleHolder;
import edu.unc.cscc.micreda.model.MUser;
import edu.unc.cscc.micreda.model.configuration.DatabaseInfo;
import edu.unc.cscc.micreda.model.configuration.MicredaEndpointInfo;
import edu.unc.cscc.micreda.service.InternalIdentitySourceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = "Instance Status")
@RestController
public class InfoController
{
	@Value("${micreda.artifact.version}")
	private String								version;
	
	@Autowired
	private AdminRoleHolder						holder;
	
	@Autowired
	private DatabaseInfo						dbInfo;
	
	@Autowired
	private InternalIdentitySourceService		iiss;

	@RequestMapping(value = "/info",
					method = RequestMethod.GET,
					produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "get information on the instance",
					notes = "Get information about the running micreda instance. "
							+ "The resulting information will indicate the "
							+ "API version supported by the instance and the "
							+ "version of micreda that the instance is running. "
							+ "It may also include additional information depending "
							+ "on configuration and/or the access level of the "
							+ "requesting user.")
	@ApiResponses({
		@ApiResponse(code = 200, message = "request succeeded")
	})
	public MicredaEndpointInfo
	get(@AuthenticationPrincipal MUser user)
	throws UnknownHostException
	{
		MicredaEndpointInfo info =
				new MicredaEndpointInfo()
					.hostname(InetAddress.getLocalHost().getHostName())
					.version(this.version)
					.apiVersion(APIConstants.VERSION)
					.allowInternalSourceCreation(this.iiss.allowsCreation());
		
		if (user != null 
			&& user.roles().contains(holder.role()))
		{
			info = info.databaseInfo(this.dbInfo);
		}
		
		return info;
					
	}
	
}
