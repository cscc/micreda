/*-
 * ========================LICENSE_START=================================
 * micreda
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.micreda.security;

import java.io.IOException;
import java.util.function.Supplier;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.Assert;
import org.springframework.web.filter.OncePerRequestFilter;

/**
 * A filter which, once configured, obtains and sets a supplied {@link Authentication}
 *  for any request which has a header key/value which match configured values.
 *  May be instructed to do nothing if an existing authentication is associated
 *  with the request.
 *  Will restore the context's existing authentication after successful 
 *  invocation of the next filter in the chain.
 *  
 * @author Rob Tomsick (rtomsick@unc.edu)
 *
 */
public class MagicHeaderFilter
extends OncePerRequestFilter
{
	private final boolean					overrideExistingAuthentication;
	private final Supplier<Authentication>	authSupplier;
	private final String					headerName;
	private final String					headerValue;
	private final boolean					checkValue;
	
	
	
	protected MagicHeaderFilter(Supplier<Authentication> authSupplier,
								String headerName, String headerValue,
								boolean overrideExistingAuthentication,
								boolean checkValue)
	{
		Assert.state(! checkValue || headerValue != null, 
						"if header value null, must not check");
		this.authSupplier = authSupplier;
		this.headerName = headerName;
		this.headerValue = headerValue;
		this.overrideExistingAuthentication = overrideExistingAuthentication;
		this.checkValue = checkValue;
	}



	@Override
	protected void
	doFilterInternal(HttpServletRequest request, HttpServletResponse response, 
					FilterChain filterChain)
	throws ServletException, IOException 
	{
		final Authentication existing = 
				SecurityContextHolder.getContext().getAuthentication();
		
		if (request.getHeader(this.headerName) != null)
		{
			if (this.headerValue.equals(request.getHeader(this.headerName))
				|| ! this.checkValue)
			{
				if (! (existing instanceof MicredaUserAuthentication) 
					|| this.overrideExistingAuthentication)
				{
					SecurityContextHolder.getContext()
								.setAuthentication(this.authSupplier.get());
				}
				
				filterChain.doFilter(request, response);
				
				SecurityContextHolder.getContext().setAuthentication(existing);

			}
			else
			{
				filterChain.doFilter(request, response);
			}
		}
		else
		{
			filterChain.doFilter(request, response);
		}
	}
	
	public static final Builder
	builder()
	{
		return new Builder();
	}
	
	public static final class Builder
	{
		private String							name;
		private String							value = null;
		private boolean							override = false;
		private Supplier<Authentication>		supplier;
		
		private Builder()
		{
			
		}
		
		public MagicHeaderFilter
		build()
		{
			Assert.hasText(this.name, "missing name");
			Assert.notNull(this.supplier, "missing supplier");
			
			return new MagicHeaderFilter(this.supplier, this.name, this.value, 
											this.override, this.value != null);
		}
		
		public final Builder
		withHeader(String header)
		{
			this.name = header;
			return this;
		}
		
		public final Builder
		withValue(String value)
		{
			this.value = value;
			return this;
		}
		
		public final Builder
		override()
		{
			this.override = true;
			return this;
		}
		
		public final Builder
		noOverride()
		{
			this.override = false;
			return this;
		}
		
		public final Builder
		supplier(Supplier<Authentication> supplier)
		{
			this.supplier = supplier;
			return this;
		}
	}
	
	
}
