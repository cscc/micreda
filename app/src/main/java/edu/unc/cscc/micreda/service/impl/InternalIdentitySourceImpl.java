/*-
 * ========================LICENSE_START=================================
 * micreda
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.micreda.service.impl;

import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.name;

import java.util.Collection;
import java.util.UUID;

import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.SelectConditionStep;
import org.jooq.Table;
import org.jooq.impl.DSL;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import edu.unc.cscc.micreda.model.MUser;
import edu.unc.cscc.micreda.model.UserIdentity;
import edu.unc.cscc.micreda.service.InternalIdentitySource;
import edu.unc.cscc.micreda.service.UserService;

final class InternalIdentitySourceImpl
implements InternalIdentitySource
{
	protected static final Table<?>			INTERNAL_ID_SOURCE_TABLE = 
			DSL.table(DSL.name("internal_identity_sources"));
	protected static final Table<?>			INTERNAL_USERS = 
			DSL.table(DSL.name("internal_user_identities"));
	
	private static final Field<UUID>		ID_SOURCE_FIELD = 
			field(name("id__internal_identity_sources"), UUID.class);
	
	private static final String				HASH_NAME = "bcrypt";

	private final UUID						id;
	private final DSLContext				ctx;
	private final UserService				userService;
	private final String					identifier;
	private final String					description;
	
	
	InternalIdentitySourceImpl(UUID id, DSLContext ctx, 
								UserService userService, String identifier,
								String description)
	{
		this.id = id;
		this.ctx = ctx;
		this.userService = userService;
		this.identifier = identifier;
		this.description = description;
	}
	
	protected DSLContext
	ctx()
	{
		return this.ctx;
	}
	
	protected UserService 
	userService()
	{
		return this.userService;
	}
	
	@Override
	public final UUID
	id()
	{
		return this.id;
	}
	
	@Override
	public String 
	identifier()
	{
		return this.identifier;
	}

	@Override
	public String 
	description()
	{
		return this.description;
	}
	
	protected final String
	hashName()
	{
		return HASH_NAME;
	}

	protected SelectConditionStep<? extends Record1<UUID>>
	sourceIDSubQuery()
	{
		return this.ctx()
					.select(field(name("id"), UUID.class))
					.from(INTERNAL_ID_SOURCE_TABLE)
					.where(field(name("identifier"), String.class)
							.eq(this.identifier()));
	}
	
	@Override
	public boolean
	exists(String username)
	{
		return this.ctx()
					.fetchExists(this.ctx()
										.selectOne()
										.from(INTERNAL_USERS.as("u")
												.join(INTERNAL_ID_SOURCE_TABLE.as("i"))
												.on(field(name("u", "id__internal_identity_sources"))
													.eq(field(name("i", "id")))))
										.where(field(name("i", "identifier")).eq(this.identifier()))
										.and(field(name("u", "username")).eq(username)));
	}

	@Override
	public UserIdentity 
	find(String username)
	{
		
		return this.ctx()
					.selectFrom(
							INTERNAL_USERS.as("u")
							.join(INTERNAL_ID_SOURCE_TABLE.as("i"))
							.on(field(name("u", "id__internal_identity_sources"))
								.eq(field(name("i", "id")))))
					.where(field(name("i", "identifier")).eq(this.identifier()))
					.and(field(name("u", "username")).eq(username))
					.fetchOne(r -> new UserIdentity(
							this.userService()
								.load(r.get(field(name(INTERNAL_USERS.getName(), "id"), UUID.class))), 
							r.get(field(name("u", "username"), String.class)),
							this.identifier()));
	}
	
	@Override
	public boolean 
	authenticate(String username, String credentials) 
	{
		/* Note the comparison to true here.  That's because since we're
		 * "fetching" a Boolean, we'd wind up with null value if there are 
		 * no results (and thus nothing going through the record mapper).
		 * 
		 * This is done in a single query for performance reasons since
		 * this gets called a *lot*...
		 */
		
		final Boolean authenticated =
				this.ctx()
				.select(field(name("password_hash")), field(name("hash_name")))
					.from(INTERNAL_USERS)
				.where(ID_SOURCE_FIELD.eq(this.sourceIDSubQuery()))
					.and(field(name("username"), String.class).eq(username))
				.fetchOne(r -> {
					return LocalCredentialsEncoder.check(
							credentials,
							r.get(field(name("password_hash"), String.class)),
							r.get(field(name("hash_name"), String.class)));
				});
		
		return Boolean.TRUE.equals(authenticated);
		
	}
	
	@Override
	@Transactional
	public UserIdentity 
	save(UserIdentity identity, String credentials)
	{
		Assert.notNull(credentials, "credentials must not be null");
		
		if (! this.userService().exists(identity.user().id()))
		{
			throw new IllegalArgumentException(
					"Cannot update credentials for non-existent user");
		}
		
		if (! this.identifier().equals(identity.identitySourceIdentifier()))
		{
			throw new IllegalArgumentException(
					"Cannot save identity managed by different identity source");
		}
		
		UserIdentity existing = this.find(identity.username());
		
		if (existing != null
			&& ! identity.user().id().equals(existing.user().id()))
		{
			throw new DataIntegrityViolationException(
					"username in use by another user");
		}
		
		Assert.isTrue(LocalCredentialsEncoder.supports(this.hashName()), 
						"encoder didn't support default hash");
		
		final String encoded = 
				LocalCredentialsEncoder.encode(credentials, this.hashName());
		
		Assert.notNull(encoded, "encoded credentials were null");
		
		/* PG supports ON DUPLICATE KEY UPDATE emulation for composite PKs, 
		 * but AFAIK HSQLDB doesn't
		 */
		this.ctx()
			.deleteFrom(INTERNAL_USERS)
			.where(field(name("id")).eq(identity.user().id()))
				.and(field(name("username")).eq(identity.username()))
				.and(ID_SOURCE_FIELD.eq(this.sourceIDSubQuery()))
			.execute();
		
		int affected = 
				this.ctx()
					.insertInto(INTERNAL_USERS)
					.set(field(name("id"), UUID.class), identity.user().id())
					.set(field(name("username"), String.class), identity.username())
					.set(ID_SOURCE_FIELD, this.sourceIDSubQuery())
					.set(field(name("password_hash"), String.class), encoded)
					.set(field(name("hash_name"), String.class), this.hashName())
					.execute();
		
		Assert.state(affected == 1, "affected <> 1 rows");
		
		return identity;
	}
	
	

	@Transactional
	@Override
	public void 
	delete(UserIdentity identity)
	{
		Assert.notNull(identity, "identity must not be null");
		
		this.ctx()
			.deleteFrom(INTERNAL_USERS)
			.where(field(name("id")).eq(identity.user().id()))
				.and(field(name("username")).eq(identity.username()))
				.and(ID_SOURCE_FIELD.eq(this.sourceIDSubQuery()))
			.execute();
	}

	@Override
	public Collection<UserIdentity> 
	find(MUser user)
	{
		return this.ctx()
				.selectFrom(
						INTERNAL_USERS.as("u")
						.join(INTERNAL_ID_SOURCE_TABLE.as("i"))
						.on(field(name("u", "id__internal_identity_sources"))
							.eq(field(name("i", "id")))))
				.where(field(name("i", "identifier")).eq(this.identifier()))
				.and(field(name("u", "id"), UUID.class).eq(user.id()))
				.fetch(r -> new UserIdentity(
						user, 
						r.get(field(name("u", "username"), String.class)),
						this.identifier()));
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((identifier == null) ? 0 : identifier.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InternalIdentitySourceImpl other = (InternalIdentitySourceImpl) obj;
		if (id == null)
		{
			if (other.id != null)
				return false;
		}
		else if (!id.equals(other.id))
			return false;
		if (identifier == null)
		{
			if (other.identifier != null)
				return false;
		}
		else if (!identifier.equals(other.identifier))
			return false;
		return true;
	}

	
}
