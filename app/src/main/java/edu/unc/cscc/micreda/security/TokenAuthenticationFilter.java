/*-
 * ========================LICENSE_START=================================
 * micreda
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.micreda.security;

import java.io.IOException;
import java.util.UUID;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import edu.unc.cscc.micreda.model.MUser;
import edu.unc.cscc.micreda.service.TokenService;
import edu.unc.cscc.micreda.service.UserService;

public final class TokenAuthenticationFilter
implements Filter
{
	
	private final TokenService		tokenService;
	private final UserService		userService;
	private final boolean			overrideExistingAuthentication;
	private final String			adminRoleName;
	
	/* FIXME - move admin role handling somewhere else.  Probably wherever
	 * we put MicredaAuthentication creation.
	 */
	public TokenAuthenticationFilter(TokenService tokenService,
										UserService userService,
										String adminRoleName,
										boolean overrideExistingAuthentication)
	{
		this.tokenService = tokenService;
		this.userService = userService;
		this.overrideExistingAuthentication = overrideExistingAuthentication;
		this.adminRoleName = adminRoleName;
	}
	
	
	
	@Override
	public void 
	doFilter(ServletRequest request, ServletResponse response, 
				FilterChain chain) 
	throws IOException, ServletException
	{
		final Authentication existing = 
				SecurityContextHolder.getContext().getAuthentication();
				
		if (this.overrideExistingAuthentication
			|| ! (existing instanceof MicredaUserAuthentication))
		{
			String token = extractToken(request);
			
			if (token != null)
			{
				try
				{
					this.tokenService.verifyToken(token);
					/* Hmmm... I suppose technically at this point we can 
					 * simply build an auth from the token.  We have roles, 
					 * ID, and state, so we could avoid the lookup...
					 */
					UUID id = this.tokenService.parseUserID(token);
					
					MUser user = this.userService.load(id);
					
					if (user != null && ! user.disabled())
					{
						Authentication auth = 
								new MicredaUserAuthentication(user, 
										user.roles().contains(this.adminRoleName));
						
						SecurityContextHolder.getContext().setAuthentication(auth);
					}
					
				}
				catch (AccessDeniedException e)
				{
					
				}
			}
		}
		
		
		
		chain.doFilter(request, response);
		
		SecurityContextHolder.getContext().setAuthentication(existing);
	}
	
	private static final String
	extractToken(ServletRequest request)
	{
		if (! (request instanceof HttpServletRequest))
		{
			return null;
		}
		
		
		String auth = ((HttpServletRequest) request).getHeader("Authorization");
		
		if (auth == null || ! auth.startsWith("Bearer "))
		{
			return null;
		}
		
		return auth.substring(7);
	}
	
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {}
	
	@Override
	public void destroy() {}
	

}
