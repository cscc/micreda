/*-
 * ========================LICENSE_START=================================
 * micreda
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.micreda.service;

import java.util.Collection;
import java.util.Map;
import java.util.UUID;

import edu.unc.cscc.micreda.model.IdentitySource;

/**
 * Service for creation and management of 
 * {@link InternalIdentitySource internal identity sources.}
 * 
 * @author Rob Tomsick (rtomsick@unc.edu)
 *
 */
public interface InternalIdentitySourceService
{
	
	/* TODO - allow delete with cascade to users */
	
	/**
	 * <p>
	 * Create an internal identity source with the given identifier and 
	 * description.  If the given identifier already 
	 * corresponds to an identity source, the description will be updated to
	 * reflect the one supplied.
	 * </p>
	 * 
	 * <p>
	 * Note that identifiers will be checked to ensure they are valid 
	 * (see {@link IdentitySource#identifier()}.)
	 * </p>
	 * 
	 * <p>
	 * If {@link #allowsCreation() source creation} is not allowed, this method
	 * will allow for update of identity sources, but not the creation of 
	 * new ones.
	 * </p>
	 * 
	 * @param identifier identifier of identity source
	 * @param description description of identity source
	 * @return identity source with given identifier, not <code>null</code>
	 * @throws IllegalArgumentException thrown if 
	 * {@link #allowsCreation() source creation is not allowed} or if 
	 * identifier is malformed
	 */
	public InternalIdentitySource save(String identifier, String description);
	
	/**
	 * Whether source creation is allowed.  If source creation is not allowed,
	 * calls to {@link #save(String)} will fail.
	 * 
	 * @return <code>true</code> if source creation is allowed, 
	 * <code>false</code> otherwise
	 */
	public boolean allowsCreation();

	/**
	 * Determine whether there exists an internal identity source with the given
	 * identifier.
	 * 
	 * @param identifier
	 * @return
	 */
	public boolean exists(String identifier);
	
	/**
	 * Find the identity source with the given identifier 
	 * (if one exists in underlying storage).
	 * 
	 * @param identifier identifier of identity source, not <code>null</code>,
	 * not empty
	 * @return identity source, or <code>null</code> if no such source exists
	 */
	public InternalIdentitySource find(String identifier);
	
	/**
	 * Find the identity source with the given ID 
	 * (if one exists in underlying storage).
	 * 
	 * @param id ID of identity source, not <code>null</code>
	 * @return identity source, or <code>null</code> if no such source exists
	 */
	public InternalIdentitySource load(UUID id);

	/**
	 * Search for an identity source with its identifier or description 
	 * containing the given query term.  If the term is <code>null</code>, 
	 * will operate in listing mode, enumerating all internal sources known
	 * to the instance.
	 * 
	 * @param query search query, may be <code>null</code>
	 * @param limit maximum number of results, must be &ge; 1
	 * @return sources matching the given query, not <code>null</code>
	 */
	public Collection<InternalIdentitySource> search(String query, int limit);
	
	/**
	 * Produce the identity sources known to this service.
	 * 
	 * @return identity sources, keyed by identifier, not <code>null</code>
	 */
	public Map<String, InternalIdentitySource> enumerateSources();

}
