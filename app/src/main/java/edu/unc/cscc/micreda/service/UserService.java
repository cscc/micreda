/*-
 * ========================LICENSE_START=================================
 * micreda
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.micreda.service;

import java.util.Collection;
import java.util.Map;
import java.util.UUID;

import edu.unc.cscc.micreda.model.MUser;

/**
 * The user service is responsible for all persistence of user entities.
 * 
 * @author Rob Tomsick (rtomsick@unc.edu)
 *
 */
public interface UserService
{
	

	public MUser save(MUser user);
	
	/**
	 * Determine whether a user (internal or external) exists with the given 
	 * ID.
	 * 
	 * @param id ID for which to check for user existence, not 
	 * <code>null</code>
	 * 
	 * @return <code>true</code> if a user with the given ID exists, 
	 * <code>false</code> otherwise
	 */
	public boolean exists(UUID id);
	
	/**
	 * Load the user with the given ID.
	 * 
	 * @param id ID for which to load user, not <code>null</code>
	 * @return user with the given ID, or <code>null</code> if no such user
	 * exists
	 */
	public MUser load(UUID id);
	
	/**
	 * <p>
	 * Attempt to resolve the "abbreviated ID" to a UUID identifying a user
	 * known to the instance.  An "abbreviated ID" is effectively simply the 
	 * first {@code n} characters of the string representation of 
	 * a {@link MUser#id() user ID}.
	 * </p>
	 * 
	 * <p>
	 * Note that this method will fail to resolve an ID if there is more than 
	 * one user with an ID beginning with the given abbreviated ID; in other 
	 * words resolution will succeed if and only if the given ID is sufficient
	 * to uniquely identify a user.
	 * </p>
	 * 
	 * @param id abbreviated ID to resolve, not <code>null</code>
	 * @return ID of which the given abbreviated ID is a starting subset, 
	 * <code>null</code> if no such ID could be found
	 */
	public UUID resolveAbbreviatedID(String id);
	
	/**
	 * Load the users with the given IDs.
	 * 
	 * @param ids IDs of users to load
	 * @return loaded users by ID, not <code>null</code>
	 */
	public Map<UUID, MUser> loadAll(Collection<UUID> ids);

	/**
	 * Produce a count of the number of users known to the service.
	 * 
	 * @return number of users known to the service
	 */
	public long count();
	
	/**
	 * Search for users with some/all of the given roles, up to a specified 
	 * maximum.  If no roles are specified, will simply enumerate users known
	 * to the instance.
	 * 
	 * @param roles roles for which to search for users for, may be empty
	 * @param anyRoleMatch <code>false</code> if resulting users must have
	 * all of the specified roles, <code>true</code> if users with at least one
	 * of the specified roles will be considered as matches
	 * @param limit maximum number of users to find, must be &ge; 1
	 * @return matching users, not <code>null</code>
	 */
	public Collection<MUser>
	search(Collection<String> roles, boolean anyRoleMatch, int limit);
	
}
