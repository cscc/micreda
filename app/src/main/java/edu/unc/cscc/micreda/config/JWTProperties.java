/*-
 * ========================LICENSE_START=================================
 * micreda
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.micreda.config;

import java.security.Key;

import edu.unc.cscc.micreda.model.configuration.JWTEndpointInfo;
import io.jsonwebtoken.SignatureAlgorithm;

public final class JWTProperties
{
	
	private final SignatureAlgorithm	signatureAlgorithm;
	private final Key					signingKey;
	private final int					lifespan;
	private final int					gracePeriodSeconds;
	private final String				claimPrefix;
	private final String				idClaimName;
	private final String				rolesClaimName;
	private final boolean				redactAdmin;
	private final String				adminRole;
	
	
	JWTProperties(Key signingKey, int lifespan, int gracePeriodSeconds,
				SignatureAlgorithm algorithm,
				String claimPrefix, String idClaimName,
				String rolesClaimName, boolean redactAdmin, String adminRole)
	{
		this.signingKey = signingKey;
		this.lifespan = lifespan;
		this.gracePeriodSeconds = gracePeriodSeconds;
		this.signatureAlgorithm = algorithm;
		this.claimPrefix = claimPrefix;
		this.idClaimName = idClaimName;
		this.rolesClaimName = rolesClaimName;
		this.redactAdmin = redactAdmin;
		this.adminRole = adminRole;
	}
	
	public final Key
	signingKey()
	{
		return this.signingKey;
	}
	
	public final int
	lifespan()
	{
		return this.lifespan;
	}
	
	public final int
	gracePeriodSeconds()
	{
		return this.gracePeriodSeconds;
	}
	
	public final SignatureAlgorithm
	signatureAlgorithm()
	{
		return this.signatureAlgorithm;
	}
	
	public final String
	claimPrefix()
	{
		return this.claimPrefix;
	}
	
	public final String
	idClaimName()
	{
		return this.idClaimName;
	}
	
	public final String
	rolesClaimName()
	{
		return this.rolesClaimName;
	}
	
	public final boolean
	redactAdminRole()
	{
		return this.redactAdmin;
	}

	public String 
	adminRole()
	{
		return this.adminRole;
	}
	
	/* this is a distinct method to reduce the risk of a bug exposing 
	 * JWT secrets.  Yes, we *could* make JWTProperties extend 
	 * JWTEndpointInfo, but then if you screw up serialization you 
	 * give everyone your key etc.
	 */
	public JWTEndpointInfo
	createEndpointInfo()
	{
		return new JWTEndpointInfo(this.gracePeriodSeconds(), this.claimPrefix(),
									this.idClaimName(), 
									this.rolesClaimName());
	}
	
}
