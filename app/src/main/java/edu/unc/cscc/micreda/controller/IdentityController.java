/*-
 * ========================LICENSE_START=================================
 * micreda
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.micreda.controller;

import java.net.URISyntaxException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonProperty;

import edu.unc.cscc.micreda.APIConstants;
import edu.unc.cscc.micreda.model.MUser;
import edu.unc.cscc.micreda.model.UserIdentity;
import edu.unc.cscc.micreda.service.ExternalIdentityService;
import edu.unc.cscc.micreda.service.IdentityResolver;
import edu.unc.cscc.micreda.service.InternalIdentitySource;
import edu.unc.cscc.micreda.service.InternalIdentitySourceService;
import edu.unc.cscc.micreda.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = "Identities")
@RestController
@Validated
public class IdentityController
{
	
	/* haha, Spring doesn't support capturing groups well either */
	private static final String				PLUS_DELIMITED_IDS = 
			"^(?:" + APIConstants.ID_REGEX + ")(?:\\+" 
			+ APIConstants.ID_REGEX + ")+$";

	@Autowired
	private ExternalIdentityService			externalIdentityService;
	@Autowired
	private InternalIdentitySourceService	internalSourceService;
	
	@Autowired
	private IdentityResolver				resolver;
	
	@Autowired
	private UserService						userService;

	
	@Transactional
	@RequestMapping(value = "/users/{id}/identities/internal/",
			method = RequestMethod.POST,
			consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Create an identity associated with an internal source, "
						+ "specifying the identity's credentials",
		notes = "Create/update an identity associated with an internal "
				+ "identity source, specifying the credentials that will be used "
				+ "for subsequent authentication attempts.  The specified "
				+ "source MUST exist within the current instance in order "
				+ "to be created.  If the given identity already exists "
				+ "for the specified user, the credentials will be updated "
				+ "to those supplied with the request.")
	@ApiResponses({
	@ApiResponse(code = 200, 
		message = "identity persisted successfully; "
				+ "response reflects current persisted state",
			response = UserIdentity.class),
	@ApiResponse(code = 404, message = "user not found"),
	@ApiResponse(code = 409, 
		message = "an identity with the same username/source is "
				+ "already associated with another user")	
	})
	public ResponseEntity<UserIdentity>
	saveInternalIdentity(@PathVariable("id")
						@NotNull
						@ApiParam(value = "ID of user for which to save identity",
									example = APIConstants.EXAMPLE_UUID,
									required = true)
							final UUID userID,
							
						@RequestBody
						@Valid @NotNull
						@ApiParam(value = "identity and credentials to create/update")
							final InternalIdentityDTO dto)
	{
		MUser user = this.userService.load(userID);
		
		if (user == null)
		{
			return ResponseEntity.notFound().build();
		}
		
		UserIdentity identity = dto.identity;
		
		final InternalIdentitySource source = 
				this.internalSourceService.find(identity.identitySourceIdentifier());
		
		if (source == null)
		{
			return ResponseEntity.notFound().build();
		}
		
		/* check for user conflict */
		final UserIdentity existing = source.find(dto.identity.username());
		
		if (existing != null
			&& ! existing.user().id().equals(userID))
		{
			return new ResponseEntity<>(HttpStatus.CONFLICT);
		}
		
		
		identity = source.save(identity, dto.credentials);
		
		return ResponseEntity.ok(identity);
	}

	@Transactional
	@RequestMapping(value = "/users/{id}/identities/external/",
			method = RequestMethod.POST,
			consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Create an identity associated with an external source",
					notes = "Create an identity associated with an external "
							+ "identity source.  The specified source does NOT "
							+ "need to exist within the current instance in order "
							+ "to be created, although the resulting identity "
							+ "will not be usable until such a source is "
							+ "available.  If the given identity already exists "
							+ "for the specified user, no action will be taken.")
	@ApiResponses({
		@ApiResponse(code = 200, 
					message = "identity persisted successfully; "
							+ "response reflects current persisted state",
						response = UserIdentity.class),
		@ApiResponse(code = 404, message = "user not found"),
		@ApiResponse(code = 409, 
					message = "an identity with the same username/source is "
							+ "already associated with another user")	
	})
	public ResponseEntity<UserIdentity>
	saveExternalIdentity(@PathVariable("id")
						@NotNull
						@ApiParam(value = "ID of user for which to create identity",
									example = APIConstants.EXAMPLE_UUID,
									required = true)
							final UUID userID, 
						@RequestBody 
						@Valid @NotNull
						@ApiParam(value = "user identity to create")
							final UserIdentity incoming)
	throws URISyntaxException
	{
		MUser user = this.userService.load(userID);

		if (user == null)
		{
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		if (! user.id().equals(userID))
		{
			return ResponseEntity.badRequest().build();
		}
		

		/* NOTE: we do *NOT* check whether the external ID source exists.
		 * Since ext. sources are pluggable, we need to support creation
		 * even if there's no source known to the currently running instance.
		 */

		/* check if already in use */

		final UserIdentity existing = 
				this.externalIdentityService.find(incoming.username(), 
													incoming.identitySourceIdentifier());

		UserIdentity identity = incoming;
		if (existing != null)
		{
			if (! existing.user().id().equals(user.id()))
			{
				return ResponseEntity.status(HttpStatus.CONFLICT).build();
			}
			
			identity = existing;
		}
		else
		{
			/* ignore user; caller might be dumb */
			identity = new UserIdentity(user, identity.username(), 
											identity.identitySourceIdentifier());

			identity = this.externalIdentityService.create(identity);
		}
		

		return ResponseEntity.ok(identity);
	}
	
	@Transactional
	@RequestMapping(value = "/users/{id:^" + APIConstants.ID_REGEX + "$}/identities/",
					method = RequestMethod.DELETE,
					params = {"!username"})
	@ApiOperation(value = "Delete all identities for a given user, optionally "
						+ "restricting by source",
					notes = "Delete the specified user's identities, optionally "
							+ "restricting deletion to those associated with "
							+ "the specified identity source.  If no source "
							+ "identifier is supplied, all identities for the "
							+ "specified user will be deleted. If no source "
							+ "is found with the given identifier, no action "
							+ "will be performed.",
					code = 204)
	@ApiResponses({
		@ApiResponse(code = 204, message = "deletion successful", 
						response = Void.class),
		@ApiResponse(code = 404, message = "user not found")
	})
	public ResponseEntity<?>
	deleteBySource(@PathVariable("id")
					@NotNull
					@ApiParam(value = "ID of the user for which to delete sources",
								required = true, 
								example = APIConstants.EXAMPLE_UUID)
						final UUID id,
					@RequestParam(value = "source", required = false)
						final String source)
	{
		final MUser user = this.userService.load(id);
	
		if (user == null)
		{
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		if (source == null)
		{
			/* nuke everything.  
			 * FIXME: This REALLY needs to be done at the service layer.
			 */
			
			
			/* first, nuke all the internals */
						
			this.internalSourceService
				.enumerateSources()
				.values()
				.stream()
				.forEach(s -> {
					s.find(user)
						.stream()
						.forEach(s :: delete);
				});
			
			/* now external */
			
			this.externalIdentityService
				.findByUser(user)
				.forEach(this.externalIdentityService :: delete);
			
		}
		else
		{
			/* FIXME - this can be implemented atomically and in one shot via 
			 * the service layer
			 */
			InternalIdentitySource is = this.internalSourceService.find(source);
			
			if (is != null)
			{
				is.find(user)
					.stream()
					.forEach(is :: delete);
			}
			else
			{
				this.externalIdentityService
							.findByUser(user)
							.stream()
							.filter(i -> i.identitySourceIdentifier().equals(source))
							.forEach(this.externalIdentityService :: delete);
			}
		}
		
		return ResponseEntity.ok().build();
	}
	
	@Transactional
	@RequestMapping(value = "/users/{id:^" + APIConstants.ID_REGEX + "$}/identities/",
					method = RequestMethod.DELETE,
					params = {"source", "username"})
	@ApiOperation(value = "delete a user's identity, specified by source and username",
					notes = "Delete the identity of the specified user with "
							+ "the given username, associated with the "
							+ "specified source.",
					code = 204)
	@ApiResponses({
		@ApiResponse(code = 204, message = "identity deleted sucessfully",
					response = Void.class),
		@ApiResponse(code = 404, message = "no such user or identity")
	})
	public ResponseEntity<?>
	deleteBySourceUsername(@PathVariable("id")
							@NotNull
							@ApiParam(value = "ID of the user for which to "
											+ "delete identity",
										example = APIConstants.EXAMPLE_UUID)
								final UUID id,
								
							@RequestParam(value = "source", required = true)
							@NotNull @Size(min = 1)
							@ApiParam(value = "identifier of the identity "
											+ "source for which to delete "
											+ "identity", 
										example = APIConstants.EXAMPLE_ID_SOURCE,
										required = true)
								final String source,
								
							@RequestParam(value = "username", required = true) 
							@NotNull @Size(min = 1) 
							@ApiParam(value = "username of identity to delete",
										example = APIConstants.EXAMPLE_USERNAME,
										required = true)
								final String username)
	{
		final MUser user = this.userService.load(id);

		if (user == null)
		{
			return ResponseEntity.notFound().build();
		}
		
		UserIdentity identity = this.resolver.resolve(username, source);
		
		if (identity == null)
		{
			return ResponseEntity.notFound().build();
		}
		
		final InternalIdentitySource s = this.internalSourceService.find(source);
		
		if (s != null)
		{
			s.delete(identity);
		}
		else
		{
			this.externalIdentityService.delete(identity);
		}
		
		return ResponseEntity.noContent().build();
	}
	
	@RequestMapping(value = "/users/{user-ids:" 
					+ IdentityController.PLUS_DELIMITED_IDS
					+ "}/identities/",
					method = RequestMethod.GET)
	@ApiOperation(value = "retrieve the identities of 1+ specified users",
					notes = "Retrieve the identities of one or more users.  "
							+ "Allows multiple user IDs to be specified "
							+ "(delimited by the '+' character).  The resulting "
							+ "response maps user IDs to the associated identities; "
							+ "any IDs which do not correspond to a known user "
							+ "will not be present in the resulting mapping.")
	@ApiResponses({
		@ApiResponse(code = 200, message = "identities successfully retrieved"),
		@ApiResponse(code = 204, 
						message = "no identities associated with the specified "
								+ "IDs were found")			
	})
	public ResponseEntity<Map<UUID, Collection<UserIdentity>>>
	getForUsers(@PathVariable("user-ids")
				@Pattern(regexp = IdentityController.PLUS_DELIMITED_IDS)
				@ApiParam(value = "a sequence of user IDs, delimited by a '+' "
								+ "character, for which to retrieve identities")
					final String idStr)
	{
		/* FIXME - batch in service layer */
		
		final Set<UUID> ids =
				Stream.of(idStr.split("\\Q+\\E"))
						.map(UUID :: fromString)
						.collect(Collectors.toSet());
		
		if (ids.isEmpty())
		{
			return ResponseEntity.noContent().build();
		}
				
		final Map<UUID, Collection<UserIdentity>> results = new HashMap<>();
		
		ids.stream()
			.map(this.userService :: load)
			.filter(u -> u != null)
			.forEach(user -> 
						results.put(user.id(), 
									this.resolver.enumerateIdentities(user)));
		
		return ResponseEntity.ok(results);
		
	}
	
	@ApiOperation(value = "Get the identities associated with a user, "
						+ "optionally restricted by source and/or username",
				notes = "Get the identities associated with a specified user, "
						+ "optionally restricting the results to those identities "
						+ "which specify a given username and/or are associated "
						+ "with a specified identity source.",
						response = UserIdentity.class, 
						responseContainer = "Set")
	@ApiResponses({
		@ApiResponse(code = 200, message = "identity retrieved successfully"),
		@ApiResponse(code = 404, message = "no such identity found")
	})
	@RequestMapping(value = "/users/{id:^" + APIConstants.ID_REGEX + "$}/identities/",
					method = RequestMethod.GET)
	public ResponseEntity<Collection<UserIdentity>>
	getBySourceUsername(@PathVariable("id") 
						@NotNull
						@ApiParam(value = "ID of the user for which to get the "
										+ "identities",
									example = APIConstants.EXAMPLE_UUID)
							final UUID id,
							
						@RequestParam(value = "source", required = false)
						@Size(min = 1)
						@ApiParam(value = "ID of identity source for which "
										+ "to retrieve user identities",
									example = APIConstants.EXAMPLE_ID_SOURCE)
							final String source,
							
						@RequestParam(value = "username", required = false) 
						@Size(min = 1) 
						@ApiParam(value = "username of identities to retrieve",
									example = APIConstants.EXAMPLE_USERNAME)
							final String username)
	{
		final MUser user = this.userService.load(id);

		if (user == null)
		{
			return ResponseEntity.notFound().build();
		}
		
		/* no source */
		if (StringUtils.isEmpty(source))
		{			
			final Collection<UserIdentity> identities = 
					this.resolver.enumerateIdentities(user);
			
			/* no source, no username */
			if (StringUtils.isEmpty(username))
			{
				return ResponseEntity.ok(identities);
			}
			
			/* no source, but username, so filter to that */
			return ResponseEntity.ok(identities.stream()
											.filter(i -> i.username().equals(username))
											.collect(Collectors.toList()));
		}
		else if (StringUtils.isEmpty(username))
		{
			/* source specified, but no username specified.  Enumerate all
			 * identities for specified source
			 */
			
			if (null == this.resolver.resolveSource(source))
			{
				return ResponseEntity.notFound().build();
			}
			
			/* TODO - make this more efficient than "load everything, and filter" */
			
			final Collection<UserIdentity> identities = 
					this.resolver.enumerateIdentities(user)
									.stream()
									.filter(i -> i.identitySourceIdentifier().equals(source))
									.collect(Collectors.toList());
			
			return ResponseEntity.ok(identities);
			
		}
		
		
		/* fall through to specifying both */
		
		UserIdentity identity = this.resolver.resolve(username, source);
		
		if (identity == null)
		{
			return ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.ok(Collections.singleton(identity));
	}
	
	
	@RequestMapping(value = {"/identities", "/identities/"},
					method = RequestMethod.GET,
					params = {"username", "source"})
	@ApiOperation(value = "Get the identity corresponding to a given "
						+ "username and identity source",
					notes = "Get the user identity identified by a given username "
							+ "and identity source identifier.")
	@ApiResponses({
		@ApiResponse(code = 200, message = "identity retrieved successfully",
						response = UserIdentity.class),
		@ApiResponse(code = 404, message = "no such identity found")
	})
	public ResponseEntity<UserIdentity>
	getIdentity(@RequestParam(value = "username", required = true) 
				@NotNull @Size(min = 1)
				@ApiParam(value = "username for which to find identity", 
							example = APIConstants.EXAMPLE_USERNAME, required = true)
					final String username, 
					
				@RequestParam(value = "source", required = true)
				@NotNull @Size(min = 1)
				@ApiParam(value = "identity source to search",
							example = APIConstants.EXAMPLE_ID_SOURCE, required = true)
					final String source)
	{
		final UserIdentity identity = this.resolver.resolve(username, source);
		
		if (identity == null)
		{
			return ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.ok(identity);
	}
	
	@RequestMapping(value = {"/identities", "/identities/"},
					method = RequestMethod.GET,
					params = {"query"},
					produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?>
	search(@RequestParam("query") @Valid @NotNull @Pattern(regexp = ".+") 
			final String query)
	{
		return null;
	}
	
	
	@ApiModel(value = "internal-identity-dto",
				description = "DTO for modification of identities from internal "
							+ "identity sources.")
	private final static class InternalIdentityDTO
	{
		@JsonProperty("identity")
		@NotNull
		@Valid
		@ApiModelProperty(value = "user identity", required = true)
		private UserIdentity		identity;
		
		@JsonProperty("credentials")
		@NotNull @Size(min = 1)
		@ApiModelProperty(value = "user credentials (i.e. plaintext password)",
							required = true, allowEmptyValue = false,
							example = "hunter2")
		private String				credentials;
	}

}
