/*-
 * ========================LICENSE_START=================================
 * micreda
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.micreda.service.impl;

import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.name;

import java.util.Collection;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.Table;
import org.jooq.impl.DSL;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.fasterxml.uuid.Generators;
import com.fasterxml.uuid.NoArgGenerator;

import edu.unc.cscc.micreda.model.IdentitySource;
import edu.unc.cscc.micreda.service.InternalIdentitySource;
import edu.unc.cscc.micreda.service.InternalIdentitySourceService;
import edu.unc.cscc.micreda.service.UserService;
import edu.unc.cscc.micreda.service.internal.ActiveExternalIdentitySourceHolder;

/* Note: this is a rather unfortunate mix between a model implementation
 * and a service.  I guess that's because IdentitySource is literally 
 * modeling a service. So don't expect these to be serializable or anything 
 * like that.
 */
public final class InternalIdentitySourceServiceImpl 
implements InternalIdentitySourceService
{
	private static final Table<?> 					ID_SOURCE_TABLE = 
			DSL.table(DSL.name("internal_identity_sources"));

	private static final NoArgGenerator				UUID_GENERATOR = 
			Generators.randomBasedGenerator();
	
	private final boolean							allowCreation;
	private final DSLContext						ctx;
	private final UserService						userService;
	private final ActiveExternalIdentitySourceHolder		externalSources;

	public InternalIdentitySourceServiceImpl(DSLContext ctx, 
											UserService userService,
											boolean allowCreation,
											ActiveExternalIdentitySourceHolder externalSourceLoader)
	{
		this.allowCreation = allowCreation;
		this.ctx = ctx;
		this.userService = userService;
		this.externalSources = externalSourceLoader;
	}
	
	@Override
	public final Map<String, InternalIdentitySource>
	enumerateSources()
	{		
		return this
				.ctx
				.selectFrom(ID_SOURCE_TABLE)
				.fetchMap(field(name("identifier"), String.class), 
							r -> new InternalIdentitySourceImpl(
									r.get(field(name("id"), UUID.class)),
									ctx, userService, 
									r.get(field(name("identifier"), String.class)),
									r.get(field(name("description"), String.class))));
	}
	
	@Override
	@Transactional
	public InternalIdentitySource
	save(String identifier, String description)
	{
		if (StringUtils.isEmpty(identifier))
		{
			throw new IllegalArgumentException("missing identifier");
		}
		
		if (description == null)
		{
			throw new IllegalArgumentException("missing description");
		}
		
		if (! this.allowsCreation())
		{
			throw new IllegalArgumentException("source creation not allowed");
		}
		
		final InternalIdentitySource existing = this.find(identifier);
		
		if (existing != null)
		{
			if (existing.description().equals(description))
			{
				return existing;
			}
			
			/* update description */
			
			int affected =
					this.ctx
						.update(ID_SOURCE_TABLE)
						.set(field(name("description"), String.class), description)
						.where(field(name("identifier"), String.class).eq(identifier))
						.execute();
			Assert.state(affected == 1, "Affected != 1 rows");

			return new InternalIdentitySourceImpl(existing.id(), this.ctx, 
													this.userService, identifier,
													description);
						
		}
		
		/* from here on out, we're assuming creation */
		
		if (! IdentitySource.validIdentifier(identifier))
		{
			throw new IllegalArgumentException("Invalid identity source identifier");
		}
		
		
		if (externalSources.externalIdentitySources().containsKey(identifier))
		{
			throw new IllegalArgumentException(
					"Cannot create internal identity source with same "
					+ "identifier as external source");
		}
		
		final UUID id = UUID_GENERATOR.generate();
		
		int affected = 
				this.ctx
					.insertInto(ID_SOURCE_TABLE)
					.set(field(name("identifier"), String.class), identifier)
					.set(field(name("description"), String.class), description)
					.set(field(name("id"), UUID.class), id)
					.execute();
		
		Assert.state(affected == 1, "Affected != 1 rows");
		
		return new InternalIdentitySourceImpl(id, this.ctx, this.userService, 
												identifier, description);
	}
	
	@Override
	public boolean
	exists(String identifier)
	{
		return this.ctx
					.fetchExists(this.ctx
									.selectOne()
									.from(ID_SOURCE_TABLE)
									.where(field(name("identifier"), String.class)
											.eq(identifier)));
	}

	@Override
	public boolean 
	allowsCreation()
	{
		return this.allowCreation;
	}

	@Override
	public InternalIdentitySource
	find(String identifier)
	{
		if (StringUtils.isEmpty(identifier))
		{
			throw new IllegalArgumentException("identifier must not be null or empty");
		}
		
		return this.ctx
					.selectFrom(ID_SOURCE_TABLE)
					.where(field(name("identifier"), String.class).eq(identifier))
					.fetchOne(r -> new InternalIdentitySourceImpl(
									r.get(field(name("id"), UUID.class)),
									ctx, userService, 
									r.get(DSL.field(DSL.name("identifier"), String.class)),
									r.get(field(name("description"), String.class))));
	}

	@Override
	public InternalIdentitySource
	load(UUID id)
	{
		if (id == null)
		{
			throw new IllegalArgumentException("id must not be null");
		}
		
		return this.ctx
				.selectFrom(ID_SOURCE_TABLE)
				.where(field(name("id"), UUID.class).eq(id))
				.fetchOne(r -> new InternalIdentitySourceImpl(
								r.get(field(name("id"), UUID.class)),
								ctx, userService, 
								r.get(DSL.field(DSL.name("identifier"), String.class)),
								r.get(field(name("description"), String.class))));
	}

	@Override
	public Collection<InternalIdentitySource>
	search(String query, int limit)
	{
		Assert.state(limit > 0, "limit must be > 0");
		
		final Condition qc = 
				StringUtils.isEmpty(query)
						? DSL.trueCondition()
						: field(name("identifier"), String.class).contains(query)
							.or(field(name("description"), String.class).contains(query));
		
		return this.ctx
				.selectFrom(ID_SOURCE_TABLE)
				.where(qc)
				.limit(limit)
				.fetch(r -> new InternalIdentitySourceImpl(
								r.get(field(name("id"), UUID.class)),
								ctx, userService, 
								r.get(DSL.field(DSL.name("identifier"), String.class)),
								r.get(field(name("description"), String.class))));
	}
	
	
	
}
