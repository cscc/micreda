/*-
 * ========================LICENSE_START=================================
 * micreda
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.micreda.service.impl;

import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.name;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Pattern;

import org.jooq.BatchBindStep;
import org.jooq.DSLContext;
import org.jooq.Table;
import org.jooq.impl.DSL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.fasterxml.uuid.Generators;
import com.fasterxml.uuid.NoArgGenerator;

import edu.unc.cscc.micreda.model.MUser;
import edu.unc.cscc.micreda.service.UserService;


@Service
public final class UserServiceImpl
implements UserService
{

	private static final Table<?>		TABLE = DSL.table(DSL.name("users"));
	private static final Table<?>		ROLES_TABLE = DSL.table(DSL.name("user_roles"));
	
	private static final NoArgGenerator	UUID_GENERATOR = 
			Generators.randomBasedGenerator();
	
	private static final Pattern		VALID_ID_CHARS = 
			Pattern.compile("^[a-fA-F0-9\\-]+$");
	
	
	private final DSLContext			ctx;	
	
	@Autowired
	public UserServiceImpl(DSLContext ctx)
	{
		this.ctx = ctx;
	}
	
	
	@Override
	public MUser 
	save(final MUser u)
	{
		MUser user = u;
		
		if (user.id() == null)
		{
			final UUID id = newID();
			final int affected =
					this.ctx
						.insertInto(TABLE)
						.set(field(name("id"),  UUID.class), id)
						.set(field(name("disabled"), Boolean.class), user.disabled())
						.execute();
			
			Assert.isTrue(affected == 1, "affected <> 1 rows");
			
			user = user.id(id);
		}
		else
		{
			final int affected =
					this.ctx
						.update(TABLE)
						.set(field(name("disabled"), Boolean.class), user.disabled())
						.where(field(name("id"), UUID.class).eq(user.id()))
						.execute();
			
			Assert.isTrue(affected == 1, "affected <> 1 rows");
		}
		
		this.saveRoles(user);
		
		return user;
	}



	@Transactional(propagation = Propagation.MANDATORY)
	private void
	saveRoles(MUser user)
	{
		Assert.notNull(user, "user must not be null");
		Assert.notNull(user.id(), "user id must not be null");
		
		this.ctx
			.deleteFrom(ROLES_TABLE)
			.where(DSL.field(name("id__users")).eq(user.id()))
			.execute();
	
		if (user.roles().isEmpty())
		{
			return;
		}
		
		if (user.roles().stream().anyMatch(s -> s == null || s.isEmpty()))
		{
			throw new IllegalArgumentException("Cannot save user with a null or empty role");
		}
		
		final BatchBindStep s = 
				this.ctx.batch(this.ctx
								.insertInto(ROLES_TABLE)
								.columns(DSL.field(name("id__users"), UUID.class), 
											DSL.field(name("role_name"), String.class))
								.values((UUID) null, (String) null));
		
		for (final String r : user.roles())
		{
			s.bind(user.id(), r);
		}
		
		s.execute();

	}

	@Override
	public boolean 
	exists(UUID id)
	{
		return this.ctx.fetchExists(
							this.ctx
								.selectOne()
								.from(TABLE)
								.where(DSL.field(name("id")).eq(id)));
	}
	
	@Override
	public long 
	count()
	{
		return this.ctx.fetchCount(this.ctx.selectOne().from(TABLE));
	}
	
	@Override
	public MUser 
	load(UUID id)
	{
		if (id == null)
		{
			return null;
		}
		
		return this.loadAll(Collections.singleton(id)).get(id);
	}
	
	
	
	
	@Override
	public UUID
	resolveAbbreviatedID(String id)
	{
		if (id == null)
		{
			throw new IllegalArgumentException("id must not be null");
		}
		
		if (id.isEmpty())
		{
			return null;
		}
		
		if (! VALID_ID_CHARS.matcher(id).matches())
		{
			return null;
		}
		
		
		List<UUID> ids = 
				this.ctx
					.select(field(name("id"), UUID.class))
					.from(TABLE)
					.where(DSL.lower(field(name("id"), UUID.class)
										.cast(String.class))
							.startsWith(DSL.lower(id)))
					.fetch(field(name("id"), UUID.class));
		
		if (ids.isEmpty() || ids.size() > 1)
		{
			return null;
		}
		
		return ids.iterator().next();
	}


	@Override
	public Map<UUID, MUser>
	loadAll(Collection<UUID> allIDs)
	{
		if (allIDs.isEmpty())
		{
			return Collections.emptyMap();
		}
		
		final Set<UUID> ids = new HashSet<>(allIDs);
				
		Map<UUID, List<String>> roleMap = 
				this.ctx.selectFrom(ROLES_TABLE)
						.where(field(name("id__users")).in(ids))
						.fetchGroups(field(name("id__users"), UUID.class), 
										field(name("role_name"), String.class));
		
		return this.ctx
				.selectFrom(TABLE)
				.where(field(name("id"), UUID.class).in(ids))
				.fetchMap(
					field(name("id"), UUID.class),
					r-> {
					
						final UUID id = r.get(field(name("id"), UUID.class));
						
						Set<String> roles;
						
						if (roleMap.containsKey(id))
						{
							roles = new HashSet<>(roleMap.get(id));
						}
						else
						{
							roles = Collections.emptySet();
						}
						
						return new MUser(id, 
									r.get(field(name("disabled"), Boolean.class)),
								roles);
				});
	}


	@Override
	public Collection<MUser> 
	search(Collection<String> r, boolean anyRoleMatch, int limit)
	{
		if (r == null || r.isEmpty())
		{
			return this.loadAll(
						this.ctx.select(field(name("id"), UUID.class))
								.from(TABLE)
								.limit(limit)
								.fetch(field(name("id"), UUID.class)))
						.values();
		}
		
		Set<String> roles = new HashSet<>(r);
		
		List<UUID> ids = new ArrayList<>();
		
		/* inefficient: fetchGroup -- would be faster to do this in SQL */
		
		this.ctx
			.selectFrom(ROLES_TABLE)
			.where(field(name("role_name"), String.class).in(roles))
			.fetchGroups(field(name("id__users"), UUID.class),
						field(name("role_name"), String.class))
			.entrySet()
			.stream()
			.filter(e ->
					anyRoleMatch
					|| e.getValue().size() == roles.size())
			.limit(limit)
			.forEach(e -> ids.add(e.getKey()));
		
		if (ids.isEmpty())
		{
			return Collections.emptySet();
		}
		
		return this.loadAll(ids).values();
	}
	

	private static final UUID
	newID()
	{
		return UUID_GENERATOR.generate();
	}


}
