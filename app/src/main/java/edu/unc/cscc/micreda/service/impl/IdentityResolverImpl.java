/*-
 * ========================LICENSE_START=================================
 * micreda
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.micreda.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.unc.cscc.micreda.model.ExternalIdentitySource;
import edu.unc.cscc.micreda.model.IdentitySource;
import edu.unc.cscc.micreda.model.MUser;
import edu.unc.cscc.micreda.model.UserIdentity;
import edu.unc.cscc.micreda.service.ExternalIdentityService;
import edu.unc.cscc.micreda.service.IdentityResolver;
import edu.unc.cscc.micreda.service.InternalIdentitySource;
import edu.unc.cscc.micreda.service.InternalIdentitySourceService;
import edu.unc.cscc.micreda.service.internal.ActiveExternalIdentitySourceHolder;

@Service
public final class IdentityResolverImpl 
implements IdentityResolver
{
	private final InternalIdentitySourceService			internalSourceService;
	private final ActiveExternalIdentitySourceHolder			externalLoader;
	private final ExternalIdentityService				externalService;
	
	@Autowired
	public IdentityResolverImpl(InternalIdentitySourceService internalSourceService,
								ActiveExternalIdentitySourceHolder externalLoader,
								ExternalIdentityService externalService)
	{
		this.internalSourceService = internalSourceService;
		this.externalLoader = externalLoader;
		this.externalService = externalService;
	}



	@Override
	public UserIdentity 
	resolve(String username, String identitySourceID)
	{
		
		UserIdentity ei = this.externalService.find(username, identitySourceID);
		
		if (ei != null)
		{
			return ei;
		}
		
		InternalIdentitySource is = 
				this.internalSourceService.find(identitySourceID);
		
		if (is != null)
		{
			return is.find(username);
		}
		
		return null;
	}

	@Override
	public IdentitySource
	resolveSource(String identifier)
	{
		Map<String, ExternalIdentitySource> externalSources = 
				this.externalLoader.externalIdentitySources();
		
		if (externalSources.containsKey(identifier))
		{
			return externalSources.get(identifier);
		}
		
		return this.internalSourceService.find(identifier);
	}

	


	@Override
	public Collection<UserIdentity> 
	enumerateIdentities(MUser user)
	{
		Collection<UserIdentity> external = 
				this.externalService.findByUser(user);
		
		Collection<UserIdentity> internal =
				this.internalSourceService
					.enumerateSources()
					.values()
					.parallelStream()
					.flatMap(s -> s.find(user).stream())
					.collect(Collectors.toSet());
		
		Collection<UserIdentity> identities = new ArrayList<>();
		
		identities.addAll(external);
		identities.addAll(internal);
		
		return identities;
	}

}
