/*-
 * ========================LICENSE_START=================================
 * micreda
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.micreda.service;

import java.util.Collection;

import org.jooq.exception.DataAccessException;

import edu.unc.cscc.micreda.model.UserIdentity;
import edu.unc.cscc.micreda.model.IdentitySource;
import edu.unc.cscc.micreda.model.MUser;

public interface ExternalIdentityService
{
	/**
	 * <p>
	 * Create the given external identity.
	 * </p>
	 * 
	 * <p>
	 * Note that identity source identifiers will be checked to ensure they 
	 * are valid (see {@link IdentitySource#identifier()}.)
	 * </p>
	 * 
	 * <p>
	 * Will fail if the identity (as defined by username/identity source) 
	 * already exists and is associated with another user, or if the identity
	 * source specified is empty.
	 * </p>
	 * 
	 * @param identity identity to create
	 * @return copy of the identity reflecting underlying storage
	 * @throws IllegalArgumentException thrown if the given identity is malformed
	 * @throws DataAccessException thrown if the identity information 
	 * specified by the given identity is already associated with another
	 * user
	 */
	public UserIdentity create(UserIdentity identity);
	
	public UserIdentity find(String username, String identitySourceID);
	
	public Collection<UserIdentity> search(String query);
	
	public Collection<UserIdentity> search(String query, String identitySourceID);
	
	public Collection<UserIdentity> findByUser(MUser user);
	
	public void delete(UserIdentity identity);
	
	/**
	 * Search for external identity source identifiers known to the service.
	 * The results will contain all matching external identity source 
	 * identifiers associated with at least one external identity regardless
	 * of whether a source with the identifier is currently loaded into the
	 * active instance.  If no query is provided (i.e. the query term is 
	 * <code>null</code>) then all identifiers up to the given limit will be
	 * found
	 * 
	 * @param query search query, may be <code>null</code>
	 * @param limit maximum number of results, &ge; 1
	 * @return external identity source identifiers matching the given query
	 */
	public Collection<String> searchIdentifiers(String query, int limit);
}
