/*-
 * ========================LICENSE_START=================================
 * micreda
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.micreda.security;

import java.security.Principal;
import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import edu.unc.cscc.micreda.model.MUser;

/**
 * <p>
 * Represents an authentication for a {@link MUser micreda user}.
 * </p>
 * 
 * <p>
 * If the 
 * associated user has no ID it is considered "transient" and will have a 
 * {@link #getName() name} representing that fact along with any roles it holds.
 * If the associated user does have an ID, however, the name of the 
 * authentication will be a String representation of the same.  This allows
 * for logically-equivalent users to have identical names while still adhering
 * to the spirit of {@link Principal} (although {@link Principal#getName()}
 * does not appear to require uniqueness.
 * </p>
 * 
 * <p>
 * <b>A word about roles</b><br><br>
 * 
 * As this class is a bridge between micreda's notion of users and Spring
 * Security's user/security model, there is some terminology overloading.  
 * The "roles" that micreda defines (i.e. those contained in 
 * {@link MUser#roles()}) are not exposed to Spring.  If and only if 
 * the authentication is specified as belonging to an admin at the time of
 * construction will it contain any {@link GrantedAuthority authorities} 
 * (specifically, it will contain the {@link #ADMIN_AUTHORITY admin authority}).
 * The practical upshot of all of this is that one cannot write security
 * annotations (e.g. {@code hasAuthority("foo")}) which reference 
 * {@link MUser#roles() user roles}.
 * </p>
 * 
 * @author Rob Tomsick (rtomsick@unc.edu)
 *
 */
public final class MicredaUserAuthentication
implements Authentication
{
	public static final GrantedAuthority	ADMIN_AUTHORITY = 
			new SimpleGrantedAuthority("micreda-admin-authority");
	
	private static final long serialVersionUID = 1L;
	
	private final boolean		isAdmin;
	private final MUser			user;
	
	private boolean				authenticated = true;
	
	public MicredaUserAuthentication(MUser user, final boolean isAdmin)
	{
		this.user = user;
		this.isAdmin = isAdmin;
	}

	@Override
	public String
	getName()
	{
		if (user.id() == null)
		{
			return String.format("transient,roles={%s},disabed=%s",
								this.user.roles()
										.stream()
										.map(r -> "'" + r + "'")
										.collect(Collectors.joining(",")),
								Boolean.valueOf(user.disabled()));
		}
		
		return user.id().toString();
	}

	@Override
	public Collection<? extends GrantedAuthority>
	getAuthorities()
	{
		if (this.isAdmin)
		{
			return Collections.singleton(ADMIN_AUTHORITY);
		}
		
		return Collections.emptySet();
	}

	@Override
	public Object 
	getCredentials()
	{
		/* nothing, since this corresponds to an authenticated user and 
		 * authentications are (from Spring's perspective) per-request we have
		 * no reason to retain this
		 */
		return null;
	}

	@Override
	public Object
	getDetails()
	{
		return this.user;
	}

	@Override
	public Object
	getPrincipal()
	{
		return this.user;
	}

	@Override
	public boolean 
	isAuthenticated()
	{
		return this.authenticated;
	}

	@Override
	public void
	setAuthenticated(boolean isAuthenticated)
	throws IllegalArgumentException
	{
		this.authenticated = isAuthenticated;
	}

	
}
