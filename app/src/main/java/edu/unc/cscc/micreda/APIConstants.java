/*-
 * ========================LICENSE_START=================================
 * micreda
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
/**
 * Contains constants used within the micreda API.
 */
package edu.unc.cscc.micreda;

/**
 * Contains constants used in API implementation and documentation.
 * 
 * @author Rob Tomsick (rtomsick@unc.edu)
 *
 */
public abstract class APIConstants
{
	/**
	 * The implemented version of the API.
	 */
	public static final String		VERSION = "0.1";
	
	/* constants for examples in swagger docs */
	/**
	 * Example username, used for documentation.
	 */
	public static final String		EXAMPLE_USERNAME = "j_doe";
	/**
	 * Example identity source identifier, used for documentation.
	 */
	public static final String		EXAMPLE_ID_SOURCE = "example-ldap-directory";
	/**
	 * Example UUID, used for documentation.
	 */
	public static final String		EXAMPLE_UUID = "3dd6f3d6-998a-11e7-9fd6-483a7beef7fc";

	/**
	 * A regular expression that may be used to validate the UUIDs used for 
	 * entity IDs.
	 */
	public static final String		ID_REGEX = 
	/* This is what we want:
	 * 
	 * "[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}"
	 * 
	 * But we can't do it since Spring chokes on quantifiers.
	 */

	/* so instead, we have this insanity */
		"[0-9a-f][0-9a-f][0-9a-f][0-9a-f][0-9a-f][0-9a-f][0-9a-f][0-9a-f]"
		+ "-"
		+ "[0-9a-f][0-9a-f][0-9a-f][0-9a-f]"
		+ "-"
		+ "[1-5][0-9a-f][0-9a-f][0-9a-f]"
		+ "-"
		+ "[89ab][0-9a-f][0-9a-f][0-9a-f]"
		+ "-"
		+ "[0-9a-f][0-9a-f][0-9a-f][0-9a-f][0-9a-f][0-9a-f]"
			+ "[0-9a-f][0-9a-f][0-9a-f][0-9a-f][0-9a-f][0-9a-f]";

	
}
