/*-
 * ========================LICENSE_START=================================
 * micreda
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.micreda.service.impl;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.fasterxml.uuid.Generators;
import com.fasterxml.uuid.NoArgGenerator;

import edu.unc.cscc.micreda.config.JWTProperties;
import edu.unc.cscc.micreda.model.MUser;
import edu.unc.cscc.micreda.service.TokenService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;

@Service
public final class TokenServiceImpl 
implements TokenService
{

	private static final NoArgGenerator	UUID_GENERATOR = 
			Generators.randomBasedGenerator();
	private final JWTProperties			config;
	
	@Autowired
	public TokenServiceImpl(JWTProperties config)
	{
		this.config = config;
	}
	
	
	@Override
	public String
    createToken(final MUser user, final boolean includeJTI)
    {
    	 final Date now = new Date();
         final Date expiration = 
        		 new Date(now.getTime() + this.config.lifespan());
         
         if (user.id() == null)
         {
        	 throw new IllegalArgumentException("Cannot create token for user without ID");
         }
         
         Set<String> roles = new HashSet<>(user.roles());
         
         if (this.config.redactAdminRole())
         {
        	 roles.remove(this.config.adminRole());
         }
         
         
         return (includeJTI 
        		 	? Jwts.builder() 
        		 	: Jwts.builder().setId(generateJTI()))
        		 .setSubject(user.id().toString())
                 .claim(this.config.claimPrefix()
                		 + this.config.rolesClaimName(), roles)
                 .claim(this.config.claimPrefix()
                		 + this.config.idClaimName(), user.id())
                 .setIssuedAt(now)
                 .setExpiration(expiration)
                 .signWith(this.config.signatureAlgorithm(), 
                		 	this.config.signingKey())
                 .compact();
    }
	
	
	
	@Override
	public void 
	verifyToken(String token) 
	throws AccessDeniedException
	{
		this.verifyToken(token, this.config.gracePeriodSeconds());
	}


	@Override
	public void 
	verifyToken(String token, int gracePeriodSeconds) 
	throws AccessDeniedException
	{
		Assert.state(gracePeriodSeconds > -1, "grace period seconds must be >= 0");
		try
		{
			JwtParser parser = 
					Jwts.parser()
						.setSigningKey(config.signingKey());
			
			if (gracePeriodSeconds > 0)
			{
				parser = parser.setAllowedClockSkewSeconds(gracePeriodSeconds);
			}
			
			Claims claims = parser.parseClaimsJws(token)
									.getBody();
			
			if (claims.getExpiration() == null)
			{
				throw new AccessDeniedException("Token is missing expiration date");
			}
			
			if (claims.getSubject() == null)
			{
				throw new AccessDeniedException("Missing subject (aka user ID)");
			}
			
			if (! claims.containsKey(this.config.claimPrefix()
										+ this.config.idClaimName()))
			{
				throw new AccessDeniedException(
							String.format("Token is missing user ID claim [%s]",
											this.config.claimPrefix()
											+ this.config.idClaimName()));
			}
			
			if (! claims.containsKey(this.config.claimPrefix() + this.config.rolesClaimName()))
			{
				throw new AccessDeniedException(
						String.format("Token is missing roles claim [%s]",
										this.config.claimPrefix() 
										+ this.config.rolesClaimName()));
			}
			
			/* validate ID format */
			try
			{
				UUID.fromString(claims.get(this.config.claimPrefix() 
											+ this.config.idClaimName(),
											String.class));
			}
			catch (Exception e)
			{
				throw new AccessDeniedException("Malformed user ID", e);
			}
			
		} 
		catch (ExpiredJwtException e)
		{
			throw new AccessDeniedException("JWT token expired", e);
		} 
		catch (SignatureException e)
		{
			throw new AccessDeniedException("JWT token signature verification failed", e);
		}
		catch (MalformedJwtException e)
		{
			throw new AccessDeniedException("JWT token was malformed", e);
		}
		catch (RuntimeException e)
		{
			throw new AccessDeniedException("Unspecified jjwt error", e);
		}
		
	}

	@Override
	public UUID
	parseUserID(String token)
	{
		try
		{
			this.verifyToken(token);
		}
		catch (AccessDeniedException e)
		{
			return null;
		}
		
		final String idStr =
				Jwts.parser()
					.setSigningKey(config.signingKey())
					.parseClaimsJws(token)
					.getBody()
					.get(this.config.claimPrefix() + this.config.idClaimName(), 
							String.class);
		
		return UUID.fromString(idStr);
		
	}

	@Override
	public Set<String> 
	parseRoles(String token)
	{
		try
		{
			this.verifyToken(token);
		}
		catch (AccessDeniedException e)
		{
			return null;
		}
		
	
		Collection<?> col = 
				Jwts.parser()
					.setSigningKey(config.signingKey())
					.parseClaimsJws(token)
					.getBody()
					.get(this.config.claimPrefix() + this.config.rolesClaimName(), 
							Collection.class);
		
		Set<String> roles = 
				col.stream()
					.filter(r -> r != null)
					.map(String :: valueOf)
					.collect(Collectors.toCollection(HashSet :: new));
		
		return roles;
		
	}

	private final static String
	generateJTI()
	{
		return UUID_GENERATOR.generate().toString();
	}
}
