/*-
 * ========================LICENSE_START=================================
 * micreda
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.micreda.config;

import java.io.IOException;
import java.util.Collections;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.AdviceMode;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.access.AccessDeniedHandlerImpl;
import org.springframework.security.web.authentication.Http403ForbiddenEntryPoint;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;
import org.springframework.util.Assert;

import edu.unc.cscc.micreda.model.MUser;
import edu.unc.cscc.micreda.security.MagicHeaderFilter;
import edu.unc.cscc.micreda.security.MicredaUserAuthentication;
import edu.unc.cscc.micreda.security.TokenAuthenticationFilter;
import edu.unc.cscc.micreda.service.TokenService;
import edu.unc.cscc.micreda.service.UserService;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, 
							proxyTargetClass = false,
							mode = AdviceMode.ASPECTJ)
public class WebSecurityConfig
extends WebSecurityConfigurerAdapter
{

	private final String		apiKeyHeaderName;
	private final String		apiKeyHeaderValue;
	private final String		adminRole;
	private final Logger		logger = 
			LoggerFactory.getLogger("micreda-http-security");
	
	
	@Autowired
	private TokenService		tokenService;
	@Autowired
	private UserService			userService;
	
    public WebSecurityConfig(@Value("${micreda.api.key.header.name}") 
    								String apiKeyHeaderName,
								@Value("${micreda.api.key}") 
    								String apiKeyHeaderValue,
    							@Value("${micreda.roles.administrator}")
    							String adminRole)
	{
    	Assert.hasText(apiKeyHeaderName, "missing API key header name");
		this.apiKeyHeaderName = apiKeyHeaderName;
		
		if (StringUtils.isEmpty(apiKeyHeaderValue))
		{
			final String generatedKey = 
					"mak-" + UUID.randomUUID().toString().substring(0, 13);
			this.logger.warn("Generated API key: '" + generatedKey + "'");
			this.apiKeyHeaderValue = generatedKey;
		}
		else
		{
			this.apiKeyHeaderValue = apiKeyHeaderValue;
		}
		Assert.hasText(adminRole, "missing admin role");
		
		this.adminRole = adminRole;
	}
    
    @Bean
    public AdminRoleHolder
    adminRole()
    {
    	return new AdminRoleHolder(this.adminRole);
    }

	protected void 
    configure(HttpSecurity http) 
    throws Exception 
    {
        http.csrf().disable()
    		.authorizeRequests()
    		/* anything under /tokens/ is open to all, since we need that for
    		 * auth.  Everything else requires admin user.
    		 */
    		.antMatchers("/info").permitAll()
    		.antMatchers("/tokens/*").permitAll()
    		.antMatchers("/**")
				.hasAuthority(MicredaUserAuthentication.ADMIN_AUTHORITY
														.getAuthority())
            .antMatchers(HttpMethod.OPTIONS).permitAll() /* allowed for CORS preflight */
            .and()
            .anonymous().and()
            /* Spring doesn't provide any login support */
            .httpBasic().disable()
            .formLogin().disable()
            .addFilterBefore(MagicHeaderFilter
            					.builder()
            					.withHeader(this.apiKeyHeaderName)
            					.withValue(this.apiKeyHeaderValue)
            					.supplier(this :: createTransientAdminAuthentication)
            					.build(), 
        					UsernamePasswordAuthenticationFilter.class)
            .addFilterAfter(new TokenAuthenticationFilter(this.tokenService, 
            												this.userService, 
            												this.adminRole, 
            												true),
            				MagicHeaderFilter.class)
            .servletApi()
            .and()
            /* This is fun to debug if omitted.  If you leave sessions enabled,
             * your Filter will see the correct header, but AuthenticationEntryPoints
             * may see stale headers.  This is documented absolutely nowhere.
             */
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
            .and()
            .exceptionHandling()
            	.authenticationEntryPoint(new BasicAuthenticationEntryPoint(){

            		private Http403ForbiddenEntryPoint pt = new Http403ForbiddenEntryPoint();
            		
					@Override
					public void commence(HttpServletRequest request,
							HttpServletResponse response,
							AuthenticationException authException)
							throws IOException, ServletException
					{
						/* only add www-auth if there's no authentication attempt
						 * in the response (i.e. no Authorization header, and
						 * no API key header if configured)
						 */
						if (request.getHeader("Authorization") == null
							&& (StringUtils.isEmpty(WebSecurityConfig.this.apiKeyHeaderName)
								|| request.getHeader(WebSecurityConfig.this.apiKeyHeaderName) == null))
						{
							
							/* add two www-authorize headers.  Bearer first
							 * (which actually means we add it last...)
							 */
							
							response.addHeader("WWW-Authenticate", "X-Header-Based");
							response.addHeader("WWW-Authenticate", "Bearer");
							response.sendError(HttpServletResponse.SC_UNAUTHORIZED,
									authException.getMessage());
							
						}
						else
						{
							pt.commence(request, response, authException);
						}
					}
            		
            	})
            	.accessDeniedHandler(new AccessDeniedHandlerImpl())
            .and()
            .headers().cacheControl();
    }
	
	/* do an end run around spring managing our authentication.  Yes, we could
	 * venture into the spooky untyped world inhabited by the ghosts of Acegi
	 * ...
	 * Or we could implement a couple trivial filters, don't use 
	 * Authentication as anything other than a holder for an authenticated 
	 * user, and go off and have a beer.
	 * ...
	 * We're picking that option.
	 */
	@Bean
	public AuthenticationManager
	authManager()
	{
		return new AuthenticationManager()
		{
			@Override
			public Authentication 
			authenticate(Authentication authentication)
					throws AuthenticationException
			{
				return null;
			}
		};
	}
	
    private final Authentication
    createTransientAdminAuthentication()
    {
    	return new MicredaUserAuthentication(
    			new MUser(null, false, Collections.singleton(this.adminRole)), 
    			true);
    }
    
    
    public static final class AdminRoleHolder
    {
    	private final String		role;
    	
    	public AdminRoleHolder(String role)
    	{
    		this.role = role;
    	}
    	
    	public String
    	role()
    	{
    		return this.role;
    	}
    }
	
}
