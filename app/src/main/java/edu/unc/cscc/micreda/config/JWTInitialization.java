/*-
 * ========================LICENSE_START=================================
 * micreda
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.micreda.config;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.Key;
import java.util.Arrays;

import javax.crypto.spec.SecretKeySpec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.Assert;

import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.crypto.MacProvider;

@Configuration
class JWTInitialization
{

	/* TODO - make this configurable */
	public static final SignatureAlgorithm SIGNATURE_ALGORITHM = 
			SignatureAlgorithm.HS512;
	
	private final String	secretPath;
	private final int		tokenLifespan;
	
	private final Logger	logger;
	
	public JWTInitialization(@Value("${micreda.jwt.key}") final String secretPath,
							@Value("${micreda.jwt.lifespan}") final int tokenLifespan)
	{
		this.secretPath = secretPath;
		
		Assert.isTrue(tokenLifespan > 0, "token lifespan must be positive integer");
		
		this.tokenLifespan = tokenLifespan;
		
		this.logger = LoggerFactory.getLogger(this.getClass());
		

	}
	
	
	@Bean
	public JWTProperties
	config(@Value("${micreda.jwt.grace-period}") int gracePeriodSecs,
			@Value("${micreda.jwt.claim-prefix}") String claimPrefix,
			@Value("${micreda.jwt.claims.id.name}") String idClaimName,
			@Value("${micreda.jwt.claims.roles.name}") String rolesClaimName,
			@Value("${micreda.jwt.claims.roles.redact-administrator}") 
				boolean redactAdmin,
			@Value("${micreda.roles.administrator}") String adminRole)
	throws IOException
	{
		
		if (claimPrefix == null)
		{
			claimPrefix = "";
		}
		
		Assert.hasText(idClaimName, "missing value for id claim name");
		Assert.hasText(rolesClaimName, "missing value for role claim name");
		
		
		return new JWTProperties(this.jwtSigningKey(), this.tokenLifespan, 
								gracePeriodSecs,
								SIGNATURE_ALGORITHM, claimPrefix,
								idClaimName, rolesClaimName,
								redactAdmin, adminRole);
	}
	
	private Key
	jwtSigningKey()
	throws IOException
	{
		
		if (this.secretPath == null || this.secretPath.trim().isEmpty())
		{
			logger.info("No secret key specified.  Generating per-instance key");
			return MacProvider.generateKey();
		}
		
		File keyFile = new File(this.secretPath);
		
		if (keyFile.canRead())
		{
			return this.readKey(keyFile);
		}
		else
		{
			throw new FileNotFoundException(
					String.format("Unable to read specified secret key "
									+ "from path '%s', halting.", 
									this.secretPath));
		}
		
	}
	
	
	private final Key
	readKey(File keyFile)
	throws IOException
	{		
		try (final BufferedInputStream bis = 
				new BufferedInputStream(new FileInputStream(keyFile)))
		{
			byte[] b = new byte[1024];
			
			final int read = bis.read(b);
			
			if (read < 64)
			{
				throw new IOException("Read < 64 bytes from key file.");
			}
			
			this.logger.debug(String.format("Read %d key bytes", read));
			
			b = Arrays.copyOf(b, read);
			
			Key key = new SecretKeySpec(b, SIGNATURE_ALGORITHM.getJcaName());
			
			return key;
		}
		
	}
	
	
}
