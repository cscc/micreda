/*-
 * ========================LICENSE_START=================================
 * micreda
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.micreda.config;

import java.lang.reflect.Method;

import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.conf.RenderNameStyle;
import org.jooq.conf.Settings;
import org.jooq.impl.DataSourceConnectionProvider;
import org.jooq.impl.DefaultDSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.AdviceMode;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.TransactionAwareDataSourceProxy;
import org.springframework.jdbc.datasource.init.DatabasePopulatorUtils;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.AnnotationTransactionAttributeSource;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.interceptor.DelegatingTransactionAttribute;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.transaction.interceptor.TransactionAttribute;
import org.springframework.util.StringUtils;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import edu.unc.cscc.micreda.model.configuration.DatabaseInfo;
import edu.unc.cscc.micreda.model.configuration.DatabaseInfo.DatabaseType;

@Configuration
@EnableTransactionManagement(mode = AdviceMode.ASPECTJ)
public class DBConfig
{

	private static final String	SCHEMA_HSQLDB_PATH = "schema.hsqldb.sql";
	private static final String SCHEMA_POSTGRES_PATH = "schema.postgres.sql";
	
	private static final int	DEFAULT_POOL_SIZE = 8;

	private final String		url;
	private final String		username;
	private final String		password;


	public DBConfig(@Value("${micreda.db.url}") final String jdbcURL,
			@Value("${micreda.db.username}") final String username,
			@Value("${micreda.db.password}") final String password)
	{
		this.url = jdbcURL;
		
		if (this.url == null || ! this.url.startsWith("jdbc:"))
		{
			throw new IllegalArgumentException("Malformed JDBC URL");
		}
		
		this.username = username;
		this.password = password;
	}

	@Bean
	public HikariDataSource
	datasource(@Value("${micreda.db.pool-size}") Integer poolSize,
				@Value("${micreda.db.initialize}") boolean initializeDB)
	{
		if (poolSize == null || poolSize < 1)
		{
			poolSize = DEFAULT_POOL_SIZE;
		}

		final HikariConfig config = new HikariConfig();
		
		config.setMaximumPoolSize(poolSize);

		if (StringUtils.hasText(this.username))
		{
			config.setUsername(this.username);
		}
		if (StringUtils.hasText(this.password))
		{
			config.setPassword(this.password);
		}
		
		config.setJdbcUrl(this.url);
				
		HikariDataSource ds = new HikariDataSource(config);
		
		
		if (initializeDB)
		{
			ResourceDatabasePopulator populator = new ResourceDatabasePopulator();
			
			ClassPathResource script;
			
			if (ds.getJdbcUrl().contains("postgresql"))
			{
				script = new ClassPathResource(SCHEMA_POSTGRES_PATH);
			}
			else if (ds.getJdbcUrl().contains(":hsqldb:"))
			{
				script = new ClassPathResource(SCHEMA_HSQLDB_PATH);
			}
			else
			{
				throw new RuntimeException(
						"Unsupported JDBC connection type.  Expected postgres or"
						+ " hsqldb");
			}
			
			populator.setScripts(script);
			
			DatabasePopulatorUtils.execute(populator, ds);
			
		}
		
		
		return ds;
	}

	@Bean
	@Autowired
	public DSLContext
	ctx(DataSourceConnectionProvider provider)
	{
		SQLDialect dialect;
		
		Settings settings = new Settings();
		
		if (this.url.contains("postgresql"))
		{
			dialect = SQLDialect.POSTGRES;
		}
		else if (this.url.contains(":hsqldb:"))
		{
			dialect = SQLDialect.HSQLDB;
			settings.withRenderNameStyle(RenderNameStyle.QUOTED);
		}
		else
		{
			throw new RuntimeException(
					"Unsupported JDBC connection type.  Expected postgres or"
					+ " hsqldb");
		}
		
		
		return new DefaultDSLContext(provider, dialect, settings);
	}
	
	@Bean
	@Autowired
	public PlatformTransactionManager
	transactionManager(TransactionAwareDataSourceProxy ctxProxy)
	{
		return new DataSourceTransactionManager(ctxProxy);
	}

	@Bean
	@Autowired
	public TransactionAwareDataSourceProxy
	ctxProxy(HikariDataSource datasource)
	{		
		return new TransactionAwareDataSourceProxy(datasource);
	}

	@Bean
	@Autowired
	public DataSourceConnectionProvider
	dsConnectionProvider(TransactionAwareDataSourceProxy ctxProxy)
	{
		return new DataSourceConnectionProvider(ctxProxy);
	}
	
	@SuppressWarnings("serial")
	@Autowired
	public void
	configure(TransactionAspectSupport tas)
	{
		tas.setTransactionAttributeSource(new AnnotationTransactionAttributeSource(){
			@Override
			public TransactionAttribute getTransactionAttribute(Method method,
					Class<?> targetClass) {
				TransactionAttribute target = super.getTransactionAttribute(method, targetClass);
		        if (target == null)
		        {
		        	return null;
		        }
		        else
		        {
		        	return new DelegatingTransactionAttribute(target)
		        	{
						private static final long serialVersionUID = 1L;
			
						@Override
			            public boolean rollbackOn(Throwable ex) {
			                return true;
			            }
		        	};
		        }
			}
		});
	}
	
	@Bean
	@Autowired
	public DatabaseInfo
	infoBean(HikariDataSource ds)
	{
		return new DatabaseInfo(ds.getJdbcUrl(), ds.getUsername(), 
									ds.getMaximumPoolSize(),
									ds.getJdbcUrl().contains("postgres")
									? DatabaseType.POSTGRES
									: DatabaseType.HSQLDB);
	}
}
