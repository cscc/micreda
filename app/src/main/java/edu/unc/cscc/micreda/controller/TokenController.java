/*-
 * ========================LICENSE_START=================================
 * micreda
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.micreda.controller;

import java.util.UUID;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hsqldb.lib.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import edu.unc.cscc.micreda.APIConstants;
import edu.unc.cscc.micreda.config.JWTProperties;
import edu.unc.cscc.micreda.model.IdentitySource;
import edu.unc.cscc.micreda.model.MUser;
import edu.unc.cscc.micreda.model.UserIdentity;
import edu.unc.cscc.micreda.model.configuration.JWTEndpointInfo;
import edu.unc.cscc.micreda.service.IdentityResolver;
import edu.unc.cscc.micreda.service.TokenService;
import edu.unc.cscc.micreda.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = "Tokens")
@RestController
@RequestMapping("/tokens")
public class TokenController
{
	@Autowired
	private TokenService			tokenService;
	
	@Autowired
	private UserService				userService;
	
	@Autowired
	private IdentityResolver		resolver;
	
	@Autowired
	private JWTProperties			jwtProperties;
	
	/**
	 * Endpoint producing information about the JWT tokens issued by this 
	 * instance.
	 * 
	 * @return endpoint information
	 */
	@RequestMapping(value = "/info",
					method = RequestMethod.GET,
					produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Token endpoint information",
					notes = "Get information about the JWT tokens issued by "
							+ "this instance (role names, grace period, etc.)")
	@ApiResponses({
		@ApiResponse(code = 200, message = "token information obtained successfully")
	})
	public JWTEndpointInfo
	info()
	{
		return this.jwtProperties.createEndpointInfo();
	}
	
	@RequestMapping(value = "/issue",
					method = RequestMethod.GET,
					produces = {"application/jose", "application/jws"})
	@ApiOperation(value = "attempt to authenticate a user against a "
						+ "specified source", 
					notes = "Attempt to authenticate a user against a specified "
							+ "source using the supplied identity and authentication "
							+ "information.  If authentication succeeds, a JWT "
							+ "token will be issued reflecting the authenticated "
							+ "user.")
	@ApiResponses({
		@ApiResponse(code = 200, message = "authentication succeeded, token issued"),
		@ApiResponse(code = 401, message = "authentication failed"),
		@ApiResponse(code = 403, 
					message = "authentication succeeded, but user was disabled"),
		@ApiResponse(code = 500, 
					message = "Authentication suceeded, but identity was unknown.  "
							+ "This typically indicates a mis-configured identity source "
							+ "and MUST NOT be treated as a success by client applications.")		
	})
	public ResponseEntity<String>
	authenticate(@RequestParam(value = "identity-source",
								required = true)
					@NotNull @Size(min = 1)
					@ApiParam(value = "identity source identifier",
								example = APIConstants.EXAMPLE_ID_SOURCE,
								required = true)
						final String isid, 
						
					@RequestParam(value = "username",
									required = true)
					@NotNull @Size(min = 1)
					@ApiParam(value = "username to authenticate",
								example = APIConstants.EXAMPLE_USERNAME,
								required = true)
						final String username, 
						
					@RequestParam(value = "credentials", 
									required = true)
					@ApiParam(value = "credentials with which to authenticate",
								example = "hunter2", required = true)
						final String credentials,
						
					@RequestParam(value = "one-time", 
									defaultValue = "false", 
									required = false)
					@ApiParam(value = "whether to include a JTI claim (if the "
									+ "client intends for this to be a single-use "
									+ "token).  Note that micreda DOES NOT "
									+ "enforce one-time usage of tokens even "
									+ "if a 'jti' claim is present.",
								defaultValue = "false",
								required = false)
					boolean includeJTI)
	{
		
		/* first, try to find the credential source */
			
		final IdentitySource source = 
				this.resolver
					.resolveSource(isid);
		
		if (source == null || ! source.isActive())
		{
			return ResponseEntity.notFound().build();
		}
		
		/* now authenticate our user */
		
		if (! source.authenticate(username, credentials))
		{
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		
		
		/* past this point, they're authenticated */
		
		UserIdentity identity = this.resolver.resolve(username, isid);
		
		if (identity == null)
		{
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
									.body("couldn't resolve authenticated identity");
		}
		
		MUser user = identity.user();
		
		if (user.disabled())
		{
			return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
		}
		
		/* now we issue our token */
		
		return ResponseEntity.ok(this.tokenService.createToken(user, includeJTI));
	}
	
	@RequestMapping(value = "/renew",
					method = RequestMethod.POST,
					produces = {"application/jose", "application/jws"})
	@ApiOperation(value = "renew a given token",
					notes = "Renew a given token.  The given token will be "
							+ "verified and a new token issued IF AND ONLY IF "
							+ "the given token carries both a valid signature "
							+ "AND has either not expired, or is within the "
							+ "instance's configured \"grace period\".")
	@ApiResponses({
		@ApiResponse(code = 200, message = "renewal succeeded, new token issued"),
		@ApiResponse(code = 401, 
						message = "token verification failed; token is invalid "
								+ "or expired"),
		@ApiResponse(code = 403, 
						message = "Token was valid, but user is disabled or "
								+ "no longer exists.  Client applications MUST "
								+ "treat this as a failure.")
	})
	public ResponseEntity<String>
	renew(@RequestBody
			@NotNull
			@ApiParam(value = "token to renew in compact representation",
						required = true)
				String token,
			@RequestParam(value = "one-time", 
							defaultValue = "false", 
							required = false)
			@ApiParam(value = "whether to include a JTI claim in the new token"
					+ " (if the client intends for this to be a single-use "
					+ "token).  Note that micreda DOES NOT "
					+ "enforce one-time usage of tokens even "
					+ "if a 'jti' claim is present.",
				defaultValue = "false",
				required = false)
			boolean includeJTI)
	{
		if (StringUtil.isEmpty(token))
		{
			return ResponseEntity.badRequest().build();
		}
		
		try
		{
			this.tokenService.verifyToken(token);
		}
		catch (AccessDeniedException e)
		{
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		
		/* since the token was valid, we can parse out the user and re-issue */
		
		UUID userID = this.tokenService.parseUserID(token);
		
		MUser user = this.userService.load(userID);
		
		if (user == null || user.disabled())
		{
			return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
		}
		
		return ResponseEntity.ok(this.tokenService.createToken(user, includeJTI));
	}
	
	
	@RequestMapping(value = "/verify",
					method = RequestMethod.POST)
	@ApiOperation(value = "verify a token",
					notes = "Verify a given token.  The signature "
							+ "and expiration time of the token will be verified. "
							+ "Tokens that are within the instance's \"grace "
							+ "period\" will be considered valid.",
					code = 204)
	@ApiResponses({
		@ApiResponse(code = 204, message = "token was valid"),
		@ApiResponse(code = 401, message = "token was invalid or expired"),
		@ApiResponse(code = 403, 
					message = "Token was valid, but user is disabled or "
							+ "no longer exists.  Client applications MUST "
							+ "treat this as a failure.")
	})
	public ResponseEntity<?>
	verify(@RequestBody
			@NotNull
			@ApiParam(value = "token to verify, in compact form")
				final String token,
			@RequestParam(name = "check-user", required = false, defaultValue = "true")
			@ApiParam(value = "Whether to verify that the user for which the token "
							+ "was issued exists and is not disabled.  This option "
							+ "is strongly recommended.")	
				boolean checkUser)
	{
		if (StringUtil.isEmpty(token))
		{
			return ResponseEntity.badRequest().body("token was empty");
		}
		
		try
		{
			this.tokenService.verifyToken(token);
		}
		catch (AccessDeniedException e)
		{
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		
		if (checkUser)
		{
			UUID userID = this.tokenService.parseUserID(token);
			
			MUser user = this.userService.load(userID);
			
			if (user == null || user.disabled())
			{
				return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
			}
		}
		
		return ResponseEntity.noContent().build();
	}

}
