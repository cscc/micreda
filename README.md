micreda
===

What is it?
---

micreda is a lightweight authentication system.  It's open source, has a 
relatively small and easy-to-understand codebase, can be extended to talk to 
just about anything, and runs on anything with a JRE.  For persistence it can
use either a PostgresSQL 9.2+ server or an internal database (HSQL).

Requirements
---
* Java 8
* maven (to build)
* somewhere on disk that you can write to OR a PostgresSQL server (>= 9.2)

License
---

3-clause BSD.  See LICENSE.

Organization
---

micreda consists of three modules:

* **micreda-model** -- the logical model.  Contains common interfaces and model
classes used by other projects and external identity source authors.
* **micreda-app** -- the Spring Boot-based application.  Provides a RESTful API 
and persistence layer.  This is what your users/callers will talk to.
* **forger** -- the command-line administration tool.  This is optional, but is
a good reference implementation for using micreda's RESTful API.  It can 
manage multiple micreda instances, create/modify/delete users, 
associate/disassociate identities with those users, etc.
* **lazy-http-sources** -- a simple implementation of an external identity 
source designed for easy integration with anything that can take 
username/password values from a GET or POST request; you may want to use this
if you've got a legacy system with a form-based login that you need to use as
an external identity source

Building
---

To build the entire project, simply type 
    mvn package

in the top level of the project's directory structure.


Usage
---

### Core concepts

micreda is built around the idea of divorcing the logical model of a user 
(a unique identifier and/or a collection of roles) from the information used
to authenticate/identify the user to a system.

#### User

A user consists of three pieces of information: a universally-unique identifier,
a state (active/inactive), and a set of roles to which the user belongs.


#### Role

A role is an arbitrary, unique, string defined by the 
administrator(s)/end-user(s) of a micreda instance.  Roles carry no logical 
meaning or access control significance within micreda and are instead 
intended for use by client applications.  (The sole exception to this is 
the administrative role defined by the `micreda.roles.administrator` property.
Any users with this role will be treated as administrators of the micreda 
instance.)

Client applications may choose to use roles as either part of a role-based
access control scheme, or as binary permissions for their particular 
application.  micreda imposes no restrictions on role structure, although 
administrators may wish to impose organizational constraints, namespacing, etc.
for ease of administration.

#### Identity Source

An identity source is an internal or external entity capable of authenticating
user credentials.  Each source has a unique string identifier.

External identity sources are configured prior to instance startup are 
considered permanent over the runtime of an instance of micreda.  Their 
implementations may perform any arbitrary operation(s) as part of 
authenticating a user.  Typical examples might include checking a username and 
hashed password against a database or issuing a POST/GET to an HTTP endpoint.

Internal identity sources (if enabled) may be added/removed during operation.
Creation of internal identity sources may be disabled via the 
`micreda.sources.internal.freeze` property (see `application.properties`
for more details).

Identity sources are neither expected nor allowed to know anything about the 
users whose identities they are authenticating -- their knowledge of the 
micreda domain model is limited to the credentials that they are given. (That 
said, it is both possible and commonplace for an identity source to "know" 
additional information about a user due to the information being shared by 
systems external to micreda.  The identity sources are simpy "not allowed" to
get anything aside from the credentials *from micreda*.)

A recommended naming strategy for identity sources is to use colon-delimited
"coordinates" a la maven: 
`<what type of source is it>:<why is it used>:<what is it>`  
For example, when naming an  identity source that 
authenticates users in the Example company against the product backup LDAP 
server, a reasonable name might be something like 
`ldap:example-ldap-backup:alt-ldap.prod`.  This is, however, only a 
recommendation.  You can use whatever format you want (although having some 
sort of internal convention will probably make your life easier.)

##### Implementing Identity Sources

The API for identity source implementation is relatively simple and easy to 
implement, however one tip that does not appear in the code is this: handle 
timeouts gracefully.  micreda will happily block a thread for eternity 
waiting for an identity source to authenticate/deny a set of credentials, and
it is left up to the implementation(s) to ensure that they respond in a timely
manner.

This is by design.  While it would certainly be possible to have micreda 
enforce a system-wide timeout for authentication attempts, it is both possible
and probable that the definition of "too long" will vary from source to source. 
Which means that micreda would then have to handle per-source timeouts, and at
that point there's very little value to having micreda handle the timeout.  
Especially once one considers that handling and logging of the failure is 
probably source-specific anyways.

So we don't.

#### User Identity

User identities consist of a user identifier (i.e. a username) and an 
identifier of the identity source against which they may be authenticated.
Identities often correspond to accounts on remote/legacy systems with which 
micreda is integrated (ex: LDAP, intranet sites via the in-built HTTP 
connector).

Although most user identities will be authenticated using a traditional 
username/password pairing, the credential information itself (i.e. the 
"password") is not part of micreda's model, and is treated opaquely for 
all external identity sources; identity source implementations receive it 
unmodified and may do whatever they wish with it.  In this manner, any 
credentials with a string representation may be used (for example, a complex 
JSON object or a Base64 encoding of binary authentication information.)



Configuration
---

micreda is more or less usable out of the box.  It is, however, strongly 
recommended to take a look in `src/main/resources/application.properites`
to get an idea of what configuration options you can set.

All properties in said file can be set at runtime, for example:

    java -jar <path to micreda JAR> --micreda.api.key="super-secret-api-key"
    

Errata
===

Documentation
---
Due to a flaw in `swagger-core`, there appears to be no way to properly 
represent an empty body for those API operations that produce no content.  
According to the generated API spec, such operations appear to produce an empty object.  In reality they do not.  Therefore, whenever an API operation is 
documented as returning a 204 (No Content) status code, do not expect a 
response body to be present.  None will.


(This has been [reported to the swagger-core developers](https://github.com/swagger-api/swagger-core/issues/2446), but as of this writing no fix has been provided.)

Authentication/Authorization and Securing micreda
---

The way that the 'WWW-Authenticate' and 'Authorization' headers are used is 
something of a mess.

First the behavior, then how to use it, and finally the rationale.

**Behavior**


Requests for any administrative functions (i.e. anything outside of token 
issuance/verification and general server information) requires authentication.
Specifically it requires a user to be authorized who has the role designated
in `micreda.roles.administrator`.  This user may be identified and used for the 
request in one of two ways: by presentation of a valid, signed JWT token
identifying a user known to the instance or by 
presentation of a specially-designated API key header and value (see 
`micreda.api.key` and related properties).

If no authentication information is provided when attempting to access an 
administrative function, the response will carry a status code of 401, and
the client will be prompted to authenticate.
 
Where the behavior deviates from the norm is in *how* clients are prompted to 
authenticate.

As the HTTP spec does not make any special provisions for advertising multiple
authentication methods, micreda chooses a possibly-non-standard behavior and 
sends multiple `WWW-Authenticate` headers.  Specifically it will first 
advertise `Bearer`, followed by an additional instance of the header 
advertising `X-Header-Based`, which is 
itself a non-standard method (the aforementioned API key/value header-based
method.)

**Usage**

Clients wishing to authenticate may choose either of the two methods of doing 
so.

To use Bearer authentication, a token should first be obtained from the token
issuance endpoint (/tokens/issue) using a set of identity information 
(username/credentials) for the appropriate identity source.  Assuming the 
given information can authenticate a user who possess the administrative role,
the resulting token may be presented with requests to endpoints which perform 
administrative functions.

To use X-Header-Based authentication, the configured API Key (see the 
`micreda.api.key` property) should be sent as the value of the configured
API Key header (see the `micreda.api.key.header.name` property).  Assuming
successful authentication, a temporary user will be created and assigned the
configured administrative role.  This user will lack an ID and will cease to 
exist after the response is sent, thus it exists only for the purposes of 
authorization of administrative functions.

If X-Header-Based authentication is used in combination with another method,
X-Header-Based authentication will take precedence; the other method(s) will 
be used only if authentication with the first fails.


**Rationale**

Because the spec is weird.

So in order to allow micreda to have an SPA UI created for it, we need to 
support issued tokens for our administrative functions.  However, we also 
need to support a "dumb" header-based method for cases like cURL (for 
example, using a bash script to batch-load a bunch of users) or for 
applications which are stateless (and thus cannot or will not store anything
between requests).

Unfortunately, there is not a single "correct" way to do two things that we 
need to do to handle both cases.  First, there is no standard way to advertise
that you use a magic header as your authentication method, and secondly there
is no way to advertise supporting multiple authentication methods.

The first problem is relatively easy to solve.  The de-facto standard seems to 
be to report `X-Header-Based` as an authentication method, and then leave it 
up to the caller to know what the magic header is (presumably because that 
sort of thing should be documented anyways.)  This is what micreda does.

The second problem is a bit harder.  There's not any prohibition against 
reporting multiple authentication methods, but there's also not any recommended
way of doing it either.  The `WWW-Authenticate` header syntax seems to allow 
multiple challenges, however reporting them this way not only creates problems
for parsers (due to the grammar being slightly ambiguous on what is a 
challenge delimiter vs. a parameter delimiter) but also provides no discernable
advantages in micreda's case (as we neither know nor care about the UI 
implications of such a request re: browsers).

The good news is that we're allowed to send the `WWW-Authenticate` header 
multiple times, so that's what we do.  We report 'Bearer' first.  Why?  Because
in our testing dumb user agents (i.e. those that assume HTTP headers are 
unique) almost invariably take a "first one wins" approach.  Since a) we're
biased in favor of JWT and b) `Bearer` is standard while `X-Header-Based` is 
not, we want to ensure that if a client is only going to be told about one
supported method, that they hear about the preferred one.

For more details on this challenges surrounding HTTP authentication, see 
[RFC 7235](https://tools.ietf.org/html/rfc7235), particularly section 4.1.

Or don't.  It's pretty dry.


Token Renewal
---

If a token is issued for a user authenticated with a given 
username, credentials, identity source triple, the same token may be renewed
within the grace period even if the identity source against which the user 
was authenticated ceases to be active/exist.

However, a valid token may not be used for renewal if the corresponding user
has become disabled or has been deleted after issuance.
 

FAQ
===

Another authentication system?  Why?!
---

Good question.

micreda is designed to work well in environments that have some/all of the 
following requirements:

* You have multiple different sources of identity (username/credentials) 
information and you need something that handle all authentication requests and
delegate to said sources as appropriate.

* You have multiple conflicting sources of identity information (ex: username
"jdoe" in one source corresponds to a different logical user than "jdoe" in
another source)

* You like solutions that offer RESTful APIs and embrace modern lightweight 
specifications like JWT.

* You don't want to be locked into a proprietary authentication system.

* Your domain model / business / lifestyle /etc.  requires you to distinguish
between a user (i.e. a person or system with a specific set of roles) and 
the information used to describe or authenticate said user.  This is probably
the biggest reason to use micreda, as this differentiation is something that
a lot of (most?) user authentication systems do not do.

* You're a microservices fan.  micreda provides authentication and role 
management as a service and that's it.  It is designed to be as minimal and
unobtrusive as possible to integrate into your stack.


So it's awesome and I should use it?
---

Maybe.  Maybe not.  Let's flip it around and see if you have requirements that
exclude micreda from consideration.

If any of the following are true, micreda is probably *not* a good choice for
the core of your authentication system:

* You think stuff like SAML is a great idea and are heavily invested in it. If
you're already using some SAML-based solution, SSO, etc. and use it to its 
fullest then you probably have more complex authentication needs than micreda 
is designed to handle.  Go buy something from Oracle/IBM/etc.

* Everything you have and everything you will ever use speaks LDAP. 
If that's the case, go use that instead.

* You have a single source of identity information that all of your software
already can talk to.  If that's the case, why do you need an authentication 
concentrator?  You don't.  You live a happy and streamlined life.  Enjoy it!

* You can't trust intra-service connections within your organization.  micreda
is not well-equipped to deal with a threat model where service-to-service 
connections are MITM prone.  It could be extended to handle this, but as of 
the current writing it assumes that it can trust the transport layer
 (assuming you use SSL).
 
* You're a conservative large enterprise and want a solution where you can 
get consulting/support from IBM, Oracle, etc.  If that's the case then, like...
how on earth did you get *here*? Are you lost?  Did a friend send you a link 
here as a joke?  Is this an "experimental phase" in your company's life?
Anyways, try heading out in 
[this direction](http://www.ibm.com/) instead.


What storage backend should I use for micreda?
---

That depends entirely on usage.  HSQLDB is pretty damn fast for reads, so for 
testing, light office usage, etc. it might be a good starting place.  Past that
Postgres is the obvious choice, and micreda isn't really picky about what 
happens from Postgres on down (so if you get super popular and have to start
clustering, that should be fine.) 

micreda's workload is mostly read.  Once you've got your users and identity 
sources set up, the vast majority of DB usage by micreda will be reads 
(primarly of users).  The identity sources are free to use whatever storage 
backend(s) they please (although obviously their workload will be 100% read,
as the API makes no provision for identity sources to be modified by the 
application proper.)
 
 
OK, I'm going to use it.  How do I secure my server?
---

Tough question with a lot of answers.  But let's assume you've got your 
server(s)/container(s) whatever locked down and you're wondering about what
micreda itself needs?

Well in a nutshell, you'd better use SSL.  By default
micreda only does plain ol' HTTP.  This is by design, since there are a number
of environments (including the one for which it was originally created) in 
which SSL is handled by something external to the service.

But let's say you don't have something doing SSL upstream of micreda, you're 
not using it within an on-the-same-host network of containers, and you 
need it to do HTTPS only?

Simple.

Assuming you have a PKCS12 keystore with your server's cert/key, you'll need to
pass a few parameters on the command line (or, if you're building your own
modified version of micreda, you can specify them in `application.properties`).

Like so:


    java -jar <path to micreda JAR>
           --server.port=8443 
           --server.ssl.key-store=<path to keystore>
           --server.ssl.key-store-password=<keystore password>
           --server.ssl.keyStoreType=PKCS12
           --server.ssl.keyAlias=tomcat


That's pretty much it.

If you need something more advanced, remember that micreda is a Spring Boot 
app, so any sort of configuration that you could do for any other Spring Boot
app is possible here.


Hey, why is all the code formatted weird?
---

Oh.  That.  [CSCC](http://cscc.io) (who paid for development of this project) 
use Eclipse as their IDE of choice.  The dev. team there has a standard style 
and formatting setup, and that includes tabs for indentation.

\<insert standard rant about tabs vs. spaces here\>

The code lines up beautifully in Eclipse, and likely looks terrible elsewhere.

We're sorry.  There's pretty much no right answer to the tabs vs. spaces debate,
and we know whichever we pick someone's gonna be unhappy, so... sorry.  Eclipse, 
Netbeans, and VS Code should all have relatively easy ways to do 
tab/space conversions.  In theory emacs does as well, but few people have 
enough fingers to hit the correct key bindings to do so.  (But seriously, it 
does too.)

Unfortunately, this isn't the sort of thing that we can take patches for (see
above: no "right" answer), and we ask that any contributors attempt to match the
existing code style.
