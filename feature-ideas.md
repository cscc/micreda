Central management of external source configuration
---

It would be possible to allow an instance of micreda to manage a set of named
properties or configuration options, passing them to an 
`ExternalIdentitySourceFactory` during the source loading process (a similar
mechanism could also be used for `ExternalIdentitySource` instantiation, although
doing so would require sources to be at least temporarily mutable).  

This would allow for sources which require configuration information 
(paths, credentials, etc.) to obtain their configuration on startup, thereby
removing the need for such information to be managed external to micreda.

Implementation would involve changing 
`ExternalIdentitySourceFactory#obtainSources()` to take a `Properties` object, `Map`,
or other such configuration holder.  For backwards compatibility, the existing
no-arg implementation would remain, with the new method having a default
implementation which simply delegates to the existing no-arg version.