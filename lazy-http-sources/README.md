lazy-http-sources
===

This module provides a simple set of external identity sources for 
authentication against arbitrary HTTP endpoints.  It is designed to allow for
easy integration when you have "something that speaks HTTP" that you need to 
hook up micreda to.  It supports POST and GET, arbitrary parameter names, 
JSON submission, and arbitrary return code behaviors.  This covers a lot of 
legacy-integration cases; if you need something more sophisticated you might 
want to consider writing your own source.

This module also provides an example of some patterns/approaches that one may
use when writing their own external identity source(s).


## Configuration


Configurations are stored in the form of Java properties files with the 
source identifier as the file name (sans ".properties" suffix).

Duplicate identifiers (i.e. filename collisions) will be handled using a 
"last one wins" policy (see path ordering, below.)

For an example of such a file, see example.properties


### Paths


lazy-http-sources will look in the following directories for configuration
properties:

* `/etc/micreda/lazy-http-sources.d/`
* `/usr/local/etc/micreda/lazy-http-sources.d/`
* a comma-delimited list of director(ies) defined in the property
  `micreda.lazy-http-sources.config`
  
Paths which do not exist will be skipped.  Paths which exist but are not 
readable or are not directories will treated as an error.

Paths will be processed in the order they are listed above, and the order in
which they appear (for those specified by the 
`micreda.lazy-http-sources.config` property).

