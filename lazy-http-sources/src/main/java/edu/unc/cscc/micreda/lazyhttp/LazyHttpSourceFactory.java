/*-
 * ========================LICENSE_START=================================
 * lazy-http-sources
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.micreda.lazyhttp;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import edu.unc.cscc.micreda.lazyhttp.sources.FormPostRequestIdentitySource;
import edu.unc.cscc.micreda.lazyhttp.sources.GetRequestIdentitySource;
import edu.unc.cscc.micreda.lazyhttp.sources.JPRequestIdentitySource;
import edu.unc.cscc.micreda.lazyhttp.sources.SingleRequestIdentitySource;
import edu.unc.cscc.micreda.model.Discoverable;
import edu.unc.cscc.micreda.model.ext.PropertyBackedSourceFactory;

@Discoverable
public class LazyHttpSourceFactory
extends PropertyBackedSourceFactory
{
	private static final String			PROP_TYPE = "type";
	private static final String			PROP_USERNAME = "username-parameter";
	private static final String			PROP_CREDENTIALS = "credentials-parameter";
	private static final String			PROP_URL = "url";
	private static final String			PROP_CODES = "success-codes";
	private static final String			PROP_TIMEOUT = "timeout";
	private static final String			PROP_DESCRIPTION = "description";
	
	private static final String[]		REQUIRED_PROPERTIES = {PROP_TYPE, PROP_URL};
	
	private static final Properties		DEFAULT_PROPERTIES = new Properties();
	
	{{
		DEFAULT_PROPERTIES.put(PROP_CODES, 200);
		DEFAULT_PROPERTIES.put(PROP_TIMEOUT, 500);
		DEFAULT_PROPERTIES.put(PROP_DESCRIPTION, "");
		DEFAULT_PROPERTIES.put(PROP_USERNAME, "username");
		DEFAULT_PROPERTIES.put(PROP_CREDENTIALS, "password");
	}}
	
	public LazyHttpSourceFactory()
	throws URISyntaxException
	{
		super(createSearchPaths(), Arrays.asList(REQUIRED_PROPERTIES), 
					DEFAULT_PROPERTIES);
	}

	@Override
	protected SingleRequestIdentitySource
	createSource(String identifier, Properties properties)
	{
		final String description = properties.getProperty(PROP_DESCRIPTION);
		final String url = properties.getProperty(PROP_URL);
		final int timeoutMillis = 
				Integer.valueOf(properties.getProperty(PROP_TIMEOUT));
		final String usernameParameterName =
				properties.getProperty(PROP_USERNAME);
		
		final String credentialsParameterName = 
				properties.getProperty(PROP_CREDENTIALS);
		
		final String scStr = properties.getProperty(PROP_CODES);
		
		final int[] successCodes = 
				Arrays.asList(scStr.split("\\Q,\\E"))
						.stream()
						.mapToInt(Integer :: valueOf)
						.toArray();
		
		/* determine type */
		
		switch (properties.getProperty(PROP_TYPE))
		{
		case "GET":
			return new GetRequestIdentitySource(identifier, 
							description, 
							url, timeoutMillis, 
							usernameParameterName, 
							credentialsParameterName,
							successCodes);
		case "POST":
			return new FormPostRequestIdentitySource(identifier, 
														description, 
														url, timeoutMillis, 
														usernameParameterName, 
														credentialsParameterName,
														successCodes);
		case "JSON_POST":
			return new JPRequestIdentitySource(identifier, 
							description, 
							url, timeoutMillis, 
							usernameParameterName, 
							credentialsParameterName,
							successCodes);
			
		default:
			return null;
		}
	}
	
	private static final List<URI>
	createSearchPaths()
	throws URISyntaxException
	{
		List<URI> paths = new ArrayList<>();
		
		paths.add(new URI("file:///etc/micreda/lazy-http-sources.d/"));
		paths.add(new URI("file:///usr/local/etc/micreda/lazy-http-sources.d/"));
		
		
		/* now see if we have any path(s) defined via a system property */
		String propPath = System.getProperty("micreda.lazy-http-sources.config");
		if (propPath != null)
		{
			/* parse */
			String[] parsed = propPath.split("\\Q,\\E");
			for (final String p : parsed)
			{
				paths.add(new URI("file://" + p));
			}
		}
		
		return paths;
	}
}
