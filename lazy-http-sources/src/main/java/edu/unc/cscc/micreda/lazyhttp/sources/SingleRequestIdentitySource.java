/*-
 * ========================LICENSE_START=================================
 * lazy-http-sources
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.micreda.lazyhttp.sources;

import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import edu.unc.cscc.micreda.model.ExternalIdentitySource;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public abstract class SingleRequestIdentitySource 
implements ExternalIdentitySource
{
	public static final String		USER_AGENT = 
			"micreda (lazy-http)";
	
	private static final int		DEFAULT_TIMEOUT_MILLIS = 500;

	private final String			identifier;
	private final String			description;
	private final String 			url;
	private final int				timeoutMillis;
	private final OkHttpClient		client;
	
	private final int[]				successCodes;
	
	
	public SingleRequestIdentitySource(String identifier, String description,
										String url,
										int timeoutMillis, int[] successCodes)
	{
		/* TODO arg validation */
		this.identifier = identifier;
		this.description = description;
		this.url = url;
		
		if (timeoutMillis < 0)
		{
			throw new IllegalArgumentException("Timeout must be >= 0");
		}
		else if (timeoutMillis == 0)
		{
			this.timeoutMillis = DEFAULT_TIMEOUT_MILLIS;
		}
		else
		{
			this.timeoutMillis = timeoutMillis;
		}
		
		this.successCodes = Arrays.copyOf(successCodes, successCodes.length);
		
		Arrays.sort(this.successCodes);
		
		this.client = (new OkHttpClient.Builder())
							.readTimeout(this.timeoutMillis, TimeUnit.MILLISECONDS)
							.writeTimeout(this.timeoutMillis, TimeUnit.MILLISECONDS)
							.connectTimeout(this.timeoutMillis, TimeUnit.MILLISECONDS)
							.addInterceptor(new Interceptor(){

								@Override
								public Response 
								intercept(Chain chain)
								throws IOException
								{
									Request originalRequest = chain.request();
							        Request requestWithUserAgent = originalRequest.newBuilder()
							            .header("User-Agent", USER_AGENT)
							            .build();
							        return chain.proceed(requestWithUserAgent);
								}
								
							})
							.build();
	}

	@Override
	public final String
	identifier()
	{
		return this.identifier;
	}

	@Override
	public final String
	description()
	{
		return this.description;
	}
	
	
	public String
	url()
	{
		return this.url;
	}
	
	public OkHttpClient
	client()
	{
		return this.client;
	}
	
	protected final boolean
	authenticationSuccess(Response response)
	{
		return Arrays.binarySearch(this.successCodes, response.code()) >= 0;
	}
	
	
}
