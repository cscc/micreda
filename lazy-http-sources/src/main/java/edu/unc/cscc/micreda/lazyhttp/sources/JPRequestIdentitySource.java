/*-
 * ========================LICENSE_START=================================
 * lazy-http-sources
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.micreda.lazyhttp.sources;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;

public class JPRequestIdentitySource
extends SingleRequestIdentitySource
{
	private final ObjectWriter			jsonWriter;
	private final String				userName;
	private final String				credName;
	
	public JPRequestIdentitySource(String identifier, String description,
									String url, int timeoutMillis, 
									String usernamePropertyName, 
									String credentialsPropertyName,
									int[] successCodes)
	{
		super(identifier, description, url, timeoutMillis, successCodes);
		
		this.userName = usernamePropertyName;
		this.credName = credentialsPropertyName;
		
		if (this.userName == null
			|| this.userName.trim().isEmpty())
		{
			throw new IllegalArgumentException("missing username parameter name");
		}
		
		if (this.credName == null
			|| this.credName.trim().isEmpty())
		{
			throw new IllegalArgumentException("missing credentials parameter name");
		}
		
		this.jsonWriter = (new ObjectMapper()).writer();
	}



	@Override
	public boolean 
	authenticate(String username, String credentials)
	{
		Map<String, String> m = new HashMap<>();
		
		m.put(this.userName, username);
		m.put(this.credName, credentials);
		
        RequestBody requestBody;
		try
		{
			requestBody = RequestBody.create(MediaType.parse("application/json"), 
								this.jsonWriter.writeValueAsBytes(m));
		}
		catch (JsonProcessingException e1)
		{
			throw new RuntimeException("Serialization failed", e1);
		}
        
        
		
		Request req = (new Request.Builder())
							.url(HttpUrl.parse(this.url()))
							.post(requestBody)
							.build();
		
		try
		{
			return this.authenticationSuccess(this.client().newCall(req).execute());
		}
		catch (IOException e)
		{
			return false;
		}
	}

}
