/*-
 * ========================LICENSE_START=================================
 * lazy-http-sources
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.micreda.lazyhttp.sources;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.Request;
import okhttp3.RequestBody;

public class FormPostRequestIdentitySource 
extends SingleRequestIdentitySource
{

	private final String			userParm;
	private final String			credParam;

	public FormPostRequestIdentitySource(String identifier, String description,
									String url, int timeoutMillis, 
									String usernameParameterName, 
									String credentialsParameterName,
									int[] successCodes)
	{
		super(identifier, description, url, timeoutMillis, successCodes);
		
		this.userParm = usernameParameterName;
		this.credParam = credentialsParameterName;
		
		if (this.userParm == null
			|| this.userParm.trim().isEmpty())
		{
			throw new IllegalArgumentException("missing username parameter name");
		}
		
		if (this.credParam == null
			|| this.credParam.trim().isEmpty())
		{
			throw new IllegalArgumentException("missing credentials parameter name");
		}
	}



	@Override
	public boolean 
	authenticate(String username, String credentials)
	{
        RequestBody requestBody = (new FormBody.Builder())
						                .add(this.userParm, username)
						                .add(this.credParam, credentials)
						                .build();
		
		Request req = (new Request.Builder())
							.url(HttpUrl.parse(this.url()))
							.post(requestBody)
							.build();
		
		try
		{
			return this.authenticationSuccess(this.client().newCall(req).execute());
		}
		catch (IOException e)
		{
			return false;
		}
	}
}
