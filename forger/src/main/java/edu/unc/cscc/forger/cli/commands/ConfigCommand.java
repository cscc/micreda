/*-
 * ========================LICENSE_START=================================
 * forger
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.forger.cli.commands;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

import de.vandermeer.asciitable.AsciiTable;
import edu.unc.cscc.forger.cli.GlobalOptions;
import edu.unc.cscc.forger.cli.ParameterValidationException;
import edu.unc.cscc.forger.configuration.SourceConfiguration;
import edu.unc.cscc.forger.model.HeaderKeyInstance;
import edu.unc.cscc.forger.model.MicredaInstance;
import edu.unc.cscc.forger.model.UsernamePasswordInstance;

@Parameters(commandNames = ConfigCommand.NAME,
			commandDescriptionKey = ConfigCommand.NAME)
public class ConfigCommand
extends NamedCommand
{
	
	protected static final String		NAME = "config";
	
	private Collection<NamedCommand> subCommands = 
			Arrays.asList(new Add(), new Delete(), new ListInstances());
	

	@Override
	public Collection<NamedCommand>
	subCommands()
	{
		return this.subCommands;
	}
	
	@Override
	public void 
	execute(MicredaInstance ignored, GlobalOptions globalOptions, PrintStream out, PrintStream err) 
	throws Exception
	{
		throw new RuntimeException("BUG: command not directly executable");
	}

	@Override
	public boolean
	expectsInstance()
	{
		return false;
	}





	@Parameters(commandNames = "add",
				commandDescriptionKey = ConfigCommand.NAME + ".add")
	public static class Add
	extends NamedCommand
	{

		@Parameter(arity = 1, descriptionKey = NAME + ".add.name")
		private List<String> name;
		
		@Parameter(names = {"-l", "--url"},
					required = true,
					descriptionKey = NAME + ".add.url")
		private String url;
		
		@Parameter(names = {"-u", "--username"},
					descriptionKey = NAME + ".add.username")
		private String username;
		
		@Parameter(names = {"-p", "--password", "--credentials"},
					descriptionKey = NAME + ".add.credentials")
		private String password;
		
		@Parameter(names = {"-s", "--source"},
					descriptionKey = NAME + ".add.source")
		private String identitySource;
		
		@Parameter(names = {"-k", "--api-key"},
					descriptionKey = NAME + ".add.api-key")
		private String apiKey;
		
		@Parameter(names = {"-h", "--api-key-header"},
					required = false,
					descriptionKey = NAME + ".add.api-key-header")
		private String apiKeyHeader;
		
		@Parameter(names = {"-f", "--force"},
					descriptionKey = NAME + ".add.force")
		private boolean force = false;

		
		@Override
		public void
		validate() throws ParameterValidationException
		{
			if (this.name == null)
			{
				throw new ParameterValidationException("'add' requires an instance name");
			}
			else if (this.name.size() > 1)
			{
				throw new ParameterValidationException("Must specify a single instance name");
			}
			else if (this.name.size() < 1)
			{
				throw new ParameterValidationException("Must specify an instance name");
			}
			
			if (! (this.apiKey != null ^ this.username != null))
			{
				throw new ParameterValidationException(
						"Must specify --api-key OR --username");
			}
			
			/* require username/password/source if any of the three present */
			if (this.username != null)
			{
				if (StringUtils.isEmpty(this.identitySource) 
					|| this.password == null)
				{
					throw new ParameterValidationException(
							"Must specify --source, --password if using --username "
							+ "authentication");
				}
			}
			
			/* if they specified a header is it sane? */
			
			if (this.apiKeyHeader != null)
			{
				if (this.apiKeyHeader.isEmpty()
					|| StringUtils.containsWhitespace(this.apiKeyHeader))
				{
					throw new ParameterValidationException(
							"--api-key-header was given, but value was not a legal HTTP header");
				}
			}
			
			/* validate URL */
			
			URL url;
			
			try
			{
				url = new URL(this.url);
			}
			catch (MalformedURLException mue)
			{
				throw new ParameterValidationException("Malformed URL");
			}
			
			if (! (url.getProtocol().toLowerCase().contains("http")
					|| url.getProtocol().toLowerCase().contains("https")))
			{
				throw new ParameterValidationException("non-HTTP protocols are not supported");
			}
		}

		@Override
		public void 
		execute(MicredaInstance ignored, GlobalOptions globalOptions, 
					PrintStream out, PrintStream err)
		throws Exception
		{			
			
			final String name = this.name.iterator().next();
			
			MicredaInstance instance;
			
			/* first, normalize the URL */
			
			URI uri;
			
			try
			{
				uri = new URI(this.url);
				uri = uri.normalize();
			}
			catch (URISyntaxException use)
			{
				throw new ParameterValidationException("Malformed URL");
			}
			
			/* warn the user if they're doing something dumb */
			if (! uri.toURL().getProtocol().toLowerCase().startsWith("https"))
			{
				if (globalOptions.recklessUser())
				{
					out.println(
							"WARNING: you're configuring an instance that "
							+ "doesn't use SSL.  You will regret this.");
				}
				else
				{
					/* TODO - get param from globalOptions? */
					err.format("WARNING: you're trying to configure an instance without SSL.%n"
							+ "I'm refusing to do this unless you specify "
							+ "--i-am-reckless%n");
					return;
				}
			}
			
			if (! uri.toString().equals(this.url))
			{
				out.format("Normalizing %s -> %s%n", this.url, uri.toString());
			}
			
			
			if (this.apiKey == null)
			{				
				/* username/password */
				instance = new UsernamePasswordInstance(name, uri.toString(), 
														this.username, 
														this.password,
														this.identitySource);
			}
			else
			{
				
				instance = new HeaderKeyInstance(name, uri.toString(), 
										this.apiKeyHeader, this.apiKey);
			}
			
			File config = SourceConfiguration.obtainInstanceConfigFile();
			
			if (! config.exists())
			{
				if (! config.getParentFile().exists())
				{
					boolean success = config.getParentFile().mkdirs();
					
					if (! success)
					{
						throw new RuntimeException(
								String.format("cannot create config dir at '%s'",
											config.getParentFile().getAbsolutePath()));
					}
				}
				
				try
				{
					config.createNewFile();
				}
				catch (IOException ioe)
				{
					throw new RuntimeException(
							String.format("cannot create instance config file at '%s' %n", 
											config.getAbsolutePath()), 
							ioe);
				}
			}
			
			List<MicredaInstance> instances = 
					new ArrayList<>(SourceConfiguration.loadInstances(config));
			
			final boolean conflict = 
					instances.stream()
							.anyMatch(i -> i.name().equals(name));
			
			if (conflict)
			{
				if (! this.force)
				{
					err.format("refusing to replace existing instance "
							+ "with name '%s', use --force to do so%n", name);
					return;
				}
				
				instances.removeIf(i -> i.name().equals(name));
			}
			
			instances.add(instance);
			
			instances.sort((a, b) -> a.name().compareTo(b.name()));
			
			SourceConfiguration.saveInstances(instances, config);
				
		}

		@Override
		public boolean
		expectsInstance()
		{
			return false;
		}
		
	}
	
	@Parameters(commandNames = "delete",
				commandDescriptionKey = NAME + ".delete")
	public static class Delete
	extends NamedCommand
	{
		@Parameter(arity = 1, required = true,
					descriptionKey = NAME + ".delete.name")
		private List<String> name;

		@Override
		public void
		validate() throws ParameterValidationException
		{
			if (this.name == null || this.name.size() != 1)
			{
				throw new ParameterValidationException("'delete' requires instance name");
			}
		}

		@Override
		public void
		execute(MicredaInstance ignored, GlobalOptions globalOptions, PrintStream out, PrintStream err)
		throws Exception
		{			
			File config = SourceConfiguration.obtainInstanceConfigFile();
			
			final String name = this.name.iterator().next();
			
			if (! config.exists())
			{
				err.println("no instances configured");
				return;
			}
			
			Map<String, MicredaInstance> instances = 
					SourceConfiguration.loadInstances(config)
										.stream()
										.collect(Collectors.toMap(i -> i.name(), i -> i));
			
			if (! instances.containsKey(name))
			{
				err.format("unknown instance '%s'%n", name);
				return;
			}
			
			instances.remove(name);
			
			SourceConfiguration.saveInstances(instances.values(), config);
		}		
		
		@Override
		public boolean
		expectsInstance()
		{
			return false;
		}
	}
	
	@Parameters(commandNames = "list",
				commandDescriptionKey = NAME + ".list")
	public static class ListInstances
	extends NamedCommand
	{
		
		@Parameter(names = "--list-auth", 
					required = false,
					descriptionKey = NAME + ".list.list-auth")
		private boolean listAuth;

		@Override
		public void
		execute(MicredaInstance ignored, GlobalOptions globalOptions, PrintStream out, PrintStream err)
		throws Exception
		{			
			File config = SourceConfiguration.obtainInstanceConfigFile();
			
			if (! config.exists())
			{
				return;
			}
			
			Collection<? extends MicredaInstance> instances = 
					SourceConfiguration.loadInstances(config);
			
			if (instances.isEmpty())
			{
				return;
			}
			
			AsciiTable at = new AsciiTable();
			
			/* set up our header */
			at.addRule();
			
			if (this.listAuth)
			{
				at.addRow("name", "URL", "auth type", "auth data");
			}
			else
			{
				at.addRow("name", "URL", "auth type");
			}
			
			/* now the data rows */
			
			for (final MicredaInstance instance : instances)
			{
				
				Collection<String> cols = new ArrayList<>();
				
				cols.add(instance.name());
				cols.add(instance.endpointBase());
				
				if (instance instanceof UsernamePasswordInstance)
				{
					final UsernamePasswordInstance u = 
							(UsernamePasswordInstance) instance;
					
					cols.add("user");		
					
					if (this.listAuth)
					{
						cols.add("username: " + u.username() + "<br>"
									+ "password: "
									+ (StringUtils.isEmpty(u.password())
										? "no"
										: "yes"));
					}
				}
				else if (instance instanceof HeaderKeyInstance)
				{
					final HeaderKeyInstance h = 
							(HeaderKeyInstance) instance;
					
					cols.add("header");
					
					if (this.listAuth)
					{
						if (h.useDefaultHeader())
						{
							cols.add(h.key());
						}
						else
						{
							cols.add(String.format("%s [%s]", h.key(), h.headerName()));
						}
					}
				}
				else
				{
					throw new IllegalArgumentException(
							"Unknown instance type configured.  "
							+ "This is a bug in the tool.");
				}
				
				at.addRule();
				at.addRow(cols);
			}
			
			at.addRule();
			
			out.println(at.render(78));
		}
		
		@Override
		public boolean
		expectsInstance()
		{
			return false;
		}
		
		
	}
}
