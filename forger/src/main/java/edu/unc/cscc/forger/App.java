/*-
 * ========================LICENSE_START=================================
 * forger
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.forger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.MissingCommandException;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;

import edu.unc.cscc.forger.cli.CommandFailureException;
import edu.unc.cscc.forger.cli.GlobalOptions;
import edu.unc.cscc.forger.cli.InstanceParameter;
import edu.unc.cscc.forger.cli.ParameterValidationException;
import edu.unc.cscc.forger.cli.commands.AboutCommand;
import edu.unc.cscc.forger.cli.commands.AuthCommand;
import edu.unc.cscc.forger.cli.commands.ConfigCommand;
import edu.unc.cscc.forger.cli.commands.IdentityCommand;
import edu.unc.cscc.forger.cli.commands.InfoCommand;
import edu.unc.cscc.forger.cli.commands.LogCommand;
import edu.unc.cscc.forger.cli.commands.NamedCommand;
import edu.unc.cscc.forger.cli.commands.SourceCommand;
import edu.unc.cscc.forger.cli.commands.SourcesCommand;
import edu.unc.cscc.forger.cli.commands.UserCommand;
import edu.unc.cscc.forger.cli.commands.UsersCommand;
import edu.unc.cscc.forger.model.MicredaInstance;

public class App 
{
		
    public static void
    main( String[] args )
    throws Exception
    {
    	/* TODO - move this CLI junk into e.u.c.forger.cli */
    	
    	JCommander jc = new JCommander();
    	
    	jc.setDescriptionsBundle(loadParamDescriptionBundle());
    	
    	InstanceParameter ip = new InstanceParameter();
    	
    	GlobalOptions go = new GlobalOptions();
    	
    	jc.addObject(go);
    	
    	jc.addObject(ip);
    	
    	Collection<NamedCommand> topLevel = new ArrayList<>();
    	
    	Stream.of(new ConfigCommand(),
    				new AuthCommand(),
    				new AboutCommand(), 
    				new InfoCommand(),
    				new IdentityCommand(),
    				new UserCommand(),
    				new UsersCommand(),
    				new SourceCommand(),
    				new SourcesCommand(),
    				new LogCommand())
    			.forEach(c -> topLevel.add(c));
    
    	/* So this is fun... 
    	 * 
    	 * JCommander is smart enough to handle commands
    	 * having multiple names via @Parameters.  That's cool.  Except there
    	 * is no way to get references to the resulting JCommander objects for
    	 * said command names without knowing the names for use via
    	 * JCommander#getCommands().  And we need the references to add 
    	 * sub-commands.  
    	 * 
    	 * So we need to do our own annotation detection to let us add 
    	 * sub-commands to each command's JCommander instance(s).
    	 * 
    	 * JCommander *really* needs a maintainer who gives a damn about API
    	 * design and documentation (since the Javadoc is typically either 
    	 * cursory and useless or simply missing.)
    	 */
    	
    	recursiveAdd(jc, topLevel);
    	
    	
    	/* This next part is odd.  Basically, we'll try to parse the args, 
    	 * and match a command.  On exception we'll whine to stderr, but
    	 * will fall through to further down where we will print usage.
    	 * This lets us use the same code path for parse failures (where
    	 * we'll want to print usage info) as we do for users who want 
    	 * help (i.e. they're requesting usage info).
    	 */
    	
    	NamedCommand command = null;
    	boolean printCommandUsage = false;
    	
    	try
		{
			jc.parse(args);
			command = match(jc);
		}
		catch (MissingCommandException e)
		{
			/* ignored.  We'll fall through to no-command usage info below. */
		}
    	catch (ParameterException e)
    	{
    		printCommandUsage = true;
    		/* we got a param error, but try to parse the command anyways
    		 * to see if we can print command usage when we fall through below.
    		 */
    		command = match(jc);
    		
    		if (command != null)
            {
    			/* we'll only complain if they weren't asking for help.  If 
    			 * they were, then we don't care if they skipped required 
    			 * params
    			 */
    			if (! command.printHelp())
    			{
    				System.err.println(
        					"That command and those parameters don't make sense.");
    			}
            }
    		else
    		{
    			System.err.println(e.getMessage());
    		}
    	}
    	
    	
    	
        /* pull out the command */
                        
        if (command == null)
        {
        	/* do they want our version string? */
            if (go.displayVersion())
            {
            	command = new AboutCommand();
            }
            else
            {
            	/* print out command options */
            	jc.usage();
            	
            	System.exit(0);
            }
        	
        }
        else if (printCommandUsage || command.printHelp())
        {
        	StringBuilder builder = new StringBuilder(); 
        	
        	jc.usage(jc.getParsedCommand(), builder);
        	builder.append("parameters with (*) are required");
        	System.out.println(builder.toString());
        	System.exit(0);
        }
        
        /* validate the command */
        
        try
        {
			command.validate();
        }
        catch (ParameterValidationException e)
        {
        	System.err.println("ERROR: " + e.getMessage());
        	StringBuilder builder = new StringBuilder(); 
        	
        	jc.usage(jc.getParsedCommand(), builder);
        	builder.append("parameters with (*) are required");
        	System.out.println(builder.toString());
        	System.exit(-1);
        }
        
        
        
        /* below this point we know we have a valid command and they weren't
         * asking for help
         */
        
        try
		{
        	/* if we expect an instance and they specified one, try to load it */
        	
        	MicredaInstance instance = null;
        	
        	if (command.expectsInstance())
        	{
        		instance = 
        				NamedCommand.loadInstance(System.err, ip.instanceName());
        		
        		if (instance == null)
        		{
        			return;
        		}
        		
        		/* check if the user is OK with being reckless */
        		if (! instance.endpointBase().toLowerCase().startsWith("https"))
        		{
        			if (! go.recklessUser())
        			{
        				fail("You're trying to use an insecure instance (no SSL). \n"
        					 + "You must specify --i-am-reckless if you really want to do this.");
        			}
        		}
        		
        	}
        	else if (ip.instanceName() != null)
        	{
        		fail("you specified an instance, but your command "
        				+ "doesn't require it");
        	}
			
			/* execute */
			command.execute(instance, go, System.out, System.err);
		}
        catch (CommandFailureException cfe)
        {
        	fail(cfe.getMessage(), cfe.returnCode());
        }
		catch (Exception e)
		{
			String msg = e.getMessage();
			
			if (StringUtils.isEmpty(msg))
			{
				msg = "Unspecified exception of type " + e.getClass().getCanonicalName();
			}
			
			if (go.debug())
			{
				e.printStackTrace(System.err);
			}
			
			fail(msg);
		}
    }

	private static final ResourceBundle 
	loadParamDescriptionBundle()
	{
		return ResourceBundle.getBundle("parameter-descriptions", Locale.getDefault());
	}
	

	private static void
    fail(String msg)
    throws IOException
    {
    	fail(msg, -1);
    }
    
    private static void
    fail(String msg, int statusCode)
    throws IOException
    {
    	System.err.println(msg);
		System.exit(statusCode);
    }
    
    private static final void
    recursiveAdd(JCommander jc, Collection<NamedCommand> commands)
    {
    	for (NamedCommand command : commands)
    	{
    		Parameters p = command.getClass().getAnnotation(Parameters.class);
    		
    		if (p != null)
    		{
    			jc.addCommand(command);
    			
    			if (command.subCommands().isEmpty())
    			{
    				continue;
    			}
    			
    			/* add subcommands to the JCommander object corresponding to
    			 * each of the command's names
    			 */
    			for (String name : p.commandNames())
    			{
    				JCommander sub = jc.getCommands().get(name);
    				recursiveAdd(sub, command.subCommands());
    			}
    			
    		}
    	}
    }
    
    private static final NamedCommand
    match(JCommander commander)
    {
    	final String commandName = commander.getParsedCommand();
    	
    	/* parsed command will be null if we're now handling our lowest
    	 * level sub-command
    	 */
    	
    	JCommander sc = commander.getCommands().get(commandName);
    	
    	if (sc != null)
    	{
    		return match(sc);
    	}
    	
    	/* first object should be command */
    	
    	Object co = commander.getObjects().get(0);
    	
    	if (co instanceof NamedCommand)
    	{
    		return (NamedCommand) co;
    	}
    	
    	return null;
    }

    public static final class DebugParameter
    {
    	@Parameter(names = {"--debug"},
    				descriptionKey = "global.debug")
    	private boolean	debug = false;
    }
    
}
