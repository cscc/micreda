/*-
 * ========================LICENSE_START=================================
 * forger
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.forger.configuration;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.ini4j.Ini;
import org.ini4j.InvalidFileFormatException;
import org.ini4j.Profile.Section;

import edu.unc.cscc.forger.model.HeaderKeyInstance;
import edu.unc.cscc.forger.model.MicredaInstance;
import edu.unc.cscc.forger.model.UsernamePasswordInstance;

public final class SourceConfiguration
{
	
	private static final String		KEY_USERNAME = "username";
	private static final String		KEY_PASSWORD = "password";
	private static final String		KEY_IDENTITY_SOURCE = "identity_source";
	private static final String		KEY_API_KEY = "api_key";
	private static final String		KEY_API_HEADER = "api_key_header_name";
	private static final String		KEY_URL = "url";	
	
	public static final File
	obtainInstanceConfigFile()
	{
		final String homePath = System.getProperty("user.home");
		
		File home = new File(homePath);
		
		File configDir = new File(home, ".config");
		
		File forgerConfigDir = new File(configDir, "forger");
		
		return new File(forgerConfigDir, "instances.ini");
	}
	
	public static final Collection<? extends MicredaInstance>
	loadInstances(File instancesFile)
	throws InvalidFileFormatException, IOException
	{
		Ini ini = new Ini(instancesFile);
		
		return ini.entrySet()
					.stream()
					.map(e -> {
						final String instanceName = e.getKey();
						
						final Section section = e.getValue();
						
						final String url = section.get(KEY_URL);
						
						if (url == null || url.trim().isEmpty())
						{
							throw new MalformedInstanceDefinition(
									String.format("Section '%s' is missing 'url'", 
													instanceName));
						}
						
						
						if (section.containsKey(KEY_API_KEY))
						{
							if (section.containsKey(KEY_USERNAME))
							{
								throw new MalformedInstanceDefinition(
										String.format("Section '%s' specifies both "
														+ "username and API key; must "
														+ "contain only one", 
													instanceName));
							}
							/* API key */
							final String key = section.get(KEY_API_KEY);
												
							/* did they specify a custom header name? */
							final String headerName = 
									section.containsKey(KEY_API_HEADER)
											? section.get(KEY_API_HEADER)
											: HeaderKeyInstance.DEFAULT_HEADER_NAME;
											
							return new HeaderKeyInstance(instanceName, url, headerName, key);
							
						}
						else if (section.containsKey(KEY_USERNAME)
								 && section.containsKey(KEY_PASSWORD)
								 && section.containsKey(KEY_IDENTITY_SOURCE))
						{
							return new UsernamePasswordInstance(instanceName, url, 
												section.get(KEY_USERNAME),
												section.get(KEY_PASSWORD),
												section.get(KEY_IDENTITY_SOURCE));
						
						}
						else
						{
							/* nothing */
							throw new MalformedInstanceDefinition(
									String.format("Section '%s' missing API "
											+ "key or username/password/source", 
												instanceName));
						}
					})
					.collect(Collectors.toList());
	}
	
	public static final void
	saveInstances(Collection<? extends MicredaInstance> instances, File dest)
	throws IOException
	{
		if (! dest.exists())
		{
			throw new IOException("Refusing to write to non-existant file");
		}
		
		if (! dest.canWrite())
		{
			throw new IOException("Can't write to " + dest.getAbsolutePath());
		}
		
		/* first, copy and sort instances */
		final List<MicredaInstance> inst = new ArrayList<>(instances);
		
		inst.sort((a, b) -> a.name().compareTo(b.name()));
		
		Ini ini = new Ini();
				
		for (MicredaInstance instance : inst)
		{
			ini.put(instance.name(), KEY_URL, instance.endpointBase());
			
			if (instance instanceof UsernamePasswordInstance)
			{
				final UsernamePasswordInstance upi = 
						(UsernamePasswordInstance) instance;
				
				ini.put(instance.name(), KEY_USERNAME, upi.username());
				ini.put(instance.name(), KEY_PASSWORD, upi.password());
				ini.put(instance.name(), KEY_IDENTITY_SOURCE, upi.identitySource());
			}
			else if (instance instanceof HeaderKeyInstance)
			{
				final HeaderKeyInstance hki = (HeaderKeyInstance) instance;
				
				ini.put(instance.name(), KEY_API_KEY, hki.key());
				if (! hki.useDefaultHeader())
				{
					ini.put(instance.name(), KEY_API_HEADER, hki.headerName());
				}
			}
			else
			{
				throw new IllegalArgumentException("Unknown instance type");
			}
		}
		
		
		ini.store(dest);
	}
	
	
	
	public static final class MalformedInstanceDefinition
	extends RuntimeException
	{
		private static final long serialVersionUID = 1L;
		
		public MalformedInstanceDefinition(String message)
		{
			super(message);
		}
		
	}

}
