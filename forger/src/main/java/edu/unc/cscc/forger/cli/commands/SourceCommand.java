/*-
 * ========================LICENSE_START=================================
 * forger
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.forger.cli.commands;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

import edu.unc.cscc.forger.cli.GlobalOptions;
import edu.unc.cscc.forger.cli.ParameterValidationException;
import edu.unc.cscc.forger.comm.AdminClient;
import edu.unc.cscc.forger.model.MicredaInstance;
import edu.unc.cscc.micreda.model.IdentitySourceAttributes;

@Parameters(commandNames = SourceCommand.NAME,
			commandDescriptionKey = SourceCommand.NAME)
public final class SourceCommand 
extends NamedCommand
{
	
	protected static final String			NAME = "source";
	
	private final Collection<NamedCommand>	subCommands;
	
	
	public SourceCommand()
	{		
		this.subCommands = new ArrayList<>();
		
		Stream.of(new CreateSourceCommand(),
					new QuerySourceCommand(),
					new UpdateSourceCommand())
				.forEach(s -> this.subCommands.add(s));
		
	}

	@Override
	public Collection<NamedCommand> 
	subCommands()
	{
		return Collections.unmodifiableCollection(this.subCommands);
	}

	@Override
	public boolean 
	expectsInstance()
	{
		return false;
	}

	@Override
	public void 
	execute(MicredaInstance instance, GlobalOptions globalOptions,
			PrintStream out, PrintStream err)
	throws Exception
	{
		throw new RuntimeException("BUG: command not directly executable");
	}
	
	@Parameters(commandNames = "query",
				commandDescriptionKey = NAME + ".query")
	private static final class QuerySourceCommand
	extends NamedCommand
	{

		@Parameter(descriptionKey = NAME + ".query.names",
					required = true)
		private List<String>	args = new ArrayList<>();


		@Override
		public void 
		validate() throws ParameterValidationException
		{
			if (this.args.isEmpty()
				|| StringUtils.isEmpty(this.args.get(0)))
			{
				throw new ParameterValidationException("identity source name is required");
			}
			
			for (String s : this.args)
			{
				if (StringUtils.isEmpty(s))
				{
					throw new ParameterValidationException(
							"identity source names must not be empty");
				}
			}
		}

		@Override
		public void
		execute(MicredaInstance instance, GlobalOptions globalOptions,
				PrintStream out, PrintStream err)
		throws Exception
		{
			
			AdminClient client = new AdminClient(instance);
			
			Set<String> names = new HashSet<>(this.args);
			
			List<IdentitySourceAttributes> sources = new ArrayList<>(names.size());
			
			for (final String name : names)
			{
				if (globalOptions.verbose())
				{
					out.println("info: querying instance for source '" + name + "'");
				}
				
				IdentitySourceAttributes source = client.findIdentitySource(name, true, true);
				
				if (source == null)
				{
					out.println("no source with name '" + name + "' known to instance");
				}
				else
				{
					sources.add(source);
				}
				
			}
			
			Iterator<IdentitySourceAttributes> si = sources.iterator();
			
			while (si.hasNext())
			{
				final IdentitySourceAttributes source = si.next();
				
				out.format("source: %s%n"
						+ "description: %s%n"
						+ "external: %s%n"
						+ (si.hasNext() ? "---%n" : ""),
						source.identifier(), source.description(),
						source.isExternal() ? "yes" : "no");
			}
			
		}
		
	}
	
	@Parameters(commandNames = "update",
				commandDescriptionKey = NAME + ".update")
	private static final class UpdateSourceCommand
	extends NamedCommand
	{
		@Parameter(descriptionKey = NAME + ".update.args")
		private List<String>	args = new ArrayList<>();

		@Override
		public void 
		validate() throws ParameterValidationException
		{
			if (this.args.size() < 2
				|| StringUtils.isEmpty(this.args.get(0)))
			{
				throw new ParameterValidationException(
						"identity source identifier  and description are required");
			}
						
			if (this.args.size() > 2)
			{
				throw new ParameterValidationException(
						"Too many arguments.  Need identifier and description only");
			}
		}

		@Override
		public void 
		execute(MicredaInstance instance, GlobalOptions globalOptions,
				PrintStream out, PrintStream err)
		throws Exception
		{
			final String identifier = this.args.get(0);
			
			final String description = 
					(this.args.size() > 1 ? this.args.get(1) : null);
			
			if (globalOptions.verbose())
			{
				out.format("Creating identity source with identifier '%s' and %n"
						+ "description: %s%n", identifier,
						(StringUtils.isEmpty(description)
							? "none"
							: "'" + description + "'"));
			}
			
			AdminClient client = new AdminClient(instance);
			
			/* check that the source is internal */
			
			IdentitySourceAttributes existing = 
					client.findIdentitySource(identifier, true, false);
			
			if (existing == null)
			{
				err.println("Instance does not have an internal source with that identifier");
				return;
			}
			
			client.saveInternalSource(identifier, description);
			
			
			
		}
	}

	@Parameters(commandNames = "create",
				commandDescriptionKey = NAME + ".create")
	private static final class CreateSourceCommand
	extends NamedCommand
	{
		
		@Parameter(descriptionKey = NAME + ".create.args")
		private List<String>	args = new ArrayList<>();


		@Override
		public void 
		validate()
		throws ParameterValidationException
		{
			if (this.args.isEmpty()
				|| StringUtils.isEmpty(this.args.get(0)))
			{
				throw new ParameterValidationException(
						"identity source identifier is required");
			}
			
			if (this.args.size() > 2)
			{
				throw new ParameterValidationException(
						"Too many arguments.  Need identifier and description only");
			}
		}

		@Override
		public void 
		execute(MicredaInstance instance, GlobalOptions globalOptions,
				PrintStream out, PrintStream err)
		throws Exception
		{
			final String identifier = this.args.get(0);
			
			final String description = 
					(this.args.size() > 1 ? this.args.get(1) : null);
			
			if (globalOptions.verbose())
			{
				out.format("Creating identity source with identifier '%s' and %n"
						+ "description: %s%n", identifier,
						(StringUtils.isEmpty(description)
							? "none"
							: "'" + description + "'"));
			}
			
			AdminClient client = new AdminClient(instance);
			
			IdentitySourceAttributes source = client.findIdentitySource(identifier, true, true);
			
			if (source != null)
			{
				out.println("A source with that identifier already exists.  To "
						+ "update try 'source update' instead.");
				
				return;
			}
			
			client.saveInternalSource(identifier, description);
			
			
			
		}
		
	}
	
}
