/*-
 * ========================LICENSE_START=================================
 * forger
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.forger.comm;

import java.io.IOException;

import edu.unc.cscc.forger.model.HeaderKeyInstance;
import edu.unc.cscc.forger.model.MicredaInstance;
import edu.unc.cscc.forger.model.UsernamePasswordInstance;
import edu.unc.cscc.forger.model.MicredaInstance.Endpoint;
import okhttp3.Authenticator;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.Route;

/*
 * TODO
 * 
 * For username/password auth we could probably save off the token that we
 * get after successful auth and use that for future requests until expiration.
 * 
 * That would require cooperating from the instance-loading layer though,
 * plus the ability to inform said layer that we have a new token (when 
 * handling renewals.)  And we don't have time to implement that right now.
 * Auth-per-request works fine, although it is a bit inefficient.
 */
public class MicredaInstanceAuthenticator
implements Authenticator
{
	private final MicredaInstance		instance;
	private final OkHttpClient			client;

	public MicredaInstanceAuthenticator(MicredaInstance instance)
	{
		this.instance = instance;
		this.client = new OkHttpClient();
	}

	@Override
	public Request 
	authenticate(Route route, Response response)
	throws IOException
	{
		if (this.instance instanceof UsernamePasswordInstance)
		{
			if (response.request().header("Authorization") != null)
			{
				/* already tried auth */
				return null;
			}
			
			final UsernamePasswordInstance upi = (UsernamePasswordInstance) this.instance;

			/* TODO - do JWT auth */
			Request req = (new Request.Builder())
								.url(HttpUrl.parse(this.instance.endpointURL(Endpoint.TOKENS))
										.newBuilder()
										.addPathSegment("issue")
										.addQueryParameter("username", upi.username())
										.addQueryParameter("credentials", upi.password())
										.addQueryParameter("identity-source", upi.identitySource())
										.build())
								.addHeader("Accept", 
											"application/jwt; q=1,"
											+ "application/jose; q=1, "
											+ "application/jws; q=1,"
											+ "*/*; q=0")
								.get()
								.build();
			
			Response resp = this.client.newCall(req).execute();
			
			if (resp.isSuccessful() && resp.code() == 200)
			{
				String ts = resp.body().string();
				
				/* got something that looks like a token, so the request again */
				return response
						.request()
						.newBuilder()
						.addHeader("Authorization", "Bearer " + ts)
						.build();
			}
			else if (resp.code() == 404)
			{
				throw new IOException(String.format(
						"Authentication endpoint rejected our request.  "
						+ "Are you sure that your configured identity source "
						+ "('%s') exists?",
						upi.identitySource()));
			}
			else if (resp.code() == 403)
			{
				throw new RuntimeException("Token authorization failed");
			}
			
			return null;
		}
		else if (this.instance instanceof HeaderKeyInstance)
		{
			final HeaderKeyInstance h = (HeaderKeyInstance) this.instance;

			if (response.request().header(h.headerName()) != null)
			{
				/* already tried auth */
				return null;
			}
			
			return response
					.request()
					.newBuilder()
					.addHeader(h.headerName(), h.key())
					.build();
		}
		else
		{
			throw new IllegalArgumentException(
					"micreda instance was of unknown type");
		}
	}

}
