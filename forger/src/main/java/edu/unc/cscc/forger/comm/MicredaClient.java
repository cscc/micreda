/*-
 * ========================LICENSE_START=================================
 * forger
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.forger.comm;

import java.io.IOException;
import java.io.Reader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import edu.unc.cscc.forger.model.MicredaInstance;
import edu.unc.cscc.forger.model.MicredaInstance.Endpoint;
import edu.unc.cscc.micreda.model.configuration.MicredaEndpointInfo;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;

abstract class MicredaClient
{
	
	public static final String[]		SUPPORTED_API_VERSIONS = {"0.1"};
	
	protected static final String		INFO_ENDPOINT = "/info";
	
	protected static final MediaType	JSON_TYPE = 
			MediaType.parse("application/json; charset=utf-8");
	
	
	private static final ObjectMapper	MAPPER = new ObjectMapper();
	
	private final MicredaInstance		instance;
	
	private final OkHttpClient			client;
	
	
	
	MicredaClient(MicredaInstance instance)
	{
		this.instance = instance;
		this.client = (new OkHttpClient.Builder())
						.authenticator(new MicredaInstanceAuthenticator(instance))
						.build();
	}
	
	/**
	 * Check whether the instance reports an API version that is 
	 * compatible with the current client.
	 * 
	 * @throws IOException thrown if an error occurred in transit
	 * @throws IncompatibleInstanceException thrown if the given instance is
	 * not compatible with the client
	 * @throws URISyntaxException 
	 * @throws UnirestException 
	 */
	public void
	checkCompatibility()
	throws IOException, IncompatibleInstanceException, 
			URISyntaxException
	{
		Request request = (new Request.Builder())
							.url(this.endpointURL(INFO_ENDPOINT))
							.get()
							.build();
		
		MicredaEndpointInfo info = 
				read(this.client
						.newCall(request)
						.execute()
						.body().charStream(),
						MicredaEndpointInfo.class);
			
		
		if (! Arrays.asList(SUPPORTED_API_VERSIONS).contains(info.apiVersion()))
		{
			throw new IncompatibleInstanceException(
					"Client not compatible with API version " + info.apiVersion());
		}			
	}
	
	protected OkHttpClient
	client()
	{
		return this.client;
	}
	
	protected final HttpUrl
	endpointURL()
	throws URISyntaxException
	{
		return HttpUrl.parse(normalizeUrl(this.instance.endpointBase()));
	}
	
	protected final HttpUrl
	endpointURL(String endpoint) 
	throws URISyntaxException
	{
		return HttpUrl.parse(normalizeUrl(this.instance.endpointBase() + endpoint));
	}
	
	protected final HttpUrl
	endpointURL(Endpoint endpoint, String ... additional)
	throws URISyntaxException
	{
		return HttpUrl.parse(
				normalizeUrl(
					this.instance.endpointURL(endpoint)
					+ Arrays.asList(additional)
							.stream()
							.collect(Collectors.joining())));
	}
	
	static ObjectMapper
	mapper()
	{
		return MAPPER;
	}
	
	static <T> T 
	read(Reader reader, Class<T> cls) 
	throws JsonProcessingException, IOException
	{
		return MAPPER.readerFor(cls).readValue(reader);
	}
	
	static String
	toJSON(Object o)
	throws JsonProcessingException
	{
		return MAPPER.writeValueAsString(o);
	}
	
	static String
	normalizeUrl(String url)
	throws URISyntaxException
	{
		return (new URI(url).normalize()).toString();
	}
	
	public static class IncompatibleInstanceException
	extends Exception
	{
		private static final long serialVersionUID = 1L;

		public IncompatibleInstanceException(String msg)
		{
			super(msg);
		}
	}
}
