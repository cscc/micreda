/*-
 * ========================LICENSE_START=================================
 * forger
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.forger.cli.commands;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Properties;

import com.beust.jcommander.Parameters;

import edu.unc.cscc.forger.cli.GlobalOptions;
import edu.unc.cscc.forger.model.MicredaInstance;

@Parameters(commandNames = "about",
			commandDescriptionKey = "about")
public class AboutCommand
extends NamedCommand
{

	private static final String		VERSION_FILE = "version.properties";
	
	@Override
	public void 
	execute(MicredaInstance ignored, GlobalOptions globalOptions, 
				PrintStream out, PrintStream err)
	throws Exception
	{		
		Properties props = new Properties();
		
		InputStream is = 
				this.getClass()
					.getClassLoader()
					.getResourceAsStream(VERSION_FILE);
		
		props.load(is);
		
		out.println(props.getProperty("name") 
					+ " " + props.getProperty("version"));
		out.println(props.getProperty("copyright"));
		out.println(props.getProperty("description"));
	}
	
	@Override
	public boolean
	printHelp()
	{
		return false;
	}
	
	
	@Override
	public boolean
	expectsInstance()
	{
		return false;
	}

}
