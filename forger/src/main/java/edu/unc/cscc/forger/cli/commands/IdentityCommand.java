/*-
 * ========================LICENSE_START=================================
 * forger
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.forger.cli.commands;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

import edu.unc.cscc.forger.cli.GlobalOptions;
import edu.unc.cscc.forger.cli.ParameterValidationException;
import edu.unc.cscc.forger.comm.AdminClient;
import edu.unc.cscc.forger.comm.UserClient;
import edu.unc.cscc.forger.model.MicredaInstance;
import edu.unc.cscc.micreda.model.IdentitySourceAttributes;
import edu.unc.cscc.micreda.model.MUser;
import edu.unc.cscc.micreda.model.UserIdentity;

@Parameters(commandNames = "identity")
public class IdentityCommand
extends NamedCommand
{

	private final Collection<NamedCommand>		subCommands;
	
	public IdentityCommand()
	{
		this.subCommands = new ArrayList<>();
		
		Stream.of(new AssocCommand(), new DisassocCommand(), new QueryCommand())
				.forEach(c -> this.subCommands.add(c));
	}


	@Override
	public Collection<NamedCommand> 
	subCommands()
	{
		return Collections.unmodifiableCollection(this.subCommands);
	}

	@Override
	public void 
	execute(MicredaInstance instance, GlobalOptions globalOptions,
			PrintStream out, PrintStream err) throws Exception
	{
		throw new RuntimeException("BUG: command not directly executable");
	}
	
	@Parameters(commandNames = "query")
	private final class QueryCommand
	extends NamedCommand
	{
		@Parameter(names = {"-u", "--username"}, required = true)
		private String			username;
		@Parameter(names = {"-s", "--source"}, required = true)
		private String			sourceIdentifier;

		
		@Override
		public void
		validate() throws ParameterValidationException
		{
			if (StringUtils.isEmpty(this.username))
			{
				throw new ParameterValidationException("username is required");
			}
			
			if (StringUtils.isEmpty(this.sourceIdentifier))
			{
				throw new ParameterValidationException("password is required");
			}
		}
		
		@Override
		public void execute(MicredaInstance instance, GlobalOptions opts, 
							PrintStream out, PrintStream err)
		throws Exception
		{
			UserClient client = new UserClient(instance);
			
			UserIdentity identity = 
					client.loadIdentity(username, sourceIdentifier);
			
			if (identity != null)
			{
				out.println(identity.user().id().toString());
			}
		}
		
		
	}
	
	@Parameters(commandNames = "assoc")
	private final class AssocCommand
	extends NamedCommand
	{
		
		@Parameter(arity = 1)
		private List<String>	ids = new ArrayList<>();

		@Parameter(names = {"-u", "--username"}, required = true)
		private String 			username;
		@Parameter(names = {"-s", "--source"}, required = true)
		private String			sourceIdentifier;
		@Parameter(names = {"-p", "--password"})
		private String			password;


		@Override
		public void 
		validate() throws ParameterValidationException
		{
			/* only one ID */
			if (this.ids.size() != 1)
			{
				throw new ParameterValidationException("Must specify a single user ID");
			}
			
			/* username and source are both mandatory */
			if (StringUtils.isEmpty(this.username))
			{
				throw new ParameterValidationException("missing username");
			}
			
			if (StringUtils.isEmpty(this.sourceIdentifier))
			{
				throw new ParameterValidationException("missing identity source name");
			}
		}



		@Override
		public void 
		execute(MicredaInstance instance, GlobalOptions globalOptions, 
				PrintStream out, PrintStream err)
		throws Exception
		{
			final String idStr = this.ids.get(0);
			
			AdminClient adminClient = new AdminClient(instance);
			UserClient userClient = new UserClient(instance);
			
			/* resolve it to a user */
			MUser user = userClient.loadByShortID(idStr);
			
			if (user == null)
			{
				err.println("No user found with given ID.  If this is a "
						+ "partial ID you may need to specify more of the ID");
				
				return;
			}
			
			/* check if identity is in use */
			UserIdentity identity = 
					userClient.loadIdentity(this.username, this.sourceIdentifier);
			
			if (identity != null)
			{
				if (! user.id().equals(identity.user().id()))
				{
					err.println("An identity with that username / source is already associated");
					err.println("with another user.  You must disassociate that identity");
					err.println("before you can associate it with the given user.");
					
					return;
				}
			}
			
			
			IdentitySourceAttributes internal = 
					adminClient.findIdentitySource(this.sourceIdentifier, true, false);
			
			UserIdentity id = 
					new UserIdentity(user, this.username, this.sourceIdentifier);
			
			/* this will let us determine internal/external */
			if (internal != null)
			{
				if (this.password == null)
				{
					err.println("Specified identity source is internal.  You'll "
							+ "need to specify --password");
					err.println("if you want to set/update the internal identity's password.");
					err.println("No changes have been made.");
					return;
				}
				
				/* internal identity source exists and they specified a PW so
				 * we're good to set/associate that identity with the given
				 * user.
				 */
				
				
				id = userClient.saveInternal(id, this.password);
				
				/* TODO - warn if source is disabled */
				
				return;
				
			}
			else
			{
				
				/* save */
				
				id = userClient.saveExternal(id);
				
				/* finally, check if the external ID source exists, and warn if not */
				
				IdentitySourceAttributes source = 
						adminClient.findIdentitySource(this.sourceIdentifier, false, true);
				
				if (source == null)
				{
					out.println("WARNING: specified source is not known to instance");
					out.println("User will not be able to authenticate until an external source");
					out.println("with that identifier is available.");
				}
			}
			
			
			
			
		}
		
	}

	@Parameters(commandNames = "disassoc")
	private final class DisassocCommand
	extends NamedCommand
	{
		@Parameter
		private List<String>	ids = new ArrayList<>();

		@Parameter(names = {"-u", "--username"})
		private String			username;
		@Parameter(names = {"-s", "--source"})
		private String			source;
		
		@Parameter(names = "--all")
		private boolean			all;
		
		@Parameter(names = "--batch")
		private boolean			batch;

		@Override
		public void
		validate() throws ParameterValidationException
		{
			if (this.ids.isEmpty())
			{
				throw new ParameterValidationException("Missing user ID(s)");
			}
			
			if (this.ids.size() > 1
				&& ! this.batch)
			{
				throw new ParameterValidationException(
						"More than one ID was specified.  Use --batch if you really "
						+ "want to disassociate identities for all specified users");
			}
			
			if (this.ids.size() > 1 
				&& ! StringUtils.isEmpty(this.username))
			{
				throw new ParameterValidationException(
						"More than one ID was specified.  Specifying username makes no sense here.");
			}
			
			if (StringUtils.isEmpty(source)
				&& ! StringUtils.isEmpty(username))
			{
				throw new ParameterValidationException(
						"You specified a username but not a source.  "
						+ "I don't think you want that.");
			}
			
			if (this.all)
			{
				if (! StringUtils.isEmpty(this.username)
					|| ! StringUtils.isEmpty(this.source))
				{
					throw new ParameterValidationException(
							"--all and username/source specification are mutually exclusive");
				}
			}
			
			if (this.username != null && this.username.isEmpty())
			{
				throw new ParameterValidationException("Username must be non-empty if specified");
			}
		}



		@Override
		public void 
		execute(MicredaInstance instance, GlobalOptions opts, 
				PrintStream out, PrintStream err)
		throws Exception
		{
			UserClient client = new UserClient(instance);
			
			Collection<MUser> users = new ArrayList<>();
			
			for (String id : this.ids)
			{
				MUser user = client.loadByShortID(id);
				
				if (user == null)
				{
					err.println("Unknown user or ambiguous ID (ID %s)");
					return;
				}
				
				users.add(user);
			}
			
			for (MUser user : users)
			{
				if (this.all)
				{
					/* nuke everything */
					if (opts.verbose())
					{
						out.format("Dissociating all identities for user %s ...", 
									user.id());
					}
					
					try
					{
						client.deleteAllIdentities(user);
						if (opts.verbose())
						{
							out.println("[OK]");
						}
					}
					catch (Exception e)
					{
						if (opts.verbose())
						{
							out.println("[ERROR]");
						}
						throw e;
					}
					
				}
				else
				{
					if (opts.verbose())
					{
						if (StringUtils.isEmpty(this.username))
						{
							out.format("Dissociating identities from '%s' for user %s ...", 
									this.source,
									user.id());
						}
						else
						{
							out.format("Dissociating identity '%s' from '%s' for user %s ...", 
									this.username,
									this.source,
									user.id());
						}
					}
					
					try
					{
						client.deleteIdentities(user, this.username, this.source);
						if (opts.verbose())
						{
							out.println("[OK]");
						}
					}
					catch (Exception e)
					{
						if (opts.verbose())
						{
							out.println("[ERROR]");
						}
						throw e;
					}	
				}
			}
		}
	}
	
}
