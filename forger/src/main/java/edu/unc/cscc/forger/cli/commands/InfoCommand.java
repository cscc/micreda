/*-
 * ========================LICENSE_START=================================
 * forger
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.forger.cli.commands;

import java.io.PrintStream;
import java.util.Collection;

import com.beust.jcommander.Parameters;

import de.vandermeer.asciitable.AsciiTable;
import edu.unc.cscc.forger.cli.GlobalOptions;
import edu.unc.cscc.forger.comm.AdminClient;
import edu.unc.cscc.forger.comm.UserClient;
import edu.unc.cscc.forger.model.MicredaInstance;
import edu.unc.cscc.micreda.model.IdentitySourceAttributes;
import edu.unc.cscc.micreda.model.configuration.MicredaEndpointInfo;

@Parameters(commandNames = "info",
			commandDescriptionKey = "info")
public class InfoCommand 
extends NamedCommand
{

	@Override
	public void
	execute(MicredaInstance instance, GlobalOptions globalOptions, PrintStream out, PrintStream err)
	throws Exception
	{
		AdminClient client = new AdminClient(instance);
		UserClient userClient = new UserClient(instance);
		
		MicredaEndpointInfo info = client.retrieveInfo();
		
		int userCount = userClient.countUsers();
		
		Collection<? extends IdentitySourceAttributes> ids = 
				client.searchSources(null, true, true, true);
		
		int totalSource = ids.size();
		
		long activesources = ids.stream().filter(s -> s.isActive()).count();
		
		
		/* display */
				
		AsciiTable table = new AsciiTable();
		
		table.addRule();
		table.addRow("host", "API version", "app version", "allows source creation");
		table.addRule();
		table.addRow(info.hostname(), info.apiVersion(), info.version(), 
						(info.allowInternalSourceCreation() ? "yes" : "no"));
		
		table.addRule();
		
		table.addRow("Total users: " + userCount, 
						"", null,
						"Total identity sources: " 
						+ totalSource 
						+ " (" + activesources + " active)");
		
		table.addRule();
		
		
		out.println(table.render(78));
	}

}
