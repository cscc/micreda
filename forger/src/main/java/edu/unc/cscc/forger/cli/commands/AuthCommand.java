/*-
 * ========================LICENSE_START=================================
 * forger
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.forger.cli.commands;

import java.io.PrintStream;

import org.apache.commons.lang3.StringUtils;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

import edu.unc.cscc.forger.cli.CommandFailureException;
import edu.unc.cscc.forger.cli.GlobalOptions;
import edu.unc.cscc.forger.cli.ParameterValidationException;
import edu.unc.cscc.forger.comm.AdminClient;
import edu.unc.cscc.forger.comm.UserClient;
import edu.unc.cscc.forger.model.MicredaInstance;
import edu.unc.cscc.micreda.model.UserIdentity;
import edu.unc.cscc.micreda.model.configuration.JWTEndpointInfo;

@Parameters(commandNames = AuthCommand.NAME,
			commandDescriptionKey = AuthCommand.NAME)
public final class AuthCommand
extends NamedCommand
{
	
	protected static final String		NAME = "auth";
	
	@Parameter(names = {"-s", "--source"},
				required = true,
				descriptionKey = NAME + ".source")
	private String		sourceIdentifier;
	
	@Parameter(names = {"-u", "--username"}, 
				required = true,
				descriptionKey = NAME + ".username")
	private String		username;
	
	@Parameter(names = {"-p", "--password", "--credentials"}, 
				required = true,
				descriptionKey = NAME + ".credentials")
	private String		password;
	
	
	@Override
	public void 
	validate() throws ParameterValidationException
	{
		if (StringUtils.isEmpty(this.sourceIdentifier))
		{
			throw new ParameterValidationException("missing source identifier");
		}
		
		if (StringUtils.isEmpty(this.username))
		{
			throw new ParameterValidationException("missing username");
		}
		
		if (StringUtils.isEmpty(this.password))
		{
			throw new ParameterValidationException("missing password");
		}
	}

	@Override
	public void 
	execute(MicredaInstance instance, GlobalOptions globalOptions,
				PrintStream out, PrintStream err)
	throws Exception
	{
		UserClient client = new UserClient(instance);
		AdminClient adminClient = new AdminClient(instance);
		
		JWTEndpointInfo epi = adminClient.retrieveJWTInfo();
		
		UserIdentity identity = 
				client.authenticate(epi, this.sourceIdentifier, this.username, 
									this.password);
		
		if (identity == null)
		{
			throw new CommandFailureException("authentication failed");
		}
		
		out.format("authenticated user %s%n", identity.user().id().toString());
	}

	
	
}
