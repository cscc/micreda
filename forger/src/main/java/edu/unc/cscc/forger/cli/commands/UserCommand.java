/*-
 * ========================LICENSE_START=================================
 * forger
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.forger.cli.commands;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

import edu.unc.cscc.forger.cli.GlobalOptions;
import edu.unc.cscc.forger.cli.ParameterValidationException;
import edu.unc.cscc.forger.comm.UserClient;
import edu.unc.cscc.forger.comm.UserClient.UserNotFoundException;
import edu.unc.cscc.forger.model.MicredaInstance;
import edu.unc.cscc.micreda.model.MUser;
import edu.unc.cscc.micreda.model.UserIdentity;

@Parameters(commandNames = "user",
			commandDescriptionKey = UserCommand.NAME)
public class UserCommand 
extends NamedCommand
{
	protected static final String				NAME = "user";
	
	private final Collection<NamedCommand>		subCommands;
	
	
	public UserCommand()
	{		
		this.subCommands = new ArrayList<>();
		
		Stream.of(new CreateSub(),
					new DisableSub(),
					new EnableSub(),
					new QuerySub(),
					new UpdateSub())
				.forEach(s -> this.subCommands.add(s));
		
	}

	@Override
	public void
	execute(MicredaInstance instance, GlobalOptions globalOptions, PrintStream out, PrintStream err)
	throws Exception
	{
		out.println("Command not directly executable");
	}
	
	@Override
	public Collection<NamedCommand> 
	subCommands()
	{
		return Collections.unmodifiableCollection(this.subCommands);
	}
	
	@Parameters(commandNames = "query",
				commandDescriptionKey = NAME + ".query")
	private final class QuerySub
	extends NamedCommand
	{
		
		@Parameter(required = false,
					descriptionKey = NAME + ".query.ids")
		private List<String>	ids;
		
		@Parameter(names = {"-e", "--identities"},
					descriptionKey = NAME + ".query.identities")
		private boolean			includeIdentities = false;

		@Override
		public void 
		validate() throws ParameterValidationException
		{
			if (this.ids == null || this.ids.isEmpty())
			{
				throw new ParameterValidationException("Must specify user ID");
			}
			
			if (this.ids.size() != 1)
			{
				throw new ParameterValidationException("Must specify single user ID");
			}
		}

		@Override
		public void
		execute(MicredaInstance instance, GlobalOptions globalOptions, 
					PrintStream out, PrintStream err)
		throws Exception
		{
			final UserClient client = new UserClient(instance);
			
			final MUser user = client.loadByShortID(this.ids.get(0));
			
			/* TODO - formalize this for other commands, show
			 * ambiguous options (requires controller support)
			 */
			if (user == null)
			{
				out.format(
						"Specified ID '%s' doesn't correspond to a user known to the instance %n"
						+ "'%s'.  If this is a partial ID, you may need to specify more of the %n"
							+ "ID to unambiguously resolve a user.%n", 
							this.ids.get(0), instance.name());
				return;
			}
			

			Collection<UserIdentity> identities = Collections.emptySet();

			
			if (this.includeIdentities)
			{
				identities = client.loadIdentities(user.id());
			}
			
			out.println("ID: " + user.id());
			out.println("disabled: " + (user.disabled() ? "yes" : "no"));
			
			if (! user.roles().isEmpty())
			{
				out.println("roles: " + 
						user.roles()
							.stream()
							.map(r -> "'" + r + "'")
							.collect(Collectors.joining(", ")));
			}
			
			if (this.includeIdentities)
			{
				/* collect by source */
				
				Map<String, Collection<UserIdentity>> bySource = 
						Util.partitionIdentitiesBySource(identities);
				
				bySource.forEach((source, ids) -> {
					
					out.format("@'%s : %s%n", source,
								ids.stream()
									.map(i -> "'" + i.username() + "'")
									.collect(Collectors.joining(", ")));
				});
				
			}
		}
		
	}
	
	@Parameters(commandNames = "create",
					commandDescriptionKey = NAME + ".create")
	private final class CreateSub
	extends NamedCommand
	{
		
		@Parameter(required = false,
						descriptionKey = NAME + ".create.roles")
		private List<String>	roles;


		@Override
		public void
		execute(MicredaInstance instance, GlobalOptions globalOptions, 
					PrintStream out, PrintStream err)
		throws Exception
		{
			final Set<String> roles = new HashSet<>();
			final Set<String> duplicates = new HashSet<>();
			
			if (! (this.roles == null || this.roles.isEmpty()))
			{
				for (final String role : this.roles)
				{
					if (roles.contains(role))
					{
						duplicates.add(role);
					}
					else
					{
						roles.add(role);
					}
				}
			}
			
			if (! duplicates.isEmpty())
			{
				out.format("INFO: ignoring duplicate role specification "
						+ "(roles: %s)%n",
						duplicates
							.stream()
							.map(s -> "'" + s + "'")
							.collect(Collectors.joining(", ")));
			}
			
			
			
			UserClient client = new UserClient(instance);
			
			
			MUser created = client.createUser(roles);
			
			out.println("created user with ID " + created.id());
			
		}
		
	}
	
	@Parameters(commandNames = {"update", "roles"},
				commandDescriptionKey = NAME + ".update")
	private final class UpdateSub
	extends NamedCommand
	{

		@Parameter(required = false,
					descriptionKey = NAME + ".update.ids")
		private List<String>		ids = new ArrayList<>();
		
		@Parameter(required = false, names = {"--continue-on-error"},
					descriptionKey = NAME + ".update.continue")
		private boolean				continueOnError;
		
		@Parameter(required = false, names = {"-a", "--add"},
					variableArity = true,
					descriptionKey = NAME + ".update.add-roles")
		private List<String>		addRoles = new ArrayList<>();
		
		@Parameter(required = false, names = {"-r", "--remove"},
					variableArity = true,
					descriptionKey = NAME + ".update.remove-roles")
		private List<String>		removeRoles = new ArrayList<>();
		
		@Parameter(required = false, names = "--clear-roles",
					descriptionKey = NAME + ".update.clear-roles")
		private boolean				clearRoles;

		@Override
		public void
		validate() throws ParameterValidationException
		{
			if (ids == null || ids.isEmpty())
			{
				throw new ParameterValidationException("missing ID(s) of user(s) to update");
			}
			
			for (final String s : ids)
			{
				try
				{
					UUID.fromString(s);
				}
				catch (IllegalArgumentException e)
				{
					throw new ParameterValidationException("Malformed ID: " + s);
				}
			}
			
			/* check that we're either adding or removing  or both */
			
			if (this.addRoles.isEmpty()
				&& this.removeRoles.isEmpty()
				&& ! this.clearRoles)
			{
				throw new ParameterValidationException(
						"Must specify addition, removal, or reset of roles");
			}
		}

		@Override
		public void
		execute(MicredaInstance instance, GlobalOptions globalOptions, 
					PrintStream out, PrintStream err)
		throws Exception
		{
			UserClient client = new UserClient(instance);
			
			Set<UUID> ids = 
					this.ids
						.stream()
						.map(UUID :: fromString)
						.collect(Collectors.toSet());
			
			/* load and disable them all */
			
			for (UUID id : ids)
			{
				if (globalOptions.verbose())
				{
					out.print("Updating roles for user " + id + " ... ");
				}
				
				try
				{
					MUser user = client.load(id);
					
					if (user == null)
					{
						throw new UserNotFoundException(id);
					}
					
					Collection<String> newRoles = new ArrayList<>(user.roles());
					
					if (this.clearRoles)
					{
						newRoles.clear();
					}
					else if (! this.removeRoles.isEmpty())
					{
						newRoles.removeAll(this.removeRoles);
					}
					
					if (! this.addRoles.isEmpty())
					{
						newRoles.addAll(this.addRoles);
					}
				
					user = user.roles(newRoles);
					
					
					client.saveUser(user);
					
					if (globalOptions.verbose())
					{
						out.println("[success]");
					}
				}
				catch (UserNotFoundException e)
				{
					out.println("[FAILURE]");
					
					if (this.continueOnError)
					{
						err.println(e.getMessage());
					}
					else
					{
						throw e;
					}
					
				}
				catch (Exception e)
				{
					if (globalOptions.verbose())
					{
						out.println("[FAILURE]");
					}
					
					if (! this.continueOnError)
					{
						throw e;
					}
				}
			}
			
		}
		
	}
	
	private abstract class EnableDisableSub
	extends NamedCommand
	{
		
		@Parameter(required = false,
					descriptionKey = NAME + ".enable-disable.ids")
		private List<String>		ids = new ArrayList<>();
		
		@Parameter(required = false, names = {"--continue-on-error"},
					descriptionKey = NAME + ".enable-disable.continue")
		private boolean				continueOnError;

		protected abstract boolean isEnable();
		
		@Override
		public void
		validate() throws ParameterValidationException
		{
			if (ids == null || ids.isEmpty())
			{
				throw new ParameterValidationException(
						"missing ID(s) of user(s) to "
						+ (this.isEnable() ? "enable" : "disable"));
			}
			
			for (final String s : ids)
			{
				try
				{
					UUID.fromString(s);
				}
				catch (IllegalArgumentException e)
				{
					throw new ParameterValidationException("Malformed ID: " + s);
				}
			}
		}

		@Override
		public void
		execute(MicredaInstance instance, GlobalOptions globalOptions, 
					PrintStream out, PrintStream err)
		throws Exception
		{
			UserClient client = new UserClient(instance);
			
			Set<UUID> ids = 
					this.ids
						.stream()
						.map(UUID :: fromString)
						.collect(Collectors.toSet());
			
			/* load and enable/disable them all */
			
			for (UUID id : ids)
			{
				if (globalOptions.verbose())
				{
					out.print((this.isEnable() ? "Enabling" : "Disabling")
								+ " user " + id + " ... ");
				}
				
				try
				{
					MUser user = client.load(id);
					
					if (user == null)
					{
						throw new UserNotFoundException(id);
					}
					
					user = user.disabled(! this.isEnable());
					
					client.saveUser(user);
					
					if (globalOptions.verbose())
					{
						out.println("[success]");
					}
				}
				catch (UserNotFoundException e)
				{
					out.println("[FAILURE]");
					
					if (this.continueOnError)
					{
						err.println(e.getMessage());
					}
					else
					{
						throw e;
					}
					
				}
				catch (Exception e)
				{
					if (globalOptions.verbose())
					{
						out.println("[FAILURE]");
					}
					
					if (! this.continueOnError)
					{
						throw e;
					}
				}
			}
			
		}
		
	}
	
	@Parameters(commandNames = "disable",
				commandDescriptionKey = NAME + ".disable")
	private final class DisableSub
	extends EnableDisableSub
	{


		@Override
		protected boolean
		isEnable()
		{
			return false;
		}
		
		
	}
	
	@Parameters(commandNames = "enable",
				commandDescriptionKey = NAME + ".enable")
	private final class EnableSub
	extends EnableDisableSub
	{


		@Override
		protected boolean 
		isEnable()
		{
			return true;
		}
		
	}

}
