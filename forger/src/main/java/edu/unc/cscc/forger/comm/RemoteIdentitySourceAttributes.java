/*-
 * ========================LICENSE_START=================================
 * forger
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.forger.comm;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;

import edu.unc.cscc.micreda.model.IdentitySourceAttributes;

/**
 * <p>
 * Properties of a remote identity source.
 * </p>
 * 
 * @author Rob Tomsick (rtomsick@unc.edu)
 *
 */
@JsonTypeInfo(use = Id.NONE)
public final class RemoteIdentitySourceAttributes
implements IdentitySourceAttributes, Comparable<IdentitySourceAttributes>
{
	
	private final boolean		external;
	private final boolean		active;
	private final String		identifier;
	private final String		description;
	
	
	@JsonCreator
	public RemoteIdentitySourceAttributes(@JsonProperty("identifier") String identifier, 
									@JsonProperty("description") String description,
									@JsonProperty("external") boolean external,
									@JsonProperty("active") boolean active)
	{
		this.active = active;
		this.external = external;
		this.identifier = identifier;
		this.description = description;
	}
	
	@Override
	public boolean 
	isActive()
	{
		return this.active;
	}
	
	@Override
	public boolean
	isExternal()
	{
		return this.external;
	}

	@Override
	public String identifier()
	{
		return this.identifier;
	}
	
	@Override
	public String description()
	{
		return this.description;
	}

	@Override
	public int 
	compareTo(IdentitySourceAttributes o)
	{
		return this.identifier().compareTo(o.identifier());
	}
	
	
}
