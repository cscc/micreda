/*-
 * ========================LICENSE_START=================================
 * forger
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.forger.cli.commands;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;

import org.ini4j.InvalidFileFormatException;

import com.beust.jcommander.Parameter;

import edu.unc.cscc.forger.cli.GlobalOptions;
import edu.unc.cscc.forger.cli.ParameterValidationException;
import edu.unc.cscc.forger.configuration.SourceConfiguration;
import edu.unc.cscc.forger.model.MicredaInstance;

/* note that --help is handled for *each* command.  Why?  Because JCommander
 * doesn't give us an easy way to specify an option that applies to any 
 * sub-command.  This makes many things messy.
 */

public abstract class NamedCommand
{
	@Parameter(names = "--help",
				descriptionKey = "global.help",
				hidden = true)
	private boolean		help;
	
	public Collection<NamedCommand>
	subCommands()
	{
		return Collections.emptySet();
	}
	
	public void
	validate()
	throws ParameterValidationException
	{
		if (! this.subCommands().isEmpty())
		{
			throw new ParameterValidationException("This command requires a sub-command");
		}
	}
	
	public boolean
	expectsInstance()
	{
		return true;
	}
	
	public boolean
	printHelp()
	{
		return this.help;
	}
	
	public abstract void
	execute(MicredaInstance instance, GlobalOptions globalOptions, 
				PrintStream out, PrintStream err)
	throws Exception;
	
	public static Collection<MicredaInstance>
	loadInstances(PrintStream err)
		throws InvalidFileFormatException, IOException
	{
		File f = SourceConfiguration.obtainInstanceConfigFile();
		
		if (! f.exists())
		{
			err.println("Instance config file not found.  Have you configured any instances?");
			return null;
		}
		
		return new ArrayList<>(SourceConfiguration.loadInstances(f));
	}
	
	public static MicredaInstance
	loadInstance(PrintStream err, String instanceName)
	throws InvalidFileFormatException, IOException
	{
		Collection<MicredaInstance> instances = loadInstances(err);
		
		if (instances == null)
		{
			return null;
		}
		
		if (instances.size() < 1)
		{
			err.println("no instances configured");
			return null;
		}
		else if (instances.size() > 1 && instanceName == null)
		{
			err.println("more than one instance configured; specify "
					+ "which instance you want with -i/--instance");
			return null;
		}
		
		if (instances.size() == 1 && instanceName == null)
		{
			return instances.iterator().next();
		}
		
		Map<String, MicredaInstance> im = 
				instances.stream()
							.collect(Collectors.toMap(i -> i.name(), i -> i));
		
		MicredaInstance instance = im.get(instanceName);
		
		if (instance == null)
		{
			err.format("no instance configured with name '%s'%n", instanceName);
		}
		
		return instance;
	}

	
}
