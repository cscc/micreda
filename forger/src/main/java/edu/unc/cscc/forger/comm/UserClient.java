/*-
 * ========================LICENSE_START=================================
 * forger
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.forger.comm;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonCreator;

import edu.unc.cscc.forger.model.MicredaInstance;
import edu.unc.cscc.forger.model.MicredaInstance.Endpoint;
import edu.unc.cscc.micreda.model.MUser;
import edu.unc.cscc.micreda.model.UserIdentity;
import edu.unc.cscc.micreda.model.configuration.JWTEndpointInfo;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import okhttp3.HttpUrl;
import okhttp3.Request;
import okhttp3.Request.Builder;
import okhttp3.RequestBody;
import okhttp3.Response;

public final class UserClient
extends MicredaClient
{
	
	public UserClient(MicredaInstance instance)
	{
		super(instance);
	}
	
	public int
	countUsers()
	throws IOException, IncompatibleInstanceException, URISyntaxException
	{
		this.checkCompatibility();
		
		Request request = 
				(new Builder())
						.url(this.endpointURL(Endpoint.USERS)
									.newBuilder()
									.addPathSegment("count")
									.build())
						.get()
						.build();
		
		Response response = this.client().newCall(request).execute();
		
		if (! response.isSuccessful())
		{
			throw new IOException(
					"User count failed. Response from server was: " 
							+ response.code() + " : " + response.message());
		}
		
		return Integer.parseInt(response.body().string());
	}
	
	/**
	 * Load the user with the given ID.
	 * 
	 * @param id user ID, not <code>null</code>
	 * @return user, or <code>null</code> if a user with the given ID was
	 * not known to the instance
	 * 
	 * @throws IOException
	 * @throws IncompatibleInstanceException
	 * @throws URISyntaxException
	 */
	public MUser
	load(UUID id)
	throws IOException, IncompatibleInstanceException, URISyntaxException
	{
		this.checkCompatibility();
		
		Request request = 
				(new Builder())
						.url(this.endpointURL(Endpoint.USERS)
									.newBuilder()
									.addPathSegment(id.toString())
									.build())
						.get()
						.build();
		
		Response resp = this.client().newCall(request).execute();
		
		if (resp.code() == 404)
		{
			return null;
		}
		
		return read(resp.body().charStream(), MUser.class);
	}
	
	public MUser
	loadByShortID(String id)
	throws IOException, IncompatibleInstanceException, URISyntaxException
	{
		this.checkCompatibility();
		
		Request request = 
				(new Builder())
						.url(this.endpointURL(Endpoint.USERS)
									.newBuilder()
									.addPathSegment("by-short-id")
									.addPathSegment(id)
									.build())
						.get()
						.build();
		
		Response resp = this.client().newCall(request).execute();
		
		if (resp.code() == 404)
		{
			return null;
		}
		
		return read(resp.body().charStream(), MUser.class);
	}

	public MUser
	createUser(Set<String> roles)
	throws IOException, IncompatibleInstanceException, URISyntaxException
	{
		this.checkCompatibility();
		
		MUser user = new MUser(null, false, roles);
				
		Request request = 
				(new Builder())
						.url(this.endpointURL(Endpoint.USERS))
						.post(RequestBody.create(JSON_TYPE, toJSON(user)))
						.build();
		
		Response response = this.client().newCall(request).execute();
		
		if (! response.isSuccessful())
		{
			throw new IOException(
					"Creation failed? Response from server was: " 
							+ response.code() + " : " + response.message());
		}
		
		return read(response.body().charStream(), MUser.class);
	}
	
	public MUser
	saveUser(MUser user) 
	throws URISyntaxException, IOException, IncompatibleInstanceException,
		UserNotFoundException
	{
		this.checkCompatibility();
						
		Request request = 
				(new Builder())
						.url(this.endpointURL(Endpoint.USERS)
									.newBuilder()
									.addPathSegment(user.id().toString())
									.build())
						.post(RequestBody.create(JSON_TYPE, toJSON(user)))
						.build();
		
		Response response = this.client().newCall(request).execute();
		
		if (response.code() == 404)
		{
			throw new UserNotFoundException(user.id());
		}
		
		if (! response.isSuccessful())
		{
			throw new IOException(
					"Update failed? Response from server was: " 
							+ response.code() + " : " + response.message());
		}
		
		return read(response.body().charStream(), MUser.class);
	}
	
	public Map<MUser, Collection<UserIdentity>>
	list(Set<String> roles, boolean includeIdentities, boolean matchAllRoles)
	throws IOException, IncompatibleInstanceException, URISyntaxException
	{
		this.checkCompatibility();
		
		HttpUrl.Builder builder =
				this.endpointURL(Endpoint.USERS)
					.newBuilder("./search");
		
		if (! roles.isEmpty())
		{
			roles.forEach(role -> builder.addQueryParameter("role", role));
		}
		
		builder.addQueryParameter("match-all", Boolean.toString(matchAllRoles));
				
		
		Request request = 
				(new Builder())
					.url(builder.build())
					.get()
					.build();
		
		Response response = 
				this.client().newCall(request).execute();
		
		if (! response.isSuccessful())
		{
			throw new IOException("Failed to retrieve user list.  "
									+ "HTTP status " + response.code()
									+ ", response: " + response.body().string());
		}
		
		Map<MUser, Collection<UserIdentity>> results = new HashMap<>();
		
		
		mapper()
			.readerFor(MUser.class)
			.<MUser>readValues(response.body().charStream())
			.forEachRemaining(u -> results.put(u, Collections.emptySet()));
		
		if (includeIdentities)
		{
			/* fetch identities */
			
			final String idSegment = 
					results.keySet()
							.stream()
							.map(u -> u.id().toString())
							.collect(Collectors.joining("+"));
			
			request = (new Builder())
						.url(this.endpointURL(Endpoint.USERS)
									.newBuilder()
									.addPathSegment(idSegment)
									.addPathSegment("identities")
									+ "/")
						.get()
						.build();
			
			response = this.client().newCall(request).execute();
			
			Map<UUID, Collection<UserIdentity>> byID = 
					read(response.body().charStream(), IdentityResults.class).val;
			
			results.replaceAll((user, e) -> byID.get(user.id()));
		}
		
		return results;
	}
	
	public Collection<UserIdentity>
	loadIdentities(UUID id)
	throws IOException, IncompatibleInstanceException, URISyntaxException,
			UserNotFoundException
	{
		if (id == null)
		{
			throw new IllegalArgumentException("user ID must not be null");
		}
		
		this.checkCompatibility();
		
		HttpUrl.Builder builder =
				this.endpointURL(Endpoint.USERS)
					.newBuilder()
					.addPathSegment(id.toString())
					.addPathSegment("identities");
		
				
		
		Request request = 
				(new Builder())
					/* oh screw you, OkHttp.  There's no way to specify trailing
					 * slashes via your fluent API
					 */
					.url(builder.build() + "/")
					.get()
					.build();
		
		Response response = 
				this.client().newCall(request).execute();
		
		if (response.isSuccessful())
		{
			Collection<UserIdentity> identities = new ArrayList<>();
			
			mapper()
				.readerFor(UserIdentity.class)
				.<UserIdentity>readValues(response.body().charStream())
				.forEachRemaining(identities :: add);
			
			return identities;
		}
		else if (response.code() == 404)
		{
			throw new UserNotFoundException(id);
		}
		
		throw new IOException("Bad response from server: " 
								+ response.code() + " : " 
								+ response.message() + " : "
								+ response.body().string());
	}
	
	public void
	deleteAllIdentities(MUser user)
	throws URISyntaxException, UserNotFoundException, IOException, IncompatibleInstanceException
	{
		this.checkCompatibility();
		Request request = (new Request.Builder())
							.url(this.endpointURL(Endpoint.USERS)
										.newBuilder()
										.addPathSegment(user.id().toString())
										.addPathSegment("identities")
										.build() + "/")
							.delete()
							.build();
		
		Response response = 
				this.client().newCall(request).execute();
		
		if (response.isSuccessful())
		{
			return;
		}
		else if (response.code() == 404)
		{
			throw new UserNotFoundException(user.id());
		}
		
		throw new IOException("Bad response from server: " 
								+ response.code() + " : " 
								+ response.message() + " : "
								+ response.body().string());
	}
	
	/**
	 * Delete the identities associated with a given user, restricting by source
	 * and optionally username.
	 * 
	 * @param user user for whom to delete associated identities
	 * @param username username, may be <code>null</code>
	 * @param source source for identities to delete, not <code>null</code>
	 * 
	 * @throws URISyntaxException
	 * @throws UserNotFoundException
	 * @throws IOException
	 * @throws IncompatibleInstanceException 
	 */
	public void
	deleteIdentities(MUser user, String username, String source)
	throws URISyntaxException, UserNotFoundException, IOException, IncompatibleInstanceException
	{
		this.checkCompatibility();
		
		HttpUrl.Builder builder = 
				this.endpointURL(Endpoint.USERS,
						"/", user.id().toString(), "/identities/")
						.newBuilder()
						.addQueryParameter("source", source);
		
		if (username != null)
		{
			builder.addQueryParameter("username", username);
		}
		
		Request request = (new Request.Builder())
						.url(builder.build())
						.delete()
						.build();
		
		Response response = 
			this.client().newCall(request).execute();
		
		if (response.isSuccessful())
		{
			return;
		}
		else if (response.code() == 404)
		{
			throw new IOException("User or identity source does not exist");
		}
		
		throw new IOException("Bad response from server: " 
							+ response.code() + " : " 
							+ response.message() + " : "
							+ response.body().string());
	}
	
	/**
	 * Load the user identity with the given username and identity source 
	 * identifier.
	 * 
	 * @param username username 
	 * @param sourceIdentifier identity source identifier
	 * 
	 * @return identity matching the given parameters or <code>null</code> if
	 * no such identity (or no such identity source) exists
	 * @throws URISyntaxException 
	 * @throws IOException 
	 * @throws IncompatibleInstanceException 
	 */
	public UserIdentity
	loadIdentity(String username, String sourceIdentifier)
	throws URISyntaxException, IOException, IncompatibleInstanceException
	{
		this.checkCompatibility();
		
		Request req = (new Builder())
						.url(this.endpointURL("/identities")
								.newBuilder()
								.addQueryParameter("source", sourceIdentifier)
								.addQueryParameter("username", username)
								.build())
						.get()
						.build();
		
		Response resp = this.client().newCall(req).execute();
		
		if (resp.isSuccessful())
		{
			return read(resp.body().charStream(), UserIdentity.class);
		}
		
		if (resp.code() == 404)
		{
			return null;
		}
		
		throw new IOException("Bad response from server: " 
								+ resp.code() + " : " 
								+ resp.message() + " : "
								+ resp.body().string());
	}
	
	

	/**
	 * Save the given internal identity and password.
	 * 
	 * @param id identity, not <code>null</code>
	 * @param password password, not <code>null</code>
	 * @return copy of the identity as stored in instance, not <code>null</code>
	 * 
	 * @throws URISyntaxException 
	 * @throws IOException 
	 * @throws IncompatibleInstanceException 
	 */
	public UserIdentity 
	saveInternal(UserIdentity id, String password)
	throws URISyntaxException, IOException, IncompatibleInstanceException
	{
		this.checkCompatibility();
		
		final Map<String, Object> payload = new HashMap<>();
		
		payload.put("identity", id);
		payload.put("credentials", password);
		
		Request req = (new Request.Builder())
						.url(this.endpointURL(Endpoint.USERS)
								.newBuilder()
								.addPathSegment(id.user().id().toString())
								.addPathSegment("identities")
								.addPathSegment("internal")
								+ "/")
						.post(RequestBody.create(JSON_TYPE, toJSON(payload)))
						.build();
		
		Response resp = this.client().newCall(req).execute();
		
		if (resp.isSuccessful())
		{
			return read(resp.body().charStream(), UserIdentity.class);
		}
		
		if (resp.code() == 404)
		{
			/* FIXME - do something a little more granular here; requires 
			 * server help to disambiguate
			 */
			throw new RuntimeException(
					"Either user not found or internal identity source with identifier '" 
						+ id.identitySourceIdentifier() + "' not found");
		}
		
		throw new IOException("Save failed. Response from server: " 
								+ resp.code() + " : " 
								+ resp.message() + " : "
								+ resp.body().string());
	}

	/**
	 * Save the given external identity.
	 * 
	 * @param id identity, not <code>null</code>
	 * @return copy of the identity as stored in instance, not <code>null</code>
	 * @throws URISyntaxException 
	 * @throws IOException 
	 * @throws UserNotFoundException 
	 * @throws IncompatibleInstanceException 
	 */
	public UserIdentity 
	saveExternal(UserIdentity id)
	throws URISyntaxException, IOException, UserNotFoundException, IncompatibleInstanceException
	{
		
		this.checkCompatibility();
		
		Request req = (new Request.Builder())
						.url(this.endpointURL(Endpoint.USERS)
								.newBuilder()
								.addPathSegment(id.user().id().toString())
								.addPathSegment("identities")
								.addPathSegment("external")
								+ "/")
						.post(RequestBody.create(JSON_TYPE, toJSON(id)))
						.build();
		
		Response resp = this.client().newCall(req).execute();
		
		if (resp.isSuccessful())
		{
			return read(resp.body().charStream(), UserIdentity.class);
		}
		
		if (resp.code() == 404)
		{
			throw new UserNotFoundException(id.user().id());
		}
		
		
		throw new IOException("Save failed. Response from server: " 
				+ resp.code() + " : " 
				+ resp.message() + " : "
				+ resp.body().string());
	}
	
	/**
	 * Attempt to authenticate a user with a given username/credentials against
	 * a specified identity source.  The given {@link JWTEndpointInfo JWT endpoint}
	 * information will be used to process the issued token's claims if 
	 * authentication succeeds.
	 * 
	 * @param endpointInfo endpoint information
	 * @param source identity source identifier
	 * @param username username
	 * @param credentials user credentials
	 * 
	 * @return user identity, or <code>null</code> if authentication failed
	 * 
	 * @throws IOException
	 * @throws URISyntaxException
	 * @throws IncompatibleInstanceException
	 */
	public UserIdentity
	authenticate(JWTEndpointInfo endpointInfo,
					String source, String username, String credentials)
	throws IOException, URISyntaxException, IncompatibleInstanceException
	{
		this.checkCompatibility();
		
		Request req = (new Request.Builder())
				.url(this.endpointURL(Endpoint.TOKENS)
						.newBuilder()
						.addPathSegment("issue")
						.addQueryParameter("username", username)
						.addQueryParameter("credentials", credentials)
						.addQueryParameter("identity-source", source)
						.build())
				.get()
				.addHeader("Accept", 
						"application/jwt; q=0.9,"
						+ "application/jose; q=0.5, "
						+ "application/jws; q=0.5,"
						+ "*/*; q=0")
				.build();

		Response resp = this.client().newCall(req).execute();
		
		if (! resp.isSuccessful())
		{
			if (resp.code() == 403)
			{
				return null;
			}
			
			throw new IOException("Authentication failed. Response from server: " 
									+ resp.code() + " : " 
									+ resp.message() + " : "
									+ resp.body().string());
		}
		
		final String body = resp.body().string();
		final String contentType = resp.header("Content-Type");
		
		Claims claims;
		
		if (contentType.startsWith("application/jwt"))
		{
			claims = Jwts.parser()
							.parseClaimsJwt(body)
							.getBody();
		}
		else if (contentType.startsWith("application/jose")
				|| contentType.startsWith("application/jws"))
		{
			/* Ignore signatuer component here.  We don't need it since
			 * we're talking to the endpoint directly, and if we're MTMd
			 * then we were screwed a long time ago.
			 */
			String trimmed = body.substring(0, body.lastIndexOf('.') + 1);
			
			claims = Jwts.parser()
						.parseClaimsJwt(trimmed)
						.getBody();
		}
		else
		{
			throw new IOException("Response was of unknown content type: " 
									+ contentType);
		}
		
		
		
		
		final String idStr = 
				claims.get(endpointInfo.claimPrefix() + endpointInfo.idClaimName(), 
							String.class);
		
		UUID id;
		
		try
		{
			id = UUID.fromString(idStr);
		}
		catch (IllegalArgumentException e)
		{
			throw new IOException("Failed to parse claim; bad/missing ID");
		}
		
		List<?> rawRoles = 
				claims.get(endpointInfo.claimPrefix() + endpointInfo.rolesClaimName(), 
							List.class);
		
		Set<String> roles = Collections.emptySet();
		
		if (! rawRoles.isEmpty())
		{
			/* oh yeah.  This is why I hate erasure... */
			if (rawRoles.stream().anyMatch(r -> ! (r instanceof String)))
			{
				throw new IOException(
						"De-serialized non-string role.  Programmer screwed up.");
			}
			roles = rawRoles.stream().map(r -> (String) r).collect(Collectors.toSet());
		}
		
		
		/* we know they're not disabled since auth won't work if they are */
		MUser user = new MUser(id, false, roles);
		
		UserIdentity identity = new UserIdentity(user, username, source);
		
		return identity;
	}
	
	public static final class UserNotFoundException
	extends Exception
	{
		private static final long serialVersionUID = 1L;

		public UserNotFoundException(String msg)
		{
			super(msg);
		}
		
		public UserNotFoundException(UUID id)
		{
			this("user with ID " + id.toString() + " not found");
		}
	}
	
	private static final class IdentityResults
	{
		
		private Map<UUID, Collection<UserIdentity>> val;
		
		@JsonCreator
		private static final IdentityResults
		creator(Map<UUID, Collection<UserIdentity>> val)
		{
			IdentityResults r = new IdentityResults();
			r.val = val;
			return r;
		}
	}
	
}
