/*-
 * ========================LICENSE_START=================================
 * forger
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.forger.cli.commands;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

import de.vandermeer.asciitable.AsciiTable;
import edu.unc.cscc.forger.cli.GlobalOptions;
import edu.unc.cscc.forger.comm.UserClient;
import edu.unc.cscc.forger.model.MicredaInstance;
import edu.unc.cscc.micreda.model.MUser;
import edu.unc.cscc.micreda.model.UserIdentity;

@Parameters(commandNames = "users",
			commandDescriptionKey = "users")
public class UsersCommand
extends NamedCommand
{

	@Parameter(variableArity = true, 
				names = {"-r", "--roles"},
				descriptionKey = "users.roles")
	private List<String>		roles;
	
	@Parameter(names = {"-m", "--matchAll"},
				descriptionKey = "users.match-all")
	private boolean				matchAllRoles = false;
	
	@Parameter(names = {"-e", "--identities"},
				descriptionKey = "users.identities")
	private boolean				includeIdentities = false;
	
	@Parameter(names = {"-t", "--table"},
				descriptionKey = "users.table")
	private boolean				tableDisplay = false;


	@Override
	public void
	execute(MicredaInstance instance, GlobalOptions globalOptions, 
				PrintStream out, PrintStream err)
	throws Exception
	{
		UserClient client = new UserClient(instance);
		
		Set<String> roles =
				(this.roles == null
					? Collections.emptySet()
					: new HashSet<>(this.roles));
		
		
		Map<MUser, Collection<UserIdentity>> results = 
				client.list(roles, this.includeIdentities, this.matchAllRoles);
		
		if (results.isEmpty())
		{
			return;
		}
		
		if (! this.tableDisplay)
		{
			this.renderTable(out, results);
		}
		else
		{
			this.renderTable(out, results);
		}
	}
	
	@SuppressWarnings("unused")
	private final void
	renderPlain(PrintStream out, Map<MUser, Collection<UserIdentity>> results)
	{
		/* TODO - implement plain rendering */
	}
	
	private final void
	renderTable(PrintStream out, Map<MUser, Collection<UserIdentity>> results)
	{
		AsciiTable at = new AsciiTable();
		
		at.addRule();
		
		if (this.includeIdentities)
		{
			at.addRow("id", "roles", "total identities");
		}
		else
		{
			at.addRow("id", "roles");
		}
		
		results.forEach((user, identities) -> {
			
			at.addRule();
			
			if (this.includeIdentities)
			{
				/* first, do our row for the user themselves */
				at.addRow(user.id()
							+ (user.disabled() ? " (disabled)" : ""),
						formatRoles(user.roles()), identities.size());
				
				/* bin identities by source */
				
				Map<String, List<UserIdentity>> idbs = new HashMap<>();
				
				Util.partitionIdentitiesBySource(identities)
					.forEach((k, v) -> idbs.put(k, new ArrayList<>(v)));
				
				/* now sort them by username descending */
				
				idbs.replaceAll((ident, l) -> {
					
					ArrayList<UserIdentity> sorted = new ArrayList<>(l);
					
					Collections.sort(l, (a, b) -> a.username().compareTo(b.username()));
					
					return sorted;
				});
				
				/* now render by source */
				
				
				
				if (! idbs.isEmpty())
				{
					at.addRule();
					at.addRow("", "identity source", "username");
					
					idbs.keySet()
						.stream()
						.sorted()
						.forEach(sourceName -> {
							idbs.get(sourceName)
								.stream()
								.forEach(id -> {
									at.addRule();
									at.addRow("",
												id.identitySourceIdentifier(), 
												id.username());
								});
							
						});
				}
				
				
				
			}
			else
			{
				at.addRow(user.id() + (user.disabled() ? " (disabled)" : ""), 
							formatRoles(user.roles()));
			}
			
			
		});
		
		at.addRule();
		
		out.println(at.render(78));
	}
	
	private static final String
	formatRoles(Collection<String> roles)
	{
		return roles.stream().map(r -> "'" + r + "'")
				.collect(Collectors.joining(","));
	}

}
