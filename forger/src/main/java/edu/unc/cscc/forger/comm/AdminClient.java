/*-
 * ========================LICENSE_START=================================
 * forger
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.forger.comm;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;

import edu.unc.cscc.forger.model.MicredaInstance;
import edu.unc.cscc.forger.model.MicredaInstance.Endpoint;
import edu.unc.cscc.micreda.model.IdentitySourceAttributes;
import edu.unc.cscc.micreda.model.configuration.JWTEndpointInfo;
import edu.unc.cscc.micreda.model.configuration.MicredaEndpointInfo;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public final class AdminClient
extends MicredaClient
{
	
	public AdminClient(MicredaInstance instance)
	{
		super(instance);
	}

	public MicredaEndpointInfo
	retrieveInfo()
	throws URISyntaxException, IOException
	{		
		Request request = (new Request.Builder())
				.url(this.endpointURL(INFO_ENDPOINT))
				.get()
				.build();

		return read(this.client()
					.newCall(request)
					.execute()
					.body().charStream(),
					MicredaEndpointInfo.class);
	}
	
	/* TODO - log retrieval */
	
	/**
	 * Get information about the connected instance's JWT endpoint.
	 * 
	 * @return endpoint information
	 * @throws URISyntaxException 
	 * @throws IOException 
	 * @throws JsonProcessingException 
	 * @throws IncompatibleInstanceException 
	 */
	public JWTEndpointInfo
	retrieveJWTInfo()
	throws URISyntaxException, JsonProcessingException, IOException, IncompatibleInstanceException
	{
		this.checkCompatibility();
		
		Request request = (new Request.Builder())
				.url(this.endpointURL(Endpoint.TOKENS)
							.newBuilder()
							.addPathSegment("info")
							.build())
				.get()
				.build();

		return read(this.client()
					.newCall(request)
					.execute()
					.body().charStream(),
					JWTEndpointInfo.class);
	}

	
	/**
	 * Find an identity source known to the connected instance by identifier.
	 * 
	 * @param identifier identity source identifier
	 * @param includeInternal include internal sources in the search
	 * @param includeExternal include external sources in the search
	 * 
	 * @return source, if known, <code>null</code> otherwise
	 * 
	 * @throws URISyntaxException 
	 * @throws IncompatibleInstanceException 
	 * @throws IOException 
	 */
	public final IdentitySourceAttributes
	findIdentitySource(final String identifier, 
						final boolean includeInternal, 
						final boolean includeExternal)
	throws IOException, IncompatibleInstanceException, URISyntaxException
	{
		this.checkCompatibility();
		
		if (! (includeInternal || includeExternal))
		{
			return null;
		}
		
		String segment = "all";
		
		if (! includeExternal)
		{
			segment = "internal";
		}
		else if (! includeInternal)
		{
			segment = "external";
		}
		
		Request request = 
				(new Request.Builder())
					.url(this.endpointURL(Endpoint.IDENTITY_SOURCES)
							.newBuilder()
							.addPathSegment(segment)
							.addPathSegment("by-identifier")
							.addQueryParameter("identifier", identifier)
							.build())
					.get()
					.build();
		
		Response resp = this.client().newCall(request).execute();
		
		if (resp.isSuccessful())
		{
			return read(resp.body().charStream(), RemoteIdentitySourceAttributes.class);
		}
		
		if (resp.code() == 404)
		{
			return null;
		}
		
		throw new IOException("Find identity source failed: " + resp.code() + " : " + resp.message());
		
	}
	
	public final void
	saveInternalSource(String identifier, String description)
	throws IOException, IncompatibleInstanceException, URISyntaxException,
		SourceConflictException
	{
		this.checkCompatibility();
		
		Request request = 
				(new Request.Builder())
					.url(this.endpointURL(Endpoint.IDENTITY_SOURCES)
							.newBuilder()
							.addPathSegment("internal")
							.build()
							+ "/")
					.post(RequestBody.create(
							JSON_TYPE, 
							toJSON(new RemoteIdentitySourceAttributes(identifier, description, false, true))))
					.build();
		
		Response resp = this.client().newCall(request).execute();
		
		if (resp.isSuccessful())
		{
			return;
		}
		
		
		if (resp.code() == 409)
		{
			throw new SourceConflictException(
					"external source with identifier '" + identifier + "' is in use in instance");
		}
		
		throw new IOException("Create identity source failed: " + resp.code() + " : " + resp.message());

	}
	
	public final Collection<? extends IdentitySourceAttributes>
	searchSources(String term, boolean includeInternal, boolean includeExternal, 
					boolean includeInactive)
	throws IOException, IncompatibleInstanceException, URISyntaxException
	{
		this.checkCompatibility();
		
		if (! (includeInternal || includeExternal))
		{
			return null;
		}
		
		Request request = 
				(new Request.Builder())
					.url(this.endpointURL(Endpoint.IDENTITY_SOURCES, "/all/")
							.newBuilder()
							.addQueryParameter("query", term)
							.addQueryParameter("include-inactive-external", 
									String.valueOf(includeInactive))
							.addQueryParameter("include-internal",
									String.valueOf(includeInternal))
							.addQueryParameter("include-external",
									String.valueOf(includeExternal))
							.build())
					.get()
					.build();
		
		Response resp = this.client().newCall(request).execute();
		
		if (resp.isSuccessful())
		{
			Iterator<RemoteIdentitySourceAttributes> si = 
					mapper()
						.readerFor(RemoteIdentitySourceAttributes.class)
						.<RemoteIdentitySourceAttributes>readValues(resp.body().charStream());
			
			List<RemoteIdentitySourceAttributes> s = new ArrayList<>();
			
			si.forEachRemaining(s :: add);
			
			return s;
		}
		
		throw new IOException("Identity source search failed: " 
								+ resp.code() + " : " + resp.message());

	}
	
	/**
	 * Indicates a conflict caused by trying to create an identity source that
	 * already exists (i.e. trying to create an internal source with the same
	 * identifier as an external source).
	 * 
	 * @author Rob Tomsick (rtomsick@unc.edu)
	 *
	 */
	public final class SourceConflictException
	extends Exception
	{
		private static final long serialVersionUID = 1L;

		public SourceConflictException(String message)
		{
			super(message);
		}
	}
}
