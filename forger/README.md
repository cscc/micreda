forger
===

`forger` is a basic implementation of a management utility for instances
of micreda.  You may configure it to talk to any number of instances of micreda
and can use it to perform all administrative tasks that micreda supports, as
well as to test authentication of a given set of credentials.

## Building
A directly-executable bundle of forger may be produced via the `assembly:assembly` target.  The resulting binary `forger.run` may be made executable and should
run on any *nix system with a current version of Java installed.  (If this 
bundling process does not work, you may need to tweak the stub shell script
`stub.sh`.  Patches to this script are welcome.)

## Usage

For usage information, run `forger.run` with no arguments to get a list of
available commands.

See `examples.md` for examples of how one might use forger to accomplish
different administrative tasks.

## License

BSD 3-clause.  See LICENSE for a copy of the full license.

The dependencies used by forger are under a variety of licenses.  See
`META-INF/THIRD-PARTY.txt` in the generated executable JAR for details.

## Limitations

forger's internal architecture is relatively... convoluted.  This is due in
large part to the choice of supporting dependencies and their approaches
 (specifically the command line argument handling).  Further, forger is 
intended to be used to test or bootstrap an installation of micreda.  It is
expected that management will be done by organization-specific tools using
micreda's REST API.  As such, while one may extend or modify forger, enabling
such extension was not necessarily a consideration during its implementation.