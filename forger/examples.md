Configuration
---

Store an instance configuration

    forger config add <name> --url https://example.com/ --username <user> --password <password>
    
Delete a specific instance

    forger config delete <name>

List the configured instances

    forger config instances

Informational
---

Get information about the configuration of an instance

    forger [-i instance] info

Get the log of an instance

    forger [-i instance] log --last <num items> # TODO
    

User Management
---

Create a user with one or more roles assigned

    forger [-i instance] user create [role 1] [role 2]
    
Update the user with the given ID(s), assigning the specified roles.
Additions will trump removals.

    forger [-i instance] user update <id id1 id2 ...> --add [role 1] [role 2] --remove [role 3] [role 4]

Remove all roles from the user(s) with the given IDs

    forger [-i instance] user update <id id1 id2 ...> --clear-roles
    
Disable the user with the given ID(s)

    forger [-i instance] user disable <id id1 id2 ...>

Enable the user with the given ID(s)

    forger [-i instance] user enable <id id1 id2 ...>

Query information on a specified user

    forger [-i instance] user query <id>

List the users (and optionally, associated identities)

	# -i is TODO
    forger [-i instance] users [-e, --identities] [-r, --roles "role_1" "role_2"]

Source Management
---

Create a new internal identity source 

    forger [-i instance] source create <name> [description]

Disable a specified internal identity source

    forger [-i instance] source disable <name> # TODO
    
Update the description of a specific internal identity source
   
    forger [-i instance] source update <name> <description>

Query a given identity source

    forger [-i instnace] source query <name>

Search for an identity source (internal or external) matching the given term.
Optionally include inactive external sources.

    forger [-i instance] sources [--search term] [--internal] [--external] [--include-inactive]

Identity Management
---

Associate a given username and external identity source with the specified user

    forger [-i instance] identity assoc <id> --username <username> --source <name>

Create an internal identity (username/password) and associate it with the 
specified user

    forger [-i instance] identity assoc <id> --username <username> --source <name> --password <password>

Dis-associate the given username/identity source information from the specified user

    forger [-i instance] identity disassoc <id> --username <username> ---source <name>
    
Dis-associate all identities from the specified user

    forger [-i instance] identity disassoc <id> --all

Dis-associate all identities from the given users

    forger [-i instance] identity disassoc <id1>...<idn> --batch --all

Find the ID of the user associated with the given identity

    forger [-i instance] identity query --username <username> --source <name>

List the known identities, optionally restricting the results to those 
from a specified identity source and/or matching a search term

    forger [-i instance] identities [--source name] [--search term] # TODO


Identity Authentication
---

Test authentication with the given username / credentials for a given identity source

(TODO: syntax OK?  Or use positional instead of argument names?)

    forger [-i instance] auth --source <source> --username <username> --password <password>
