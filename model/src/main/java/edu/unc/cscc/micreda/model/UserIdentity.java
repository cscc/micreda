/*-
 * ========================LICENSE_START=================================
 * model
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.micreda.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonTypeName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * A user identity is a unique set of information identifying a user to 
 * micreda.  No uniqueness requirement is placed on the username, however, 
 * as usernames may be non-unique when considering multiple sources of 
 * identity information.  A username does not necessarily have to correspond
 * to a human-friendly name for the associated user; the format and 
 * significance of the {@link #username() "username"} is left up to the 
 * identity source which handles authentication of the identity.
 * 
 * @author Rob Tomsick (rtomsick@unc.edu)
 *
 */
@JsonTypeName("user-identity")
@ApiModel(value = "user-identity",
			description = "A user identity is a unique set of information "
						+ "identifying a user to micreda.  No uniqueness "
						+ "requirement is placed on the username, however, "
						+ "as usernames may be non-unique when considering "
						+ "multiple sources of identity information. For "
						+ "more information, see documentation of the 'user' "
						+ "property.")
public class UserIdentity
{
	private final MUser			user;
	private final String		username;
	private final String		identitySourceID;
	
	@JsonCreator
	public UserIdentity(@JsonProperty("user") @NotNull final MUser user,
						@JsonProperty("username")
						@Size(min = 1) @NotNull
						final String username, 
						@JsonProperty("identity-source")
						@Size(min = 1) @NotNull
						final String identitySourceIdentifier)
	{
		if (user == null)
		{
			throw new IllegalArgumentException("user must not be null");
		}
		
		if (StringUtils.isEmpty(username))
		{
			throw new IllegalArgumentException(
					"username must not be empty or null");
		}
		
		if (StringUtils.isEmpty(identitySourceIdentifier))
		{
			throw new IllegalArgumentException(
					"identity source identifier must not be empty or null");
		}
		
		this.user = user;
		this.username = username;
		this.identitySourceID = identitySourceIdentifier;
	}
	
	/**
	 * Get the user with which the identity is associated.
	 * 
	 * @return user, not <code>null</code>
	 */
	@JsonGetter("user")
	@NotNull
	@ApiModelProperty(value = "user with which the identity is associated",
						required = true, allowEmptyValue = false)
	public MUser 
	user()
	{
		return this.user;
	}
	
	/**
	 * Identifier of the identity source against which this user may be 
	 * authenticated.  This is a string identifier rather than an object
	 * reference so that identities which correspond to identity sources that
	 * are no longer available to an instance of the application may still
	 * be handled/manipulated.
	 * 
	 * @return identity source ID, not <code>null</code>
	 */
	@JsonGetter("identity-source")
	@NotNull
	@Size(min = 1)
	@ApiModelProperty(value = "Identifier of the identity source against "
							+ "which the user may be authenticated. Is NOT "
							+ "required to correspond to a source currently "
							+ "active/configured in a given instance of micreda.",
						required = true, allowEmptyValue = false,
						example = "ldap:example-ldap")
	public String
	identitySourceIdentifier()
	{
		return this.identitySourceID;
	}
	
	/**
	 * Username of the identity.  Unique across all identities within a given 
	 * identity source, but not globally unique.
	 * 
	 * @return username, not <code>null</code>
	 */
	@JsonGetter("username")
	@NotNull
	@Size(min = 1)
	@ApiModelProperty(value = "Username of the identity.  Unique across all "
							+ "identities within a given identity source, but "
							+ "NOT globally unique. (May be used in combination "
							+ "with 'identity-source' as a unique key for "
							+ "a user identity.)  The format and significance "
							+ "of the username beyond these constraints is "
							+ "left up to the identity source and/or end-users "
							+ "of micreda.",
						required = true, allowEmptyValue = false,
						example = "jdoe2")
	public String
	username()
	{
		return this.username;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((identitySourceID == null) ? 0
				: identitySourceID.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		result = prime * result
				+ ((username == null) ? 0 : username.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserIdentity other = (UserIdentity) obj;
		if (identitySourceID == null)
		{
			if (other.identitySourceID != null)
				return false;
		}
		else if (!identitySourceID.equals(other.identitySourceID))
			return false;
		if (user == null)
		{
			if (other.user != null)
				return false;
		}
		else if (!user.equals(other.user))
			return false;
		if (username == null)
		{
			if (other.username != null)
				return false;
		}
		else if (!username.equals(other.username))
			return false;
		return true;
	}
	
	/* sham setters to work around swagger-core's read-only bug */
	@JsonSetter("username")
	private void
	hack_username(String username)
	{
		throw new Error("workaround setter called");
	}
	
	@JsonSetter("user")
	private void
	hack_user(MUser user)
	{
		throw new Error("workaround setter called");
	}
	
	@JsonSetter("identity-source")
	private void
	hack_identitySource(String identitySource)
	{
		throw new Error("workaround setter called");
	}
}
