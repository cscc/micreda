/*-
 * ========================LICENSE_START=================================
 * model
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.micreda.model;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonTypeName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Model of a "user" within micreda.  Essentially a unique, stable identifier
 * with some additional meta-information (disabled/enabled, roles, etc.)
 * 
 * @author Rob Tomsick (rtomsick@unc.edu)
 *
 */
@JsonTypeName("user")
@ApiModel(value = "user", 
			description = "A user within micreda.  A stable, unique "
						+ "identifier with additional meta-information "
						+ "(enabled/disabled, roles, etc.) ")
public final class MUser
{
	private final UUID				id;
	private final boolean			disabled;
	private final Set<String>		roles;
	
	@JsonCreator
	public MUser(@JsonProperty(value = "id", required = false) UUID id, 
					@JsonProperty(value = "disabled",
									required = false,
									defaultValue = "false") 
					boolean disabled, 
					@JsonProperty(value = "roles", required = false, 
									defaultValue = "[]")
					@NotNull
					Set<String> roles)
	{
		this.id = id;
		this.disabled = disabled;
		this.roles = new HashSet<>(roles);
	}

	/**
	 * Unique identifier for the user, or <code>null</code> if the user is
	 * not currently known (i.e. has no internal representation) within 
	 * micreda.
	 * 
	 * @return id, may be <code>null</code>
	 */
	@JsonGetter("id")
	@ApiModelProperty(value = "unique identifier for the user; may be omitted/null if "
							+ "the user is currently unknown",
						required = false, 
						example = "3dd6f3d6-998a-11e7-9fd6-483a7beef7fc")
	public UUID 
	id()
	{
		return this.id;
	}
	
	/**
	 * Set the user's identifier.
	 * 
	 * @param id new ID for the user, may be <code>null</code>
	 * @return copy of the user with the given identifier set
	 */
	public MUser
	id(UUID id)
	{
		return new MUser(id, this.disabled(), this.roles());
	}

	/**
	 * Whether the user identity is considered "disabled".  Disabled users
	 * will be considered non-existent for the purposes of authentication.
	 * 
	 * @return <code>true</code> if the user is disabled, <code>false</code>
	 * otherwise
	 */
	@JsonGetter("disabled")
	@ApiModelProperty(value = "Whether the user identity is considered \"disabled\".  "
						+ "Disabled users will be considered non-existent for "
						+ "the purposes of authentication.",
						required = false)
	public boolean
	disabled()
	{
		return this.disabled;
	}
	
	/**
	 * Enable or disable the user.
	 * 
	 * @param disabled <code>true</code> if the user should be disabled,
	 * <code>false</code> otherwise
	 * @return copy of the user disabled/enabled accordingly
	 */
	public MUser
	disabled(boolean disabled)
	{
		return new MUser(this.id(), disabled, this.roles());
	}

	/**
	 * Get the roles the user has.
	 * 
	 * @return roles that the user has, not <code>null</code>, may be empty
	 */
	@JsonGetter("roles")
	@ApiModelProperty(value = "roles held by the user",
						required = true, allowEmptyValue = true)
	public Set<String> 
	roles() 
	{
		return Collections.unmodifiableSet(this.roles);
	}
	
	/**
	 * Set the roles that the user has.  The given roles will be de-duplicated.
	 * 
	 * @param roles roles to set, not <code>null</code>
	 * @return copy of the user with the given roles set
	 */
	public MUser
	roles(Collection<String> roles)
	{
		if (roles == null)
		{
			throw new IllegalArgumentException("roles must not be null");
		}
		
		return new MUser(this.id(), this.disabled(), new HashSet<>(roles));
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + (disabled ? 1231 : 1237);
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((roles == null) ? 0 : roles.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MUser other = (MUser) obj;
		if (disabled != other.disabled)
			return false;
		if (id == null)
		{
			if (other.id != null)
				return false;
		}
		else if (!id.equals(other.id))
			return false;
		if (roles == null)
		{
			if (other.roles != null)
				return false;
		}
		else if (!roles.equals(other.roles))
			return false;
		return true;
	}

	
	/* dummy setters because swagger-core sucks (bug #2169) */
	
	@JsonSetter("id")
	private void setID(UUID id) {}
	
	@JsonSetter("disabled")
	private void setDisabled(boolean disabled){}
	
	@JsonSetter("roles")
	private void setRoles(Set<String> roles) {}
	
	
}
