/*-
 * ========================LICENSE_START=================================
 * model
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.micreda.model.configuration;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;

import edu.unc.cscc.micreda.model.IdentitySource;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Container for information about a micreda instance/endpoint.
 * 
 * @author Rob Tomsick (rtomsick@unc.edu)
 *
 */
@ApiModel(value = "instance-information",
			description = "contains general interestg information about a "
						+ "micreda instance")
public final class MicredaEndpointInfo
{
	
	private final String				version;
	private final String				apiVersion;
	private final String				hostname;
	private final DatabaseInfo			databaseInfo;
	
	private final boolean				allowInternalSourceCreation;
	
	public MicredaEndpointInfo()
	{
		this("", "", null, null, false);
	}
	
	@JsonCreator
	public MicredaEndpointInfo(@JsonProperty("application-version") String version, 
							@JsonProperty("api-version") String apiVersion,
							@JsonProperty("hostname") String hostname, 
							@JsonProperty("database") DatabaseInfo databaseInfo,
							@JsonProperty("allows-internal-source-creation") 
								boolean allowInternalSourceCreation)
	{
		this.version = version;
		this.apiVersion = apiVersion;
		this.hostname = hostname;
		this.databaseInfo = databaseInfo;
		this.allowInternalSourceCreation = allowInternalSourceCreation;
	}
	
	/**
	 * The version of the application.
	 * 
	 * @return application version, not <code>null</code>
	 */
	@JsonGetter("application-version")
	@NotNull
	@ApiModelProperty(value = "Version of the micreda application.  "
								+ "May be empty if redacted.",
						allowEmptyValue = true, 
						required = true)
	public final String
	version()
	{
		return version;
	}

	/**
	 * Version of the micreda REST API that the instance supports.
	 * 
	 * @return API version, not <code>null</code>
	 */
	@JsonGetter("api-version")
	@NotNull
	@Size(min = 1)
	@ApiModelProperty(value = "Version of the micreda REST API that the "
							+ "instance supports. All instances must report "
							+ "a value here.",
						allowEmptyValue = false, required = true)
	public final String
	apiVersion()
	{
		return apiVersion;
	}

	/**
	 * Hostname under which the instance is running.  May or may not be 
	 * accurate.
	 * 
	 * @return hostname, may be <code>null</code>
	 */
	@JsonGetter("hostname")
	@ApiModelProperty(value = "hostname under which the instance is running. "
							+ "Intended for internal (administrative) "
							+ "debugging; in a production setting this may "
							+ "or may not correspond to the publicly-accessible "
							+ "hostname(s) used to reach the instance.",
						required = false,
						allowEmptyValue = true)
	public final String 
	hostname()
	{
		return hostname;
	}

	/**
	 * Information about the persistent storage that backs the instance.
	 * 
	 * @return database information, may be <code>null</code> if redacted
	 */
	@JsonGetter("database")
	@ApiModelProperty(value = "Information about the persistent storage that "
							+ "backs the instance. As this information is "
							+ "sensitive, instances may choose to redact it "
							+ "and/or omit it entirely.",
							required = false)
	public final DatabaseInfo 
	databaseInfo()
	{
		return databaseInfo;
	}

	/**
	 * Whether this instance allows the creation of internal 
	 * {@link IdentitySource identity sources}.
	 * 
	 * @return <code>true</code> if internal identity sources may be created,
	 * <code>false</code> otherwise
	 */
	@JsonGetter("allows-internal-source-creation")
	@ApiModelProperty(value = "Whether the instance allows the creation of "
							+ "internal identity sources.",
						required = true)
	public final boolean 
	allowInternalSourceCreation()
	{
		return allowInternalSourceCreation;
	}

	public MicredaEndpointInfo
	version(String version)
	{
		return new MicredaEndpointInfo(version, this.apiVersion(), 
									this.hostname(), this.databaseInfo(), 
									this.allowInternalSourceCreation());
	}
	
	public MicredaEndpointInfo
	apiVersion(String version)
	{
		return new MicredaEndpointInfo(this.version(), version, 
				this.hostname(), this.databaseInfo(), 
				this.allowInternalSourceCreation());
	}
	
	public MicredaEndpointInfo
	hostname(String hostname)
	{
		return new MicredaEndpointInfo(this.version(), this.apiVersion(), 
				hostname, this.databaseInfo(), 
				this.allowInternalSourceCreation());
	}
	
	public MicredaEndpointInfo
	databaseInfo(DatabaseInfo dbInfo)
	{
		return new MicredaEndpointInfo(this.version(), this.apiVersion(), 
				this.hostname(), dbInfo, 
				this.allowInternalSourceCreation());
	}
	
	public MicredaEndpointInfo
	allowInternalSourceCreation(boolean allowed)
	{
		return new MicredaEndpointInfo(this.version(), this.apiVersion(), 
				this.hostname(), this.databaseInfo(), 
				allowed);
	}
}
