/*-
 * ========================LICENSE_START=================================
 * model
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.micreda.model.ext;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;

import edu.unc.cscc.micreda.model.ExternalIdentitySource;
import edu.unc.cscc.micreda.model.ExternalIdentitySourceFactory;

/**
 * Skeletal implementation of a factory for external identity sources which
 * are instantiated according to properties files.  Handles discovery, parsing, 
 * and validation logic for factories which wish to externalize configuration
 * logic into properties files.
 * 
 * @author Rob Tomsick (rtomsick@unc.edu)
 *
 */
public abstract class PropertyBackedSourceFactory
implements ExternalIdentitySourceFactory
{
	
	private final List<URI>					searchPaths;
	private final List<String>				requiredProperties;
	private final Properties				defaultProperties;
	
	/**
	 * Construct a new source factory which will search the given paths (assumed
	 * to be files) for properties files.  Any discovered files will be checked
	 * to see if they contain the specified required properties, and their 
	 * keys/values combined with the given default properties when creating
	 * identity sources.
	 * 
	 * @param searchPaths URIs of paths to search, not <code>null</code>
	 * @param requiredProperties required properties, not <code>null</code> 
	 *  may be empty
	 * @param defaultProperties default properties, not <code>null</code>
	 */
	protected PropertyBackedSourceFactory(List<URI> searchPaths,
										List<String> requiredProperties, 
										Properties defaultProperties)
	{
		if (searchPaths == null)
		{
			throw new IllegalArgumentException(
					"search paths must not be null");
		}
		if (requiredProperties == null)
		{
			throw new IllegalArgumentException(
					"required properties must not be null");
		}
		
		this.searchPaths = new ArrayList<>(searchPaths);
		this.requiredProperties = new ArrayList<>(requiredProperties);
		this.defaultProperties = new Properties();
		if (defaultProperties != null)
		{
			this.defaultProperties.putAll(defaultProperties);
		}
	}

	@Override
	public Collection<ExternalIdentitySource> 
	obtainSources(final Logger logger)
	throws ExternalIdentitySourceConfigurationException
	{
		/* sanity check our search paths */
		
		List<File> pathDirs = new ArrayList<>();
		
		for (final URI u : this.searchPaths)
		{
			File file = new File(u);
			
			if (file.exists())
			{
				if (! file.canRead() || ! file.isDirectory())
				{
					throw new ExternalIdentitySourceConfigurationException(
							String.format("Path '%s' exists but is not directory", 
											file.getAbsolutePath()));
				}
				pathDirs.add(file);
			}
			
			
		}
		
		/* enumerate properties files in path dirs */
		List<File> propertiesFiles = discoverPropertiesFiles(pathDirs);
		
		if (propertiesFiles.isEmpty())
		{
			logger.warn("No configuration files in any of the search paths.  "
					+ "No sources have been configured.");
			
			return Collections.emptySet();
		}
		
		Map<String, File> fileMap = new HashMap<>();
		
		for (final File f : propertiesFiles)
		{
			final String name = f.getName();
			
			if (! name.endsWith(".properties"))
			{
				throw new IllegalArgumentException(
						"Found properties file without .properties suffix.  "
						+ "This is a bug.");
			}
			
			final String identifier = 
					name.substring(0, name.lastIndexOf(".properties"));
			
			if (fileMap.containsKey(identifier))
			{
				logger.warn(
						String.format(
						"Duplicate source identifier '%s'; "
							+ "replacing configuration from '%s' with one from "
							+ "'%s'", 
							identifier, 
							fileMap.get(identifier).getAbsolutePath(),
							f.getAbsolutePath()));
			}
			
			fileMap.put(identifier, f);
		}
		
		Map<File, ExternalIdentitySource> sources = new HashMap<>();

		
		for (Entry<String, File> e : fileMap.entrySet())
		{
			final File file = e.getValue();
			final String id = e.getKey();
			Properties props = new Properties(this.defaultProperties);
			
			try
			{
				try (final InputStream is = 
						new BufferedInputStream(new FileInputStream(file), 16 * 1024))
				{
					props.load(is);
				}
			}
			catch (IOException ioe)
			{
				throw new ExternalIdentitySourceConfigurationException(
						String.format("Failed to load properties file from '%s'", 
										file.getAbsolutePath()), 
						ioe);				
			}
			
			/* validate our properties */
			
			for (final String propName : this.requiredProperties)
			{
				if (! props.containsKey(propName))
				{
					throw new ExternalIdentitySourceConfigurationException(
							String.format("Required property '%s' missing from file '%s'", 
									propName,
									file.getAbsolutePath()));
				}
			}
			
			/* now instantiate.  We do this in the file loop so that we can
			 * whine about a specific file name if necessary
			 */
			ExternalIdentitySource src = null;
			
			try
			{
				src = createSource(id, props);
			}
			catch (ExternalIdentitySourceConfigurationException e1)
			{
				throw new ExternalIdentitySourceConfigurationException(
						String.format("Failed to create source from file '%s';"
								+ " reason was %s",
								e1.getMessage(),
								file.getAbsolutePath()), e1);
			}
			
			if (src == null)
			{
				throw new ExternalIdentitySourceConfigurationException(
						String.format("Failed to create source from file '%s'", 
								file.getAbsolutePath()));
			}
			
			sources.put(file, src);
		}
		
		if (! sources.isEmpty())
		{
			logger.info(
					String.format("Configured %d source(s) from properties: ", 
									sources.size()));
			sources.forEach((f, s) -> {
				logger.info(String.format("	'%s' <- '%s'", 
								s.identifier(), f.getAbsolutePath()));
			});
		}
		
		return sources.values();
	}
	
	/**
	 * Create a source with the given identifier, configured using the given
	 * properties.
	 * 
	 * @param identifier identifier, not <code>null</code>
	 * @param properties properties containing configuration information for
	 * the desired source
	 * @return source configured from the given properties, or <code>null</code>
	 * if a source could not be created (e.g. properties were invalid)
	 */
	protected abstract ExternalIdentitySource
	createSource(String identifier, Properties properties)
	throws ExternalIdentitySourceConfigurationException;
	
	private static final List<File>
	discoverPropertiesFiles(List<File> dirs)
	{
		return dirs.stream()
					.flatMap(PropertyBackedSourceFactory :: findPropertiesFiles)
					.collect(Collectors.toList());
	}
	
	private static final Stream<File>
	findPropertiesFiles(File dir)
	{
		if (! dir.canRead() || ! dir.isDirectory())
		{
			return Stream.empty();
		}
		
		final File[] foundFiles = 
			dir.listFiles(new FileFilter()
			{
				
				@Override
				public boolean
				accept(File file)
				{
					return file.canRead() 
							&& file.getName().endsWith(".properties");
				}
			});
		
		return Stream.of(foundFiles);
	}
	
}
