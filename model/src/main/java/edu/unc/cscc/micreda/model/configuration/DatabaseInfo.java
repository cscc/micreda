/*-
 * ========================LICENSE_START=================================
 * model
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.micreda.model.configuration;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Simple holder for database information that might be of use to other
 * layers of the application (for logging configuration info, for example)
 * 
 * @author Rob Tomsick (rtomsick@unc.edu)
 *
 */
@ApiModel(value = "database-info",
		description = "Database configuration information for an instance "
						+ "of micreda.")
public final class DatabaseInfo
{
	private final String		jdbcURL;
	private final String		username;
	private final int			poolSize;
	private final DatabaseType	type;
	
	@JsonCreator
	public DatabaseInfo(@JsonProperty("jdbc-url") String jdbcURL, 
						@JsonProperty("username") String username, 
						@JsonProperty("pool-size") int poolSize,
						@JsonProperty("backend") DatabaseType type)
	{
		this.jdbcURL = jdbcURL;
		this.username = username;
		this.poolSize = poolSize;
		this.type = type;
	}

	/**
	 * The JDBC URL used to connect the instance to persistent storage.
	 * 
	 * @return JDBC URL, not <code>null</code>
	 */
	@JsonGetter("jdbc-url")
	@ApiModelProperty(value = "JDBC URL used to connect the instance to "
							+ "persistent storage.",
						required = true, allowEmptyValue = false)
	@Size(min = 1)
	@NotNull
	public String 
	jdbcURL()
	{
		return this.jdbcURL;
	}

	/**
	 * Username used to connect the instance to persistent storage.
	 * 
	 * @return username, may be <code>null</code>
	 */
	@JsonGetter("username")
	@ApiModelProperty(value = "Username used to connect to the persistent "
							+ "storage (if any).",
					required = false, allowEmptyValue = true)
	public String 
	username()
	{
		return this.username;
	}

	/**
	 * The size of the database connection pool via which the instance 
	 * connects to persistent storage.
	 * 
	 * @return pool size, should be &gt; 0
	 */
	@JsonGetter("pool-size")
	@ApiModelProperty(value = "Size of the database connection pool via which "
						+ "the instance connects to persistent storage.",
						required = false, allowEmptyValue = false)
	@Min(0)
	public int 
	poolSize()
	{
		return this.poolSize;
	}

	/**
	 * The type of persistent storage that backs the instance.
	 * 
	 * @return backend type, not <code>null</code>
	 */
	@JsonGetter("backend")
	@ApiModelProperty(value = "Type of persistent storage that backs the instance.",
						required = true, allowEmptyValue = false)
	@NotNull
	public DatabaseType 
	type()
	{
		return this.type;
	}

	/**
	 * Enumeration of the types of persistent storage that micreda may use.
	 * 
	 * @author Rob Tomsick (rtomsick@unc.ed)
	 *
	 */
	@ApiModel("database-type")
	public static enum DatabaseType
	{
		/**
		 * PostgreSQL relational database.
		 */
		POSTGRES,
		/**
		 * HSQL embedded relational database.
		 */
		HSQLDB
	}
	
}
