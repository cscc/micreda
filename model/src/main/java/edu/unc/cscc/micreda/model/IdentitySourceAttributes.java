/*-
 * ========================LICENSE_START=================================
 * model
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.micreda.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonGetter;

import io.swagger.annotations.ApiModelProperty;

/**
 * The identifying attributes of an identity source.
 * 
 * @author Rob Tomsick (rtomsick@unc.edu)
 *
 */
public interface IdentitySourceAttributes
{

	/**
	 * Unique identifier for the identity source.  Must not contain
	 * a dot ('.'), consist only of whitespace, be empty, 
	 * or be <code>null</code>.
	 * 
	 * @return identity source identifier, not <code>null</code>, not empty
	 */
	@JsonGetter("identifier")
	@ApiModelProperty(value = "A unique identifier for the identity source.",
						example = "sample-ldap-source",
						required = true, allowEmptyValue = false)
	@NotNull @Size(min = 1) @Pattern(regexp = "^[^\\.]*$")
	public String identifier();

	/**
	 * A human-readable description of the identity source.
	 * 
	 * @return description of identity source, not <code>null</code>, may be
	 * empty
	 */
	@JsonGetter("description")
	@ApiModelProperty(value = "A human-readable description of the identity "
							+ "source. May be empty, but strongly recommended.",
						example = "a sample LDAP-based source",
						required = true, allowEmptyValue = true)
	@NotNull
	public String description();
	
	/**
	 * <p>
	 * Whether the identity source is external (i.e. authenticates users against
	 * a source of identity information outside of an instance of micreda)
	 * or internal.
	 * </p>
	 * 
	 * <p>
	 * This method should ONLY be used for serialization.  You probably don't
	 * want to override the default implementation.
	 * </p>
	 * 
	 * @return <code>true</code> if the source is external, <code>false</code>
	 * otherwise
	 */
	@JsonGetter("external")
	@ApiModelProperty(value = "Whether the identity source is external "
							+ "(i.e. authenticates users against a source of "
							+ "identity information outside of an instance of "
							+ "micreda)or internal.", 
					example = "true", required = true)
	default boolean isExternal()
	{
		return true;
	}
	
	/**
	 * Whether the identity source is active, i.e. whether it may be used
	 * by the current instance for authentication of user identities.  Inactive
	 * sources may correspond to those which are associated with a user identity
	 * but are not loaded into the current instance, internal identity sources
	 * which are disabled, etc.
	 * 
	 * @return <code>true</code> if the source is active, <code>false</code>
	 * otherwise
	 */
	@JsonGetter("active")
	@ApiModelProperty(value = "Whether the identity source is active, i.e. "
							+ "whether it may be used by the current instance "
							+ "for authentication of user identities.  "
							+ "Inactive sources may correspond to those "
							+ "which are associated with a user identity but "
							+ "are not loaded into the current instance,"
							+ " internal identity sourceswhich are disabled, "
							+ "etc.",
						required = true, example = "true")
	public default boolean
	isActive()
	{
		return true;
	}

}
