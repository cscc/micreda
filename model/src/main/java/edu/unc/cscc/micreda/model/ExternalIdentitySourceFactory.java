/*-
 * ========================LICENSE_START=================================
 * model
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.micreda.model;

import java.util.Collection;

import org.slf4j.Logger;

/**
 * Factory interface for {@link ExternalIdentitySource external identity sources}.
 * 
 * Designed to allow third party implementors to produce artifacts which are
 * capable of providing multiple external identity sources, usually created
 * at run-time (such as would be preferred when authenticating against a
 * remote endpoint for which hardcoded configuration would be inappropriate.)
 * 
 * @author Rob Tomsick (rtomsick@unc.edu)
 *
 */
public interface ExternalIdentitySourceFactory
{	
	/**
	 * Create/obtain the sources produced by this factory.  Implementations
	 * do not need to create their produced sources each time this method is
	 * invoked; returning references to previously-created sources is 
	 * perfectly acceptable.
	 * 
	 * @param logger logger which the factory may use to log information
	 * during the source configuration process
	 * 
	 * @return sources managed by this factory
	 * @throws RuntimeException thrown
	 * if an error occurred during source creation
	 * @throws {@link ExternalIdentitySourceConfigurationException} thrown
	 * if an error occurred during source creation
	 */
	public Collection<ExternalIdentitySource>
	obtainSources(Logger logger)
	throws RuntimeException, ExternalIdentitySourceConfigurationException;
	
	/**
	 * Exception indicating an error or failure during the configuration or 
	 * initialization of an 
	 * {@link ExternalIdentitySource external identity source}.
	 * 
	 * @author Rob Tomsick (rtomsick@unc.edu)
	 *
	 */
	public class ExternalIdentitySourceConfigurationException
	extends Exception
	{
		
		private static final long serialVersionUID = 1L;
		
		public ExternalIdentitySourceConfigurationException(String msg)
		{
			this(msg, null);
		}

		public ExternalIdentitySourceConfigurationException(String msg, 
															Throwable cause)
		{
			super(msg, cause);
		}
	}
}
