/*-
 * ========================LICENSE_START=================================
 * model
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.micreda.model.configuration;

import javax.validation.constraints.Min;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;

import edu.unc.cscc.micreda.model.MUser;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Represents public, discoverable information about a micreda JWT endpoint.
 * 
 * @author Rob Tomsick (rtomsick@unc.edu)
 *
 */
@ApiModel(value = "jwt-info",
			description = "Public, discoverable information about JWT tokens "
						+ "issued by a micreda instance.")
public class JWTEndpointInfo
{
	
	private final int		gracePeriod;
	private final String	claimPrefix;
	private final String	idClaimName;
	private final String	rolesClaimName;
	
	
	@JsonCreator
	public JWTEndpointInfo(@JsonProperty("grace-period-seconds") int gracePeriod, 
							@JsonProperty("claim-prefix") String claimPrefix,
							@JsonProperty("id-claim-name") String idClaimName, 
							@JsonProperty("roles-claim-name") String rolesClaimName)
	{
		if (gracePeriod < 0)
		{
			throw new IllegalArgumentException("grace period must be >= 0");
		}
		
		if (claimPrefix == null)
		{
			throw new IllegalArgumentException("claim prefix may not be null");
		}
		
		if (idClaimName == null || idClaimName.isEmpty())
		{
			throw new IllegalArgumentException("ID claim name may not be null or empty");
		}
		
		if (rolesClaimName == null || rolesClaimName.isEmpty())
		{
			throw new IllegalArgumentException("roles claim name may not be null or empty");
		}
		
		this.gracePeriod = gracePeriod;
		this.claimPrefix = claimPrefix;
		this.idClaimName = idClaimName;
		this.rolesClaimName = rolesClaimName;
	}

	/**
	 * Grace period of an endpoint, in seconds.
	 * 
	 * @return number of seconds in grace period, &ge; 0
	 */
	@JsonGetter("grace-period-seconds")
	@ApiModelProperty(value = "Grace period for token expiration, in seconds",
						example = "100", required = true)
	@Min(0)
	public int
	gracePeriod()
	{
		return this.gracePeriod;
	}
	
	/**
	 * Instance-specific prefix for all JWT claims.
	 * 
	 * @return prefix, not <code>null</code>, may be empty
	 */
	@JsonGetter("claim-prefix")
	@ApiModelProperty(value = "Instance-specific prefix for all JWT claims",
						example = "my_organization_name-", required = true)
	public String
	claimPrefix()
	{
		return this.claimPrefix;
	}
	
	/**
	 * Un-prefixed name of the claim containing the {@link MUser#id() ID} of 
	 * the {@link MUser user} for which a token is issued.
	 *  
	 * @return name of ID claim, not <code>null</code>, not empty
	 */
	@JsonGetter("id-claim-name")
	@ApiModelProperty(value = "Un-prefixed name of the claim containing the "
							+ "ID of the user for which a token is issued",
						example = "user-id",
						required = true)
	public String
	idClaimName()
	{
		return this.idClaimName;
	}
	
	/**
	 * Un-prefixed name of the claim containing the {@link MUser#roles()() roles} 
	 * assigned to the {@link MUser user} for which a token is issued.
	 *  
	 * @return name of roles claim, not <code>null</code>, not empty
	 */
	@JsonGetter("roles-claim-name")
	@ApiModelProperty(value = "Un-prefixed name of the claim containing the "
							+ "roles assigned to the user for which a token is "
							+ "issued",
						example = "roles", required = true)
	public String
	rolesClaimName()
	{
		return this.rolesClaimName;
	}
	
}
