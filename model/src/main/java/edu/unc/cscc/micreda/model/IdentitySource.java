/*-
 * ========================LICENSE_START=================================
 * model
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.micreda.model;

import io.swagger.annotations.ApiModel;

/**
 * Base interface for all identity source implementations.  Not intended for 
 * direct third party implementation (you want {@link ExternalIdentitySource} 
 * instead.)
 * 
 * @author Rob Tomsick (rtomsick@unc.edu)
 *
 */
@ApiModel(value = "identity-source")
public interface IdentitySource
extends IdentitySourceAttributes
{

	/**
	 * Attempt to authenticate a user with the given username using the given
	 * credentials.  The format or significance of the credentials is left
	 * to the identity source.
	 * 
	 * @param username username of user for which to attempt authentication, 
	 * not <code>null</code>
	 * @param credentials credentials of user for which to attempt 
	 * authentication, may be <code>null</code>
	 * @return <code>true</code> if the given username/credentials correspond
	 * to an identity known by the identity source, <code>false</code>
	 * otherwise
	 */
	public boolean authenticate(String username, String credentials);
	
	
	/**
	 * Validate the identifier according to the {@link #identifier() rules}.
	 * 
	 * @param identifier identifier to validate
	 * @return <code>true</code> if the identifier is valid, <code>false</code>
	 * otherwise
	 */
	public static boolean
	validIdentifier(String identifier)
	{
		return identifier != null
				&& ! identifier.trim().isEmpty()
				&& ! identifier.contains("/")
				&& ! identifier.contains(".");
	}
}
