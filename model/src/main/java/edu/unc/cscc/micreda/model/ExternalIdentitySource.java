/*-
 * ========================LICENSE_START=================================
 * model
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.micreda.model;

import io.swagger.annotations.ApiModel;

/**
 * <p>
 * An external identity source; i.e. one which is implemented outside of 
 * micreda, intended for run-time detection and loading.
 * 
 * <p>
 * Sources must fulfill three requirements to be suitable for use:
 * </p>
 * 
 * <ol>
 * 	<li>Sources must be thread-safe.</li>
 * 	<li>Sources must be usable as singletons (i.e. one instance per instance of
 * micreda)</li>
 * 	<li>Sources must ensure that their {@link #authenticate(String, String)}
 * implementations have reasonable timeouts and do not block indefinitely.</li>
 * </ol>
 * 
 * <p><b>Failure to meet the above requirements may result in incorrect behavior
 * or deadlock of instances into which the source is loaded.</b><p>
 * 
 * @author Rob Tomsick (rtomsick@unc.edu)
 *
 */
@ApiModel("external-identity-source")
public interface ExternalIdentitySource
extends IdentitySource
{

}
